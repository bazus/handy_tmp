<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once '/var/www/html/classes/affiliate/affiliates.php';
function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
{
	$sets = array();
	if(strpos($available_sets, 'l') !== false)
		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
	if(strpos($available_sets, 'u') !== false)
		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
	if(strpos($available_sets, 'd') !== false)
		$sets[] = '23456789';
	if(strpos($available_sets, 's') !== false)
		$sets[] = '!@#$%&*?';
	$all = '';
	$password = '';
	foreach($sets as $set)
	{
		$password .= $set[array_rand(str_split($set))];
		$all .= $set;
	}
	$all = str_split($all);
	for($i = 0; $i < $length - count($sets); $i++)
		$password .= $all[array_rand($all)];
	$password = str_shuffle($password);
	if(!$add_dashes)
		return $password;
	$dash_len = floor(sqrt($length));
	$dash_str = '';
	while(strlen($password) > $dash_len)
	{
		$dash_str .= substr($password, 0, $dash_len) . '-';
		$password = substr($password, $dash_len);
	}
	$dash_str .= $password;
	return $dash_str;
}

$fullName = $_POST['fullName'];
$phone = $_POST['phone'];
$email = $_POST['email'];

$password = generateStrongPassword();
$cryptpassword = password_hash($password, PASSWORD_BCRYPT);

$returnData = false;

try{
    $affiliates = new affiliates();
    
    $returnData = $affiliates->createAcc($fullName, $email, $cryptpassword, $phone);
    
} catch (Exception $e){
    $returnData = $e;
}

echo json_encode($returnData);
if($returnData['status'] == false){
    die();
}
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '/var/www/html/vendor/autoload.php';


$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->CharSet = 'UTF-8';
    $mail->SMTPDebug = false;//SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'joe@handymatcher.com';                     // SMTP username
    $mail->Password   = 'Mds6565959';                               // SMTP password
    $mail->SMTPSecure = "TLS";//PHPMailer::ENCRYPTION_STARTTLS         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    $mail->Port       = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('joe@handymatcher.com', 'Joe from Handy Matcher');
    $mail->addAddress($email, $fullName);     // Add a recipient

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Thanks for signing up with HandyMatcher Affiliates Program';

    $message = '<html><body>';
    $message .= '<h2 style="font-size: 18px;">Hello '.$fullName.'</h2>';
    $message .= '<p style="font-size:16px;">Thanks for signing up with HandyMatcher.</p>';
    $message .= '<p style="font-size:16px;">Your new password is: '.$password.' you can change it anytime by the console at <a href="https://handymatcher.com">handymatcher.com</a></p>';
    $message .= '<p style="font-size:16px;">Looking forward to hear from you!</p><br>';
    $message .= '<p style="font-size:16px;">Regards, <br><br>Joe Pusser<br>Sales Executive<br><a href="https://handymatcher.com">www.handymatcher.com</a><br><a href="tel:+1 888 205 9685">888 205 9685</a><br><a href="mailto:joe@leadrift.com">joe@handymatcher.com</a></p>';
    $message .= '</body></html>';

    $mail->Body    = $message;
    $mail->AltBody = $message;

    $mail->send();
    $returnStatus = true;

} catch (PHPMailer\PHPMailer\Exception $e) {
    $returnStatus = $e;
    unset($e);
}

$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->CharSet = 'UTF-8';
    $mail->SMTPDebug = false;//SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'leads@handymatcher.com';                     // SMTP username
    $mail->Password   = 'Mds6565959';                               // SMTP password
    $mail->SMTPSecure = "TLS";//PHPMailer::ENCRYPTION_STARTTLS         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    $mail->Port       = 587;                                     // TCP port to connect to

    //Recipients
    $mail->setFrom('leads@handymatcher.com', 'Handy Matcher');
    $mail->addAddress('maorjosephnotify@gmail.com', 'Maor User');     // Add a recipient
    $mail->addAddress('joe@leadrift.com', 'Joe User');     // Add a recipient
    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject ='New Affiliate Register';

    $message = '<html><body>';
    $message .= '<h2 style="font-size: 20px;">New Affiliate</h2>';
    $message .= '<p style="font-size: 16px;">';
    $message .= 'Full Name: '.$fullName.'<br>';
    $message .= 'Email: '.$email.'<br>';
    $message .= 'Phone: '.$phone.'<br>';
    $message .= '</p>';
    $message .= '</body></html>';

    $mail->Body    = $message;
    $mail->AltBody = $message;

    $mail->send();
} catch (Exception $e) {
    
}   