<?php
if(isset($_POST['zip'])){
    $zip = $_POST['zip'];
    $conn = new PDO("mysql:host=127.0.0.1;dbname=myApi", "admin", "ae03be1f9f67f91cbb12b493f514da6c5e5258ef3a9cddf0");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT * FROM myApi.ZIPCodes WHERE ZipCode=:zip");
    $stmt->bindParam(':zip', $zip, PDO::PARAM_INT);
    $stmt->execute();
    $stmtData = $stmt->fetch(PDO::FETCH_ASSOC);
    echo json_encode($stmtData);
}
