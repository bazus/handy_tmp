<?php
require_once '/var/www/html/classes/server/DB.php';


class affiliates extends DB {
    function __construct() {
        parent::__construct();
    }
    public function createAcc($fullName,$email,$cryptpassword,$phone){
        
        
        $returnData = [];
        $returnData['status'] = false;
        $returnData['emailError'] = $this->checkEmail($email);
        $returnData['key'] = $this->generateLink();
        $returnData['userId'] = 0;
        
        if($returnData['emailError'] == false){
            $stmt = $this->conn->prepare("INSERT INTO affiliates(fullName,email,password,phone,customLink) VALUES(:fullName,:email,:password,:phone,:customLink)");

            $stmt->bindParam(':fullName', $fullName, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':password', $cryptpassword, PDO::PARAM_STR);
            $stmt->bindParam(':phone', $phone, PDO::PARAM_STR);
            $stmt->bindParam(':customLink', $returnData['key'], PDO::PARAM_STR);

            $stmt->execute();
            $id = $this->conn->lastInsertId();
            if($id){
                $returnData['status'] = true;
                $returnData['userId'] = $id;
                
                $_SESSION['affId'] = $id;
                $_SESSION['affLink'] = $returnData['key'];
            }
        }
        
        return $returnData;
        
    }
    public function login($username,$password){
        
        
        $returnData = [];
        $returnData['status'] = false;
        
        try{
            $stmt = $this->conn->prepare("SELECT * FROM joseianq_myApi.affiliates WHERE email=:email");

            $stmt->bindParam(':email', $username, PDO::PARAM_STR);

            $stmt->execute();
            $data = $stmt->fetch();
            
            if($data && $data['password']){
                if(password_verify($password, $data['password'])){
                    $_SESSION['affId'] = $data['id'];
                    $_SESSION['affLink'] = $data['customLink'];
                    $returnData['status'] = true;
                }
            }
        } catch (Exception $e){
            $returnData['error'] = $e;
        }
        
        return $returnData;
        
    }
    private function generateLink(){
        
        $link = $this->generateStrongPassword(4);
        $stmt = $this->conn->prepare("SELECT COUNT(*) FROM joseianq_myApi.affiliates WHERE customLink=:link");
        $stmt->bindParam(':link', $link, PDO::PARAM_STR);
        $stmt->execute();
        if($stmt->fetchColumn() > 0){
            $this->generateLink();
        }else{
            return $link;
        }
    }
    private function checkEmail($email){
        
        $stmt = $this->conn->prepare("SELECT COUNT(*) FROM joseianq_myApi.affiliates WHERE email=:email");
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
        if($stmt->fetchColumn() > 0){
            return true;
        }else{
            return false;
        }
    }
    private function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'lud'){
	$sets = array();
	if(strpos($available_sets, 'l') !== false)
		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
	if(strpos($available_sets, 'u') !== false)
		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
	if(strpos($available_sets, 'd') !== false)
		$sets[] = '23456789';
	if(strpos($available_sets, 's') !== false)
		$sets[] = '!@#$%&*?';
	$all = '';
	$password = '';
	foreach($sets as $set)
	{
		$password .= $set[array_rand(str_split($set))];
		$all .= $set;
	}
	$all = str_split($all);
	for($i = 0; $i < $length - count($sets); $i++)
		$password .= $all[array_rand($all)];
	$password = str_shuffle($password);
	if(!$add_dashes)
		return $password;
	$dash_len = floor(sqrt($length));
	$dash_str = '';
	while(strlen($password) > $dash_len)
	{
		$dash_str .= substr($password, 0, $dash_len) . '-';
		$password = substr($password, $dash_len);
	}
	$dash_str .= $password;
	return $dash_str;
    }
    public function createClick($affLink,$ip){
        try{
            $stmt = $this->conn->prepare("INSERT INTO affiliatesClicks(affLink,ip) VALUES(:affLink,:ip)");
            $stmt->bindParam(':affLink', $affLink, PDO::PARAM_STR);
            $stmt->bindParam(':ip', $ip, PDO::PARAM_STR);
            $stmt->execute();
        } catch (PDOException $e){
            var_dump($e);
        }
        return 1;
    }
    public function getAffIdFromLink($affLink){
        try{
            $stmt = $this->conn->prepare("SELECT id FROM joseianq_myApi.affiliates WHERE customLink=:affLink");
            $stmt->bindParam(':affLink', $affLink, PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetch();
            if($data){
                return $data['id'];
            }else{
                return false;
            }
        } catch (PDOException $e){
            return $e;
        }
        return 0;
    }
    public function getAffData($affId){
        try{
            $returnData = [];
            $stmt = $this->conn->prepare("SELECT * FROM joseianq_myApi.affiliates WHERE id=:affId");
            $stmt->bindParam(':affId', $affId, PDO::PARAM_STR);
            $stmt->execute();
            $returnData['data'] = $stmt->fetch();
            
            $stmt = $this->conn->prepare("SELECT * FROM joseianq_myApi.affiliatesCredits WHERE affId=:affId");
            $stmt->bindParam(':affId', $affId, PDO::PARAM_STR);
            $stmt->execute();
            $returnData['credits'] = $stmt->fetchAll();
            if($returnData){
                return $returnData;
            }else{
                return false;
            }
        } catch (PDOException $e){
            return $e;
        }
        return 0;
    }
    public function creditAcc($affLink){
        try{
            $stmt = $this->conn->prepare("SELECT id FROM joseianq_myApi.affiliates WHERE customLink=:affLink");
            $stmt->bindParam(':affLink', $affLink, PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetch();
            if($data){
                return $data['id'];
            }else{
                return false;
            }
        } catch (PDOException $e){
            return $e;
        }
        return 0;
    }
}
