<?php
session_start();
if (isset($_GET['clearSession'])){
    session_destroy();
}
$siteUrl = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/";
?>
<?php
// Include config file
require_once "config/db.php";

// Define variables and initialize with empty values
$email = $password =  "";
$email_err = $password_err =  "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Validate username
    if(empty(trim($_POST["email"]))){
        $email_err = "Please enter a email.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM users WHERE email = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_email);

            // Set parameters
            $param_email = trim($_POST["email"]);

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) == 1){
                    $email_err = "This email is already taken.";
                } else{
                    $email = trim($_POST["email"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }

    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }

    // Check input errors before inserting in database
    if(empty($email_err) && empty($password_err)){

        // Prepare an insert statement
        $sql = "INSERT INTO users (email, password) VALUES (?, ?)";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_email, $param_password);

            // Set parameters
            $param_email = $email;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                header("location: login.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }

    // Close connection
    mysqli_close($link);
}
?>
<html lang="en">
    <head>
        <title>Handy Matcher</title>
        <!-- ============== Jquery & UI ============== -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <!-- ============== Bootstrap ============== -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

        <!-- ============== Custom Files ============== -->
        <link rel="stylesheet" href="<?= $siteUrl; ?>css/style.css">
        <script>
            var BASE_URL = '<?= $siteUrl ?>';
        </script>

        <meta name="google-signin-client_id" content="357226719040-hd75f4ltg8cakhmgcfmaqo7qsi9cv9dn.apps.googleusercontent.com">

    </head>
    <body>
    <?php require_once($fullPath."tpl/header.php")?>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-main">
            <div class="app-main__outer" style="padding: 0">
                <div class="app-main__inner">
                    <div id="registerModal" class="modal-dialog w-100">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="h5 modal-title text-center">
                                    <h4 class="mt-2">
                                        <div>Welcome to Handy Matcher</div>
                                    </h4>
                                </div>
                                    <div class="form-row">
                                             <div class="col-md-12">
                                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                                                        <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                                                            <label>Email</label>
                                                            <input type="text" name="email" class="form-control" value="<?php echo $email; ?>">
                                                            <span class="help-block"><?php echo $email_err; ?></span>
                                                        </div>
                                                        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                                                            <label>Password</label>
                                                            <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                                                            <span class="help-block"><?php echo $password_err; ?></span>
                                                            <p class="text-center" style="font-size: 0.8rem;" >Use 6 or more characters, with a mix of letters, numbers and symbols</p>
                                                        </div>
                                                        <div class="mt-5 modal-title text-center">
                                                            <button class="btn btn-primary btn-lg form-control" type="submit" value="Sign Up">Sign Up</button>
                                                        </div>
                                                        <div class="modal-footer clearfix"></div>
                                                        <h6 class="modal-title text-center">Already have an account? <a style="color:black" class="font-weight-bold"  href="https://handymatcher.com/login.php"">Sign in</a>
                                                        <div class="text-center" style="font-size: 0.7rem;">By signing up, signing in or continuing. I agree to Houzz's <a href="#">Terms of Use </a> and <a href="#">Privacy Policy</a></div>
                                                    </form>
                                             </div>
                                    </div>
                            </div>
                        </div>
                </div>
            </div>
            <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
        <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
    </body>
</html>
