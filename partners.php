﻿<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    $isMobile = true;
}else{
    $isMobile = false;
}
if (isset($_GET['ref'])){$ref = $_GET['ref'];}else{
    if (isset($_GET['r'])){
        $ref = $_GET['r']." | Handy Matcher";
    }else{
        $ref = "Organic"." | Handy Matcher";
    }
}
$fullPath = "/var/www/html/";
if($_SERVER['HTTP_HOST'] == "localhost"){$fullPath = "";}

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <?php
        $title = "Handy Matcher | Join Our Network";
        $description = "Handy Matcher | Moving and Home Improvement. It is the simplest way to find and book top-rated local home services. Connect with trusted home repair and improvement contractors.";
        require_once '/var/www/html/tpl/head.php';
    ?>
    <link rel="stylesheet" href="https://handymatcher.com/css/site.css">
</head>

<body>
    <?php require_once($fullPath."tpl/header.php")?>
    <section id="progressBarContainer">
        <div class="row">
            <div class="col-11">
                <div class="progress">
                    <div id="progressBar" class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <div class="col-1">
                <div id="percentage">0%</div>
            </div>
        </div>
    </section>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="myBox">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h2>Become a Partner</h2>
                                <br><br><br>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-center">
                                <h5>Please fill the info below so we can get you more clients in your area!</h5>
                            </div>
                        </div>
                        <div class="stepContainer">
                            <div class="row customBox" id="step1">
                                <div class="col-12 text-center">
                                    <h2>What kind of Service do you supply?</h2>
                                    <hr>
                                    <h5>Service</h5>
                                    <select class="my-custom-control" id="serviceType" onchange="selectServiceType(this.value)">
                                        <option selected="" disabled="" value="">Select Service</option>
                                        <option value="Local Moving">Local Moving</option>
                                        <option value="Long Distance Moving">Long Distance Moving</option>
                                        <option value="Local & Long Distance Moving">Local & Long Distance Moving</option>
                                        <option value="Auto transport">Auto transport</option>
                                        <option value="Cleaning">Cleaning</option>
                                        <option value="Plumbing">Plumbing</option>
                                        <option value="Flooring">Flooring</option>
                                        <option value="Roofing">Roofing</option>
                                        <option value="" disabled="">===========</option>
                                        <option value="0">Other</option>
                                    </select>
                                    <hr>
                                    <input type="text" placeholder="Enter Reason" class="my-custom-control" style="display:none;" id="serviceTypeOther">
                                    <button disabled id="firstNext" class="btn btn-custom btn-lg" onclick="moveToStep2()">Next</button>
                                </div>
                            </div>
                            <div class="row customBox" id="step2" style="display:none;">
                                <div class="col-12 text-center">
                                    <h2>In what states your business operate?</h2>
                                    <hr>
                                    <h5>States</h5>
                                    <input id="states" onkeyup="checkStep2(this.value)" type="text" class="my-custom-control" placeholder="Enter States e.g. NY,CA,OH,...">
                                    <hr>
                                    <button disabled id="secondNext" class="btn btn-custom btn-lg" onclick="moveToStep3()">Next</button>
                                    <button class="btn btn-default btn-lg" onclick="moveToStep1()">Previous</button>
                                </div>
                            </div>
                            <div class="row customBox" id="step3" style="display:none;">
                                <div class="col-12 text-center">
                                    <h2>How can we reach you?</h2>
                                    <hr>
                                    <h5>Personal Name</h5>
                                    <input type="text" id="fullName" class="my-custom-control" placeholder="Enter Full Name">
                                    <hr>
                                    <h5>Company Name</h5>
                                    <input type="text" id="companyName" class="my-custom-control" placeholder="Enter Company Name">
                                    <hr>
                                    <h5>Phone Number</h5>
                                    <input type="text" id="phone" class="my-custom-control" placeholder="Enter Phone Number">
                                    <hr>
                                    <h5>Email Address</h5>
                                    <input type="text" id="email" class="my-custom-control" placeholder="Enter Email Address">
                                    <hr>
                                    <button class="btn btn-custom btn-lg" id="sendBtn" onclick="sendClientRequest()">Send</button>
                                    <button class="btn btn-default btn-lg" onclick="moveToStep3()">Previous</button>
                                </div>
                            </div>
                        </div>
                        
                        <div class="section1">
                      		<div class="container">
                      			<div class="head text-center">
                      				<h3>Attract new customers every day with contractors leads</h3>
                      				<p>We're generating leads specifically for your needs, all our leads must prove that they are genuine and authentic in order to be sent.</p>
                      			</div>
                            <br><br><br>
                      			<div class="row">
                      				<div class="col-md-4 col-lg-4 box">
                      					<div class="box-inner">
                      						<img src="images/3.png" class="img-fluid d-block mx-auto">
                      						<h4>100% “live” customers</h4>
                      						<p>You will get the contact info of a people who are looking for your service.</p>
                      					</div>
                      				</div>
                      				<div class="col-md-4 col-lg-4 box">
                      					<div class="box-inner">
                      						<img src="images/2.png" class="img-fluid d-block mx-auto">
                      						<h4>In less than a moment</h4>
                      						<p>Average time from the moment of request to your account.</p>
                      					</div>
                      				</div>
                      				<div class="col-md-4 col-lg-4 box">
                      					<div class="box-inner">
                      						<img src="images/1.png" class="img-fluid d-block mx-auto">
                      						<h4>Flexible Filtering</h4>
                      						<p>Receive only leads you are looking for. Our filters are fully customizable.</p>
                      					</div>
                      				</div>
                      			</div>
                      		</div>
                      	</div>
                        <br><br><br><br>

                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php require_once($fullPath."tpl/footer.php")?>
    <script src="https://use.fontawesome.com/7e5b532868.js"></script>
    <script>
        var clientData = [];
        clientData['service'] = "";
        clientData['states'] = "";

        function sendClientRequest(){
            $("sendBtn").attr("disabled",true);
            $("sendBtn").text("Sending");

            var sentData = [];
            sentData['name'] = document.getElementById('fullName').value;
            sentData['companyName'] = document.getElementById('companyName').value;
            sentData['phone'] = document.getElementById('phone').value;
            sentData['email'] = document.getElementById('email').value;
            sentData['stats'] = clientData['status'];
            sentData['service'] = clientData['service'];

            if(sentData){
                $.ajax({
                    url: "https://handymatcher.com/actions/sendPartnerInfo.php",
                    data:{
                        name: document.getElementById("fullName").value,
                        companyName: document.getElementById('companyName').value,
                        phone: document.getElementById('phone').value,
                        email: document.getElementById('email').value,
                        states: document.getElementById('states').value,
                        service: document.getElementById('serviceType').value
                    },
                    dataType:"JSON",
                    method:"POST",
                    async: false
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data) {
                            window.location.href("https://handymathcer.com/thankYouPartners/"+document.getElementById('fullName').value);
                        }else{
                            alert("oops we got into a problem, please try again later");
//                          window.location.reload();
                        }
                    }catch (e) {
                        alert(data);
                        console.log(data);
                        console.log(e);
                    }
                });
            }
        }

        function selectServiceType(val){
            if(val != 0 && val != "0"){
                $("#serviceTypeOther").hide();
                clientData['service'] = val;
                $("#firstNext").removeAttr("disabled");
                $("#progressBar").width('50%');
                $("#percentage").text("50%");
            }else{
                $("#progressBar").width('0%');
                $("#percentage").text("0%");
                $("#firstNext").attr("disabled",true);
                $("#serviceTypeOther").show();
            }
        }
        window.onscroll = function() {checkForSticky()};
        var progressBarContainer = document.getElementById("progressBarContainer");
        var sticky = progressBarContainer.offsetTop;
        function checkForSticky() {
          if (window.pageYOffset > sticky) {
            progressBarContainer.classList.add("sticky");
          } else {
            progressBarContainer.classList.remove("sticky");
          }
        }
        function moveToStep1(){
            $('html, body').animate({scrollTop:$('#step1').position().top}, 'slow');
        }
        function moveToStep2(){
            $("#step2").show();
            $('html, body').animate({scrollTop:$('#step2').position().top}, 'slow');
        }
        function checkStep2(val){
            if(val){
                clientData['states'] = val;
                $("#secondNext").removeAttr("disabled");
                $("#progressBar").width('90%');
                $("#percentage").text("90%");
            }else{
                $("#secondNext").attr("disabled",true);
                $("#progressBar").width('50%');
                $("#percentage").text("50%");
                $("#step3").hide();
            }
        }
        function moveToStep3(){
            $("#step3").show();
            $('html, body').animate({scrollTop:$('#step3').position().top}, 'slow');
        }

    </script>
</body>
</html>
