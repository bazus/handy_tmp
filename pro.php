﻿<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    $isMobile = true;
}else{
    $isMobile = false;
}
if (isset($_GET['r'])){
    $ref = $_GET['r']." | Handy Matcher";
}else{
    $ref = "Organic"." | Handy Matcher";
}
$fullPath = "/var/www/html/";
if($_SERVER['HTTP_HOST'] == "localhost"){$fullPath = "";}
if(isset($_GET['affiliate'])){
    function get_client_ip_env() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
    if(!isset($_SESSION)){
        session_start();
    }
    if(!isset($_COOKIE['affiliateLink'])){
        $_SESSION['affiliateLink'] = $_GET['affiliate'];
        $cookie_name = "affiliateLink";
        $cookie_value = $_GET['affiliate'];
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

        require_once '/var/www/html/classes/affiliate/affiliates.php';
        $affilaites = new affiliates();
        $ip = get_client_ip_env();
        $affilaites->createClick($_GET['affiliate'], $ip);

    }
}
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <?php
        $title = "Handy Matcher | Moving and Home Improvement";
        $description = "Handy Matcher | Moving and Home Improvement. It is the simplest way to find and book top-rated local home services. Connect with trusted home repair and improvement contractors.";
        require_once '/var/www/html/tpl/head.php';
    ?>
    <link rel="stylesheet" href="https://handymatcher.com/css/style.css">
</head>

<body>
    <?php require_once($fullPath."tpl/header.php")?>
    <main>
      <div class="main-container">
        <div class="container">
          <div class="row">
            <div class="col-12 heroContainer">
              <div class="hero">
                <h1>We help you run your business better</h1>
                <h2>Handy Matcher is the all-in-one marketing and management
                    solution built exclusively for home remodeling and design professionals.
                </h2>
                <br><br>
                <a class="btn btn-warning"
                  href="https://handymatcher.com/signup.php">Create a free account
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <div class="dark-background"><br><br>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h4>Tools custom-built for your business</h4>
            <h6>
              We know how you work. So we designed software and services
              to help industry professionals from <br />
              designers to contractors manage and grow their businesses
              with ease.
            </h6>
          </div>
        </div>
      </div><br><br>
    </div><br><br>

      <!-- BOXES -->

    <section id="home-icons"><br><br>
      <div class="container-fluid myRows">
        <div class="grey">
          <div class="container">
            <div class="row">
              <div class="col-md-6 mb-4 text-center" id="aka-1"><br><br>
                <h5 class="h-5">Let us introduce you to your next client</h5>
                <p class="p-1">Reach homeowners in your desired markets
                  with the help of our targeted advertising
                  initiatives. Build name recognition,
                  howcase your finished projects, and get
                  matched with prospective clients based on
                  budget, style, and location.
                </p>

              </div>
              <div class="col-md-6">
                <img class="center-image img-fluid" src="images/3o.jpg">
              </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img class="center-image img-fluid" src="images/1o.jpg"/>
          </div>
          <div class="col-md-6">
            <h5 class="h-5">Everything your business needs in one central place</h5>
            <p class="p-1">Our innovative easy-to-use tools live in just
              one platform, saving your time and
              streamlining your work. Make admin tasks
              more efficient, get paid faster and
              collaborate more seamlessly.
            </p>
          </div>
        </div>
      </div>

      <div class="grey">
        <div class="container">
          <div class="row">
            <div class="col-md-6 mb-4 text-center" id="aka-1">
              <br><br>
              <h5 class="h-5">An easier way to turn more leads into clients</h5>
              <p class="p-1">We understand that leads don't pay the bills
              - clients do. Our lead management,
              collaboration and tracking software helps
              you grow your client roster through easy-to-
              use tools your team can use.</h6>
            </div>
            <div class="col-md-6">
              <img class="center-image img-fluid" src="images/2o.jpg"/>
            </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <img class="center-image img-fluid" src="images/4o.jpg"/>
        </div>
        <div class="col-md-6">
          <h5 class="h-5">We've got your back<h5>
          <p class="p-1">Our friendly team of industry specialists will
          help you make the most of our business
          tools and marketing services. We're focused
          on your success!</p>
        </div>
      </div>
    </div><br><br><br><br>
    <div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-4 text-center" id="aka-1">
            <h1>Learn why industry leaders choose HandyMatcher</h1><br><br>
            <div class="embed-wrapper">
              <iframe width="" height=""
                src="https://www.youtube.com/embed/ef0VHTPzWV8?rel=0&modestbranding=0&loop=0&controls=1&autoplay=1&fs=0&showinfo=0&color=Red&vq=720p"
                frameborder="0" allowfullscreen>
              </iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="dark-background">
      <div class="container">
        <div class="row"><br><br>
          <div class="col-md-12 mb-4 text-center" id="aka-1">
            <h3>Ready to get started?</h3><br><br>
            <div class="bottom-demo-button-container">
              <a class="btn btn-warning" href="https://handymatcher.com/pro.php">Get your Free Account</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<?php require_once($fullPath."tpl/footer.php")?>
<script src="https://use.fontawesome.com/7e5b532868.js"></script>
<script>
    function moveToForm(){
        window.location.href = "https://handymatcher.com/moving";
    }
    function openService(id){
        switch(id){
            case "":
                break;
            case '1':
                window.location.href = "https://handymatcher.com/localMoving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                break;
            case '2':
                window.location.href = "https://handymatcher.com/longDistanceMoving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                break;
            case '3':
                window.location.href = "https://handymatcher.com/moving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                break;
            case '4':
                window.location.href = "https://handymatcher.com/moving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                break;
            default:
                alert("in development check back soon!");
                break;
        }
    }
  </script>
</body>
</html>
