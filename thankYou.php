<?php
require_once '/var/www/html/config/database.php';
use Illuminate\Database\Capsule\Manager as DB;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function get_client_ip_env() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}
function createLeadRecord($array)
{
    DB::table('leads')->insertGetId($array);
}

createLeadRecord($_POST);
if (isset($_POST['phone']) && $_POST['phone'] && trim($_POST['phone']) && $_POST['phone'] != 0000000000 && $_POST['phone'] != 1111111111 && $_POST['phone'] != 2222222222 && $_POST['phone'] != 3333333333 && $_POST['phone'] != 4444444444 && $_POST['phone'] != 5555555555 && $_POST['phone'] != 6666666666 && $_POST['phone'] != 7777777777 && $_POST['phone'] != 8888888888 && $_POST['phone'] != 9999999999){

}else{
    die("Error Please Enter a real phone number, please go back and try again");
}
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    $isMobile = true;
}else{
    $isMobile = false;
}






$fullPath = "/var/www/html/";
if($_SERVER['HTTP_HOST'] == "localhost"){$fullPath = "";}

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <?php 
        $title = "Handy Matcher | Thank you for choosing us";
        $description = "Handy Matcher | Moving and Home Improvement. It is the simplest way to find and book top-rated local home services. Connect with trusted home repair and improvement contractors.";
        require_once '/var/www/html/tpl/head.php';
    ?>
    <link rel="stylesheet" href="https://handymatcher.com/css/site.css">
</head>

<body>
    <?php require_once($fullPath."tpl/header.php")?>
    <section id="progressBarContainer">
        <div class="row">
            <div class="col-11">
                <div class="progress">
                    <div id="progressBar" class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <div class="col-1">
                <div id="percentage">100%</div>
            </div>
        </div>
    </section>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="myBox">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h3><?= $_POST['service'] ?></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <h5>Thank you <?= $_POST['fullName'] ?>, The best moving companies in your area will be in contact with you soon!</h5>
<!--                                <h5>Your Total Moving Distance Is: <?= number_format($zipRes['DistanceInMiles'],".",""); ?> Miles / <?= number_format($zipRes['DistanceInKm'],".",""); ?> Km</h5>-->
                            </div>
                        </div>
                        <div class="stepContainer">
                            <div class="row customBox">
                                <div class="col-12 text-center">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php require_once($fullPath."tpl/footer.php")?>
    <script src="https://use.fontawesome.com/7e5b532868.js"></script>
    <script>
        window.onscroll = function() {checkForSticky()};
        var progressBarContainer = document.getElementById("progressBarContainer");
        var sticky = progressBarContainer.offsetTop;
        function checkForSticky() {
          if (window.pageYOffset > sticky) {
            progressBarContainer.classList.add("sticky");
          } else {
            progressBarContainer.classList.remove("sticky");
          }
        }
    </script>
    <?php
    require_once '/var/www/html/tpl/thankYou.php';
    ?>

</body>
</html>

