<?php
require_once '/var/www/html/admin/classes/server/DB.php';

class movingDir extends DB {
    function __construct() {
        parent::__construct();
    }
    
    public function getData($page = 0){
        
        
        $res = [];
        $res['status'] = false;
        $res['data'] = false;
        if($page > 0){
            $page = $page * 10;
        }
        try{
            $stmt = $this->conn->prepare("SELECT * FROM directory.moving WHERE isDeleted=0 AND url IS NOT null ORDER BY id ASC LIMIT :page,10");
            $stmt->bindParam(':page', $page, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($data){
                foreach($data as &$item){
                    $item['url'] = strtolower($item['url']);
                }
                $res['status'] = true;
                $res['data'] = $data;

            }else{

                $res['status'] = false;
                $res['data'] = "Empty";

            }
        }  catch (PDOException $e){

            $res['status'] = false;
            $res['data'] = $e;
            
        }
        
        return $res;
    }
    public function getAllData(){
       
        try{
            $stmt = $this->conn->prepare("SELECT companyname FROM directory.moving WHERE isDeleted=0 ORDER BY id ASC");
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($data){

                return $data;

            }else{


            }
        }  catch (PDOException $e){

           
            
        }
        
    }
    
    public function searchData($query){
        
        
        $res = [];
        $res['status'] = false;
        $res['data'] = false;
        
        try{
            $stmt = $this->conn->prepare("SELECT * FROM directory.moving WHERE companyname LIKE :query AND isDeleted=0 ORDER BY id ASC");
            $q = "%".$query."%";
            $stmt->bindParam(':query', $q, PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($data){
                foreach($data as &$item){
                    $item['url'] = strtolower($item['url']);
                }
                $res['status'] = true;
                $res['data'] = $data;

            }else{

                $res['status'] = false;
                $res['data'] = "Empty";

            }
        }  catch (PDOException $e){

            $res['status'] = false;
            $res['data'] = $e;
            
        }
        
        return $res;
    }
    public function getCompany($query){
        
        
        $res = [];
        $res['status'] = false;
        $res['data'] = false;
        
        try{
            $stmt = $this->conn->prepare("SELECT * FROM directory.moving WHERE url=:query AND isDeleted=0");
            
            $stmt->bindParam(':query', $query, PDO::PARAM_STR);
            
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            if($data){

                $res['status'] = true;
                $res['data'] = $data;

            }else{

                $res['status'] = false;
                $res['data'] = "Empty";

            }
        }  catch (PDOException $e){

            $res['status'] = false;
            $res['data'] = $e;
            
        }
        
        return $res;
    }
    
}