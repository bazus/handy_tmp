<?php
require_once '/var/www/html/movingDir/sitemap.php';
require_once 'vendor/autoload.php';
require_once 'config/database.php';

use Illuminate\Database\Capsule\Manager as DB;


$clients = DB::table('moving')->get();

?>
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<url><loc>https://handymatcher.com/</loc><changefreq>always</changefreq><priority>1.00</priority></url>
<url><loc>https://handymatcher.com/</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://handymatcher.com/directory</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<?php foreach( $clients as $client ){ ?>
<url><loc>https://handymatcher.com/company/<?=$client->url?></loc><changefreq>always</changefreq><priority>0.80</priority></url>
<?php } ?>
<url><loc>https://handymatcher.com/partners</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://handymatcher.com/privacy</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://handymatcher.com/moving/</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://handymatcher.com/localMoving</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://handymatcher.com/longDistanceMoving</loc><changefreq>always</changefreq><priority>0.85</priority></url>

</urlset>
 
