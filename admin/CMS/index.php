<?php
$pagPage = 1;
session_start();
if(isset($_GET['page'])){
    $pagPage = $_GET['page'];
    $_SESSION['page'] = $pagPage;
}
if(isset($_SESSION['page'])){
    $pagPage = $_SESSION['page'];
}
?>
<html>
    <head>
        <title>CMS</title>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/7e5b532868.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <style>
            body{
                background: #2d3243 !important;
                color: #fff !important;
            }
            .logo{
                color: #fff;
            }
            header{
                padding: 15px;
            }
            table.table{
                color:white;
            }
            .page-link{
                background-color: #2d3243;
            }
            .page-link:hover{
                background-color: #2d3245;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <a class="logo" href="https://handymatcher.com/admin/CMS/"><h3>CMS Page-<span id="pageID"><?= $pagPage ?></span></h3></a>
                    </div>
                    <div class="col-4"></div>
                    <div class="col-4">
                        <div class="input-group">
                            <input id="searchBar" class="form-control" type="text" placeholder="Search...">
                            <div class="input-group-append">
                                <button class="btn btn-primary" onclick="search()"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <main>
            <div class="container">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Company Name</th>
                                    <th>Actions <button class="btn btn-info pull-right" onclick="window.location.href = 'https://handymatcher.com/admin/CMS/create.php'"><i class="fa fa-plus"></i></button></th>
                                </tr>
                            </thead>
                            <tbody id="tableData">
                                
                            </tbody>
                            <tfoot id="nav">
                                <tr>
                                    <td colspan="3">
                                    <nav style="width: fit-content;display: block;margin: 0 auto;">
                                        <?php if($pagPage == 1){ ?>
                                            <small>Showing <b>1 - 10</b> Results out of <b>6996</b> Companies</small>
                                        <?php }else{ ?>
                                            <?php if($pagPage == 700){ ?>
                                                <small>Showing <b>6991-6996</b> Results out of <b>6996</b> Companies</small>
                                            <?php }else{ ?>
                                                <small>Showing <b><?= ($pagPage-1)*10+1 ." - ".(($pagPage-1)*10+10); ?></b> Results out of <b>6996</b> Companies</small>
                                            <?php } ?>
                                        <?php } ?>
                                        <ul class="pagination">
                                            <?php if ($pagPage > 1){ ?>
                                            <li class="page-item"><a class="page-link"  href="index.php?page=<?= $pagPage-1 ?>" href="#">Previous</a></li>
                                            <?php }else{ ?>
                                            <li class="page-item"><a class="page-link">Previous</a></li>
                                            <?php } ?>
                                            <?php
                                            if($pagPage < 6){
                                                $pagination = 10;
                                                $start = 1;
                                            }else{
                                                $pagination = $pagPage+5;
                                                $start = $pagination-10;
                                            }
                                            if($pagination > 701){
                                                $pagination = 701;
                                            }
                                            for($i = $start; $i<$pagination;$i++){?>
                                            <li class="page-item <?php if($pagPage == $i){echo 'active';}?>"><a class="page-link" href="index.php?page=<?= $i ?>"><?= $i ?></a></li>
                                            <?php } ?>
                                            <li class="page-item"><a class="page-link" href="index.php?page=<?= $pagPage+1 ?>">Next</a></li>
                                        </ul>
                                      </nav>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </main>
        <script>
            var page = <?= $pagPage-1 ?>;
            function changePage(action,num){
                if(num != undefined){
                    page = (num-1);
                    $("#pageID").text(page+1);
                    getData();
                    return 1;
                }
                if(action == "-"){
                    if(page > 0){
                        page--;
                    }
                }else{
                    page++;
                }
                $("#pageID").text(page+1);
                getData();
            }
            function getData(){
                var container = document.getElementById("tableData");
                container.innerHTML = "";
                $.ajax({
                    url: "https://handymatcher.com/admin/CMS/actions/getCompanies.php",
                    method:"POST",
                    data:{
                        page:page
                    },
                    async: true
                }).done(function (data) {
                    try{
                        data = JSON.parse(data);
                        if(data['status'] == true){
                            for(var i = 0; i < data['data'].length; i++){
                                setData(data['data'][i],container);
                            }
                        }else{
                            alert(data['data']);
                        }
                    }catch(e){
                        alert(e);
                    }
                });
            }
            function search(){
                $("#nav").remove();
                var container = document.getElementById("tableData");
                container.innerHTML = "";
                var search = $("#searchBar").val();
                $.ajax({
                    url: "https://handymatcher.com/admin/CMS/actions/searchCompanies.php",
                    method:"POST",
                    data:{
                        search:search
                    },
                    async: true
                }).done(function (data) {
                    try{
                        data = JSON.parse(data);
                        if(data['status'] == true){
                            for(var i = 0; i < data['data'].length; i++){
                                setData(data['data'][i],container);
                            }
                        }else{
                            alert(data['data']);
                        }
                    }catch(e){
                        alert(e);
                    }
                });
            }
            function setData(d,c){
                
                var x = document.createElement("tr");
                
                var y = document.createElement("td");
                if(d.url){
                    y.style.color = "green";
                }
                y.innerText = d.id;
                x.appendChild(y);
                
                var y = document.createElement("td");
                y.innerText = d.companyname;
                x.appendChild(y);
                
                var y = document.createElement("td");
                y.innerHTML = "<button class='btn btn-danger' onclick='deleteItem("+d.id+")' style='margin-right:15px;'>Delete</button><button class='btn btn-warning' onclick='edit("+d.id+")'>Edit</button>";
                x.appendChild(y);
                
                c.appendChild(x);
            }
            function edit(id){
                location.href = "https://handymatcher.com/admin/CMS/edit.php?id="+id;
            }
            function deleteItem(id){
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this company",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((willDelete) => {
                    if (willDelete) {
                      $.ajax({
                            url: "https://handymatcher.com/admin/CMS/actions/delete.php",
                            method:"POST",
                            data:{
                                id:id
                            },
                            async: true
                        }).done(function (data) {
                            data = JSON.parse(data);
                            getData();
                        });
                    } else {
                      swal("The company is safe!");
                    }
                  });
            }
            getData();
        </script>
    </body>
</html>
