<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>
<html>
    <head>
        <title>CMS</title>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <!-- Include Editor style. -->
        <link href='https://cdn.jsdelivr.net/npm/froala-editor@3.0.6/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />

        <!-- Include JS file. -->
        <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.0.6/js/froala_editor.pkgd.min.js'></script>
        
        <style>
            body{
                background: #2d3243 !important;
                color: #fff !important;
            }
            .logo{
                color: #fff;
            }
            header{
                padding: 15px;
            }
            table.table{
                color:white;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="container">
                <div class="col-12">
                    <a class="logo" href="https://handymatcher.com/admin/CMS/"><h3>CMS - Create New</h3></a>
                </div>
            </div>
        </header>
        <main>
            <div class="container">
                <div class="col-12">
                    <div class="form-group">
                        <label>companyname</label>
                        <input type="text" class='form-control' id="companyname" value="">
                    </div>
                    <div class="form-group">
                        <label>description</label>
                        <textarea rows="4" class='form-control' id="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>url</label>
                        <input type="text" class='form-control' id="url" value="">
                    </div>
                    <div class="form-group">
                        <label>email</label>
                        <input type="text" class='form-control' id="email" value="">
                    </div>
                    <div class="form-group">
                        <label>phone</label>
                        <input type="text" class='form-control' id="phone" value="">
                    </div>
                    <div class="form-group">
                        <label>firstname</label>
                        <input type="text" class='form-control' id="firstname" value="">
                    </div>
                    <div class="form-group">
                        <label>lastname</label>
                        <input type="text" class='form-control' id="lastname" value="">
                    </div>
                    <div class="form-group">
                        <label>usdot</label>
                        <input type="text" class='form-control' id="usdot" value="">
                    </div>
                    <div class="form-group">
                        <label>youtube</label>
                        <input type="text" class='form-control' id="youtube" value="">
                    </div>
                    <div class="form-group">
                        <label>website</label>
                        <input type="text" class='form-control' id="website" value="">
                    </div>
                    <div class="form-group">
                        <label>logo</label>
                        <input type="text" class='form-control' id="logo" value="">
                    </div>
                    <div class="form-group">
                        <label>state</label>
                        <input type="text" class='form-control' id="state" value="">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-primary" onclick="create()">Create</button>
                    </div>
                </div>
            </div>
        </main>
        <script>
            
            function create(){
                var arr = {
                    companyname: $("#companyname").val(),
                    description: $("#description").val(),
                    url: $("#url").val(),
                    email: $("#email").val(),
                    phone: $("#phone").val(),
                    firstname: $("#firstname").val(),
                    lastname: $("#lastname").val(),
                    usdot: $("#usdot").val(),
                    youtube: $("#youtube").val(),
                    website: $("#website").val(),
                    logo: $("#logo").val(),
                    state: $("#state").val()
                };
                $.ajax({
                    url: "https://handymatcher.com/admin/CMS/actions/create.php",
                    method:"POST",
                    data:{
                        arr:arr
                    },
                    async: true
                }).done(function (data) {
                    try{
                        data = JSON.parse(data);
                        //window.location.href = "https://handymatcher.com/admin/CMS/";
                    }catch(e){
                        alert(e);
                    }
                });
            }
            
        </script>
    </body>
</html>
