<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once '/var/www/html/admin/CMS/classes/companies.php';
$c = new companies();
$data = $c->getDataByID($_GET['id']);

?>
<html>
    <head>
        <title>CMS</title>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <style>
            body{
                background: #2d3243 !important;
                color: #fff !important;
            }
            .logo{
                color: #fff;
            }
            header{
                padding: 15px;
            }
            table.table{
                color:white;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="container">
                <div class="col-12">
                    <a class="logo" href="https://handymatcher.com/admin/CMS/"><h3>CMS Edit-<?= $data['data'][0]['companyname'] ?></h3></a>
                </div>
            </div>
        </header>
        <main>
            <div class="container">
                <div class="col-12">
                    <div class="form-group">
                        <label>companyname</label>
                        <input type="text" class='form-control' id="companyname" value="<?= $data['data'][0]['companyname'] ?>">
                    </div>
                    <div class="form-group">
                        <label>description</label>
                        <textarea rows="4" class='form-control' id="description"><?= $data['data'][0]['description'] ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>URL</label>
                        <input type="text" class='form-control' id="url" value="<?= $data['data'][0]['url'] ?>">
                    </div>
                    <div class="form-group">
                        <label>email</label>
                        <input type="text" class='form-control' id="email" value="<?= $data['data'][0]['email'] ?>">
                    </div>
                    <div class="form-group">
                        <label>phone</label>
                        <input type="text" class='form-control' id="phone" value="<?= $data['data'][0]['phone'] ?>">
                    </div>
                    <div class="form-group">
                        <label>firstname</label>
                        <input type="text" class='form-control' id="firstname" value="<?= $data['data'][0]['firstname'] ?>">
                    </div>
                    <div class="form-group">
                        <label>lastname</label>
                        <input type="text" class='form-control' id="lastname" value="<?= $data['data'][0]['lastname'] ?>">
                    </div>
                    <div class="form-group">
                        <label>usdot</label>
                        <input type="text" class='form-control' id="usdot" value="<?= $data['data'][0]['usdot'] ?>">
                    </div>
                    <div class="form-group">
                        <label>youtube</label>
                        <input type="text" class='form-control' id="youtube" value="<?= htmlspecialchars($data['data'][0]['youtube']) ?>">
                    </div>
                    <div class="form-group">
                        <label>website</label>
                        <input type="text" class='form-control' id="website" value="<?= $data['data'][0]['website'] ?>">
                    </div>
                    <div class="form-group">
                        <label>logo</label>
                        <input type="text" class='form-control' id="logo" value="<?= $data['data'][0]['logo'] ?>">
                    </div>
                    <div class="form-group">
                        <label>state</label>
                        <input type="text" class='form-control' id="state" value="<?= $data['data'][0]['state'] ?>">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-primary" onclick="update()">Update</button>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger" style="display:block;margin: 5px auto;" onclick="deleteItem()">Delete</button>
                    </div>
                </div>
            </div>
        </main>
        <script>
            function update(){
                var arr = {
                    companyname: $("#companyname").val(),
                    description: $("#description").val(),
                    url: $("#url").val(),
                    email: $("#email").val(),
                    phone: $("#phone").val(),
                    firstname: $("#firstname").val(),
                    lastname: $("#lastname").val(),
                    usdot: $("#usdot").val(),
                    youtube: $("#youtube").val(),
                    website: $("#website").val(),
                    logo: $("#logo").val(),
                    state: $("#state").val()
                };
                
                $.ajax({
                    url: "https://handymatcher.com/admin/CMS/actions/edit.php",
                    method:"POST",
                    data:{
                        arr:arr,
                        id: <?= $_GET['id'] ?>
                    },
                    async: true
                }).done(function (data) {
                    try{
                        data = JSON.parse(data);
                        window.location.reload();
                    }catch(e){
                        alert(e);
                    }
                });
            }
            function deleteItem(){
            var id = <?= $_GET['id']; ?>;
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this company",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((willDelete) => {
                    if (willDelete) {
                      $.ajax({
                            url: "https://handymatcher.com/admin/CMS/actions/delete.php",
                            method:"POST",
                            data:{
                                id:id
                            },
                            async: true
                        }).done(function (data) {
                            data = JSON.parse(data);
                            getData();
                        });
                    } else {
                      swal("The company is safe!");
                    }
                  });
            }
        </script>
    </body>
</html>
