<?php
require_once '/var/www/html/admin/classes/server/DB.php';

class companies extends DB{
    
    function __construct() {
        parent::__construct();
    }
    
    public function getData($page = 0){
        
        
        $res = [];
        $res['status'] = false;
        $res['data'] = false;
        if($page > 0){
            $page = $page * 10;
        }
        try{
            $stmt = $this->conn->prepare("SELECT id,companyname,url FROM directory.moving WHERE isDeleted=0 ORDER BY id ASC LIMIT :page,10");
            $stmt->bindParam(':page', $page, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($data){
                foreach($data as &$item){
                    $item['url'] = strtolower($item['url']);
                }
                $res['status'] = true;
                $res['data'] = $data;

            }else{

                $res['status'] = false;
                $res['data'] = "Empty";

            }
        }  catch (PDOException $e){

            $res['status'] = false;
            $res['data'] = $e;
            
        }
        
        return $res;
    }
    public function searchData($search){
        
        
        $res = [];
        $res['status'] = false;
        $res['data'] = false;
        
        try{
            $stmt = $this->conn->prepare("SELECT id,companyname FROM directory.moving WHERE isDeleted=0 AND (companyname LIKE :search OR usdot=:cleanSearch) ORDER BY id ASC");
            $cleanSearch = $search;
            $search = "%".$search."%";
            $stmt->bindParam(':search', $search, PDO::PARAM_STR);
            $stmt->bindParam(':cleanSearch', $cleanSearch, PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($data){

                $res['status'] = true;
                $res['data'] = $data;

            }else{

                $res['status'] = false;
                $res['data'] = "Empty";

            }
        }  catch (PDOException $e){

            $res['status'] = false;
            $res['data'] = $e;
            
        }
        
        return $res;
    }
    public function getDataByID($id){
        
        
        $res = [];
        $res['status'] = false;
        $res['data'] = false;
        
        try{
            $stmt = $this->conn->prepare("SELECT * FROM directory.moving WHERE isDeleted=0 AND id=:id");
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($data){
                foreach($data as &$item){
                    $item['url'] = strtolower($item['url']);
                }
                $res['status'] = true;
                $res['data'] = $data;

            }else{

                $res['status'] = false;
                $res['data'] = "Empty";

            }
        }  catch (PDOException $e){

            $res['status'] = false;
            $res['data'] = $e;
            
        }
        
        return $res;
    }
    
    public function delete($id){
        $stmt = $this->conn->prepare("UPDATE directory.moving SET isDeleted=1 WHERE id=:id");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function update($id,$arr){
        try{
            foreach($arr as &$a){
                if(!$a){
                    $a = "";
                }
            }
            $stmt = $this->conn->prepare("UPDATE directory.moving SET companyname=:companyname,url=:url,email=:email,phone=:phone,firstname=:firstname,lastname=:lastname,usdot=:usdot,youtube=:youtube,website=:website,logo=:logo,state=:state,description=:description WHERE id=:id");
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->bindParam(':companyname', $arr['companyname'], PDO::PARAM_STR);
            $stmt->bindParam(':url', $arr['url'], PDO::PARAM_STR);
            $stmt->bindParam(':email', $arr['email'], PDO::PARAM_STR);
            $stmt->bindParam(':phone', $arr['phone'], PDO::PARAM_STR);
            $stmt->bindParam(':firstname', $arr['firstname'], PDO::PARAM_STR);
            $stmt->bindParam(':lastname', $arr['lastname'], PDO::PARAM_STR);
            $stmt->bindParam(':usdot', $arr['usdot'], PDO::PARAM_STR);
            $stmt->bindParam(':youtube', $arr['youtube'], PDO::PARAM_STR);
            $stmt->bindParam(':website', $arr['website'], PDO::PARAM_STR);
            $stmt->bindParam(':logo', $arr['logo'], PDO::PARAM_STR);
            $stmt->bindParam(':state', $arr['state'], PDO::PARAM_STR);
            $stmt->bindParam(':description', $arr['description'], PDO::PARAM_STR);

            $stmt->execute();
            
            return true;
        } catch (PDOException $e){
            return $e;
        }
    }

    public function create($arr){
        try{
            foreach($arr as &$a){
                if(!$a){
                    $a = "";
                }
            }
            $stmt = $this->conn->prepare("INSERT INTO directory.moving (companyname,url,email,phone,firstname,lastname,usdot,youtube,website,logo,state,description) VALUES (:companyname,:url,:email,:phone,:firstname,:lastname,:usdot,:youtube,:website,:logo,:state,:description)");
            
            $stmt->bindParam(':companyname', $arr['companyname'], PDO::PARAM_STR);
            $stmt->bindParam(':email', $arr['email'], PDO::PARAM_STR);
            $stmt->bindParam(':url', $arr['url'], PDO::PARAM_STR);
            $stmt->bindParam(':phone', $arr['phone'], PDO::PARAM_STR);
            $stmt->bindParam(':firstname', $arr['firstname'], PDO::PARAM_STR);
            $stmt->bindParam(':lastname', $arr['lastname'], PDO::PARAM_STR);
            $stmt->bindParam(':usdot', $arr['usdot'], PDO::PARAM_STR);
            $stmt->bindParam(':youtube', $arr['youtube'], PDO::PARAM_STR);
            $stmt->bindParam(':website', $arr['website'], PDO::PARAM_STR);
            $stmt->bindParam(':logo', $arr['logo'], PDO::PARAM_STR);
            $stmt->bindParam(':state', $arr['state'], PDO::PARAM_STR);
            $stmt->bindParam(':description', $arr['description'], PDO::PARAM_STR);

            $stmt->execute();
            
            return true;
        } catch (PDOException $e){
            return $e;
        }
    }

}
