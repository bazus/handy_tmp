<html>
    <head>
        <title>Test</title>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="accordion" id="accordionExample" style="margin-top: 25px;">


                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Affiliates Users
                      </button>
                    </h2>
                  </div>

                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Link</th>
                                        <th>Join Date</th>
                                    </tr>
                                </thead>
                                <tbody id="affiliatesTable">

                                </tbody>
                            </table>
                        </div>
                    </div>
                  </div>
                </div>


                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Affiliates Clicks
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>Affiliate Name</th>
                                        <th>Click Date</th>
                                        <th>Clicker IP</th>
                                    </tr>
                                </thead>
                                <tbody id="affiliatesClicksTable">

                                </tbody>
                            </table>
                        </div>
                    </div>
                  </div>
                </div>


              </div>
        </div>
        <script>
            $(document).ready(function(){
                getAffiliatesData();
                getAffiliatesClicksData();
            });
            function getAffiliatesData(){
                var container = document.getElementById("affiliatesTable");
                $.ajax({
                    url: "https://handymatcher.com/admin/actions/getAffiliatesData.php",
                    async: true
                }).done(function (data) {
                    try{
                        data = JSON.parse(data);
                        if(data['status'] == true){
                            for(var i = 0; i < data['data'].length; i++){
                                setAffiliatesTable(data['data'][i],container);
                            }
                        }else{
                            alert(data['data']);
                        }
                    }catch(e){
                        alert(e);
                    }
                });
            }
            function setAffiliatesTable(d,c){
                
                var x = document.createElement("tr");
                
                var y = document.createElement("td");
                y.innerText = d.fullName;
                x.appendChild(y);
                
                var y = document.createElement("td");
                y.innerText = d.customLink;
                x.appendChild(y);
                
                var y = document.createElement("td");
                y.innerText = d.dateCreated;
                x.appendChild(y);
                
                
                c.appendChild(x);
            }
         
            function getAffiliatesClicksData(){
                var container = document.getElementById("affiliatesClicksTable");
                $.ajax({
                    url: "https://handymatcher.com/admin/actions/getAffiliatesClicksData.php",
                    async: true
                }).done(function (data) {
                    try{
                        data = JSON.parse(data);
                        if(data['status'] == true){
                            for(var i = 0; i < data['data'].length; i++){
                                setAffiliatesClicksTable(data['data'][i],container);
                            }
                        }else{
                            alert(data['data']);
                        }
                    }catch(e){
                        alert(e);
                    }
                });
            }
            function setAffiliatesClicksTable(d,c){
                console.log(d);
                var x = document.createElement("tr");
                
                var y = document.createElement("td");
                y.innerText = d.fullName;
                x.appendChild(y);
                
                var y = document.createElement("td");
                y.innerText = d.date;
                x.appendChild(y);
                
                var y = document.createElement("td");
                y.innerText = d.ip;
                x.appendChild(y);
                
                c.appendChild(x);
            }
        </script>
    </body>
</html>
