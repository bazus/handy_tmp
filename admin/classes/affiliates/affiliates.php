<?php
require_once("/var/www/html/admin/classes/server/DB.php");

class affiliates extends DB {
    
    function __construct() {
        parent::__construct();
    }
    
    public function getAffiliatesData(){
        
        $res = [];
        $res['status'] = false;
        $res['data'] = false;
        
        try{
            $stmt = $this->conn->prepare("SELECT * FROM joseianq_myApi.affiliates ORDER BY id DESC");
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($data){
                $res['status'] = true;
                $res['data'] = $data;
            }else{
                $res['status'] = false;
                $res['data'] = "Empty Table";
            }
        } catch (PDOException $e){      
            $res['status'] = false;
            $res['data'] = $e;
        }
        
        return $res;
        
    }
    
    public function getAffiliatesClicksData(){
        
        $res = [];
        $res['status'] = false;
        $res['data'] = false;
        
        try{
            $stmt = $this->conn->prepare("SELECT AC.*,A.fullName FROM joseianq_myApi.affiliatesClicks AS AC INNER JOIN joseianq_myApi.affiliates AS A ON A.customLink = AC.affLink ORDER BY AC.id DESC");
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($data){
                $res['status'] = true;
                $res['data'] = $data;
            }else{
                $res['status'] = false;
                $res['data'] = "Empty Table";
            }
        } catch (PDOException $e){      
            $res['status'] = false;
            $res['data'] = $e;
        }
        
        return $res;
        
    }
    
}
