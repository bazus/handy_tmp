﻿<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    $isMobile = true;
}else{
    $isMobile = false;
}
if (isset($_GET['ref'])){$ref = $_GET['ref'];}else{
    if (isset($_GET['r'])){
        $ref = $_GET['r']." | Handy Matcher";
    }else{
        $ref = "Organic"." | Handy Matcher";
    }
}
$fullPath = "/var/www/html/";
if($_SERVER['HTTP_HOST'] == "localhost"){$fullPath = "";}

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <?php 
        $title = "Handy Matcher | Affiliates Program";
        $description = "Handy Matcher | Moving and Home Improvement. It is the simplest way to find and book top-rated local home services. Connect with trusted home repair and improvement contractors.";
        require_once '/var/www/html/tpl/head.php';
    ?>
    <link rel="stylesheet" href="https://handymatcher.com/css/site.css">
    <style>
        main{
            padding-top: 0;
        }
        .my-custom-control {
            width: 320px;
        }
        h3,h4{
            font-weight: bold;
        }
    </style>
</head>

<body>
    <?php require_once($fullPath."tpl/header.php")?>
    <main>
        <div class="container-fluid" style="padding-bottom:25px;">
            <div class="row" style="background: url('https://handymatcher.com/images/affShowcase.jpg');background-repeat: no-repeat;background-size: cover;box-shadow: inset 50px 50px 500px 50px #ffffff;">
                <div class="container">
                    <div class="col-12 text-left" style="padding: 250px 0px 250px;">
                        <div style="background: rgba(255,255,255,0.7);width: fit-content;padding: 50px;border-radius: 25px;">
                            <h3 style="font-weight: 1000;">Affiliate Program for Moving<br> and Home Improvement Leads </h3>
                        <p style="line-height: 1.1;font-weight: 600;font-size: 0.8rem;">
                            Join now and start earning referral revenue on all moving leads you<br> generate! Our relocation affiliate program is easy to join, easy to set-up,<br> and most importantly, it sells!
                        </p>
                        <a class="btn btn-lg btn-custom" style="margin-top:15px;" href="https://handymatcher.com/affiliates/register">Start Earning Today</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="myBox">
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    Immediate profits:
                                </h4>
                                <ul>
                                    <li>
                                        Signing up as an affiliate is fast and easy!
                                    </li>
                                    <li>
                                        Use our ready-to-embed code, start earning commissions right away – no design or programming skills required (and you will get a free phone and email support too).
                                    </li>
                                </ul>
                                <h4>
                                    Great commissions:
                                </h4>
                                <ul>
                                    <li>
                                        Earn a percentage of revenue of all approved moving leads.
                                    </li>
                                    <li>
                                        We’ll do all the work, maintenance and reporting for you, you just maintain your Web site and collect commissions.
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    Many lead forms to choose from:
                                </h4>
                                <ul>
                                    <li>
                                        Big moving quote forms with 4 steps for a detailed move information collection.
                                    </li>
                                    <li>
                                        Short easy to fill in moving estimate forms.
                                    </li>
                                    <li>
                                        Just log in to monitor what you are going to collect at the end of the month.
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h3>How does it work?</h3>
                                <p>
                                    When a visitor fills in a lead form, we track your site’s referrer of the moving lead, we calculate your commission and we provide you with real-time calculation of your commission in your account. Then you collect your payment at the end of the month.
                                    To become a moving Referral Partner or if you just want to receive more information, please <a href="https://handymatcher.com/affiliates/register">Click Here</a>
                                    It is so easy, it takes just a few seconds to complete.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h3>Who can join this moving leads affiliate program?</h3>
                                <p>
                                    At Handy Matcher we partner with some of the World’s largest websites, blogs, and apps for moving as well as small relocation blogs (see our current partners list).
                                    If you are a website owner in some of these niches, please do contact us so we can show you our lucrative offers to become an affiliate partner:
                                </p>
                                <ul>
                                    <li>
                                        Moving portals and lead providers.
                                    </li>
                                    <li>
                                        Real estate related site or blog.
                                    </li>
                                    <li>
                                        Home rental site or blog.
                                    </li>
                                    <li>
                                        Moving tips website.
                                    </li>
                                    <li>
                                        Expats website.
                                    </li>
                                    <li>
                                        Immigration website.
                                    </li>
                                    <li>
                                        Home inventory website/app.
                                    </li>
                                    <li>
                                        Apartment rental websites
                                    </li>
                                    <li>
                                        Furniture website.
                                    </li>
                                </ul>
                                <p>
                                    You can generate moving leads directly from your website and supply us with them for a commission on every real lead we receive from you. Once you send us a request via the form below, we will send you some technical specs for the integration. If you don’t have programming or design skills, we will do the integration for you at no additional cost.
                                </p>
                                <p>
                                    If you currently generate quality moving leads from your site and you want to increase your revenue per lead, join our moving leads affiliate program and we will give you ideas on how to maximize your payout per lead. We work with many moving companies both domestic and international. Our system works on a bidding principle to make sure you get the best revenue per moving lead. We believe that Handy Matcher is the affiliate program for moving leads that pays the best commission in the industry.
                                    With our sophisticated lead management system, you will keep track of all the relocation leads you will generate as well as the commission that you receive.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h3>
                                    Payments
                                </h3>
                                <p>
                                    For all our affiliate programs, we issue payments around the 10th of the following month. We support Bank transfer, PayPal, ACH and Credit Card for payment methods.
                                    You will see all leads (both web leads and phone leads) clearly marked into your account with us.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h3>
                                    Start earning referral revenue!
                                </h3>
                                <ul>
                                    <li>
                                        Immediate profits.
                                    </li>
                                    <li>
                                        Great commissions.
                                    </li>
                                    <li>
                                        Many lead forms to choose from.
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4"></div>
                            <div class="col-4">
                                <a class="btn btn-block btn-info" href="https://handymatcher.com/affiliates/register">Start Earning Today</a>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php require_once($fullPath."tpl/footer.php")?>
    <script src="https://use.fontawesome.com/7e5b532868.js"></script>
    <script>
        function registerAffiliate(){
            $.ajax({
                method:"POST",
                url: "https://handymatcher.com/actions/affiliate/register.php",
                data:{ 
                    years: $("#years").val(),
                    email: $("#email").val(),
                    fullName: $("#fullName").val(),
                    website: $("#website").val(),
                    phone: $("#phone").val()
                }
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    console.log(data);
                }catch (e) {
                    console.log(data);
                    console.log(e);
                }
            });
        }
        function moveToStep2(){
            $("#step2").show();
            $('html, body').animate({scrollTop:$('#step2').position().top}, 'slow');
        }
        
    </script>
</body>
</html>
