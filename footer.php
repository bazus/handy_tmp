<style>
    .footer {
        padding: 50px 0
    }
    footer {
        padding-top: 50px;
        padding-bottom: 0px;
        text-shadow: 0 0 black;
        height: 305px;
        background-color: #CECECE
    }
    .footer-title {
        font-size: 25px;
        text-align: left;
        font-size: 16px;
        margin-bottom: 30px;
        color: #010101;
    }
    .footer-widget {
        margin-bottom: 0
    }
    .footer-bg {
        height: 305px;
        background-color: #CECECE
    }
    .footer-wrapper.footer-bg {
        background-position: 48% 50%;
        background-color: #CECECE;
        background-repeat: no-repeat;
    }
    .footer-links>li {
        margin: 0 20px 15px 0;
        display: inline-block;
        list-style: none;
        color: #010101;
    }
    .footer-links {
        margin: 0;
        padding: 0;
    }
    .footer-social {
        margin: 0;
        padding: 0;
        list-style: none;
        display: block;
    }
    ul {
        display: flex;
        flex-direction: column;
        flex-wrap:wrap;
    }
    .footer-wrapper {
        background-color: #CECECE;
        color: #010101;
        padding: 60px 0 40px;
    }
    @media screen and (max-width:768px) {
        .footer-wrapper.footer-bg {
            height: fit-content
        }
    }
    @media (max-width:575.98px) {
        .footer-wrapper.footer-bg {
            height: 470px
        }
        .footer-links li{
            margin-bottom: 30px;
        }
        .footer-social li a i{
            font-size: 38px;
        }
    }
</style>

<footer class="footer-wrapper footer-bg" style="padding-top: 50px; padding-bottom: 0px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="footer-title">Homeowner services</h3>
                            <ul class="footer-links clearfix">
                                <li><a href="https://handymatcher.com/directory">My Account</a></li>
                                <li><a href="https://handymatcher.com/affiliates">Find pros</a></li>
                                <li><a href="https://handymatcher.com/affiliates">Browse Pro Directory</a></li>
<!--                                <li><a href="#" onclick="moveToForm()">Request a Moving Estimate Now</a></li>-->
                            </ul>
                        </div>
                        <br><br><br>
                        <div class="col-md-3">
                            <h3 class="footer-title">For service professionals</h3>
                            <ul class="footer-links clearfix">
                                <li><a href="https://handymatcher.com/login">Service professional login</a></li>
                                <li><a href="https://handymatcher.com/pro.php">Join our pro network</a></li>
                                <li><a href="https://handymatcher.com/affiliates">Contractor leads</a></li>
<!--                                <li><a href="#" onclick="moveToForm()">Request a Moving Estimate Now</a></li>-->
                            </ul>
                        </div>
                        <br><br><br>
                        <div class="col-md-3">
                            <h3 class="footer-title">About HandyMatcher</h3>
                            <ul class="footer-links clearfix">
                                <li><a href="https://handymatcher.com/about.php">About the comopany</a></li>
                                <li><a href="https://handymatcher.com/contact.php">Contact</a></li>
                                <li><a href="https://handymatcher.com/contact.php">How it works</a></li>
                                <li><a href="https://handymatcher.com/contact.php">Refer a pro</a></li>
                                <li><a href="https://handymatcher.com/affiliates">Earn with us</a></li>
<!--                                <li><a href="#" onclick="moveToForm()">Request a Moving Estimate Now</a></li>-->
                            </ul>
                        </div>
                        <br><br><br>
                        <div class="col-md-3 social">
                            <h3 class="footer-title">Stay connected</h3>
                            <ul class="footer-social" style="display:flex; flex-direction:row; flex-wrap:wrap;">
<!--                                <li style="padding-left:0px; padding-right:20px;"><a href="https://twitter.com/" data-toggle="tooltip" title="Twitter" target="_blank"><i class="fa fa-twitter" style="border-color: #70c2e9;color: #70c2e9;"></i></a></li>-->
                                <li style="padding-left:20px; padding-right:20px;"><a href="https://www.facebook.com/HandyMatcher/" data-toggle="tooltip" title="Facebook" target="_blank"><i class="fa fa-facebook" style="color: #7C7C7C;"></i></a></li>
                                <li style="padding-left:20px; padding-right:20px;"><a href="https://www.instagram.com/handymatcher/" data-toggle="tooltip" title="Instagram" target="_blank"><i class="fa fa-instagram" style="color: #7C7C7C;"></i></a></li>
                                <li style="padding-left:20px; padding-right:20px;"><a href="https://www.pinterest.com/handymatcher/" data-toggle="tooltip" title="Pinterest" target="_blank"><i class="fa fa-pinterest" style="color: #7C7C7C;"></i></a></li>
                                <li style="padding-left:20px; padding-right:20px;"><a href="https://twitter.com/HandyMatcher" data-toggle="tooltip" title="Twitter" target="_blank"><i class="fa fa-twitter" style="color: #7C7C7C;"></i></a></li>
                                <li style="padding-left:20px; padding-right:20px;"><a href="https://www.youtube.com/channel/UCAOSW8TGW-CjFRCmMMBlB7A" data-toggle="tooltip" title="Youtube" target="_blank"><i class="fa fa-youtube" style="color: #7C7C7C;"></i></a></li>
<!--                                <li style="padding-left:20px; padding-right:20px;"><a href="https://www.youtube.com/" data-toggle="Youtube" title="Twitter" target="_blank"><i class="fa fa-youtube" style="border-color: #d5615c;color: #d5615c;"></i></a></li>-->
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

            <div>
                <div class="footer-widget">
                    <p class="copyright" style="color:#808080;padding-top: 23px; text-align: center;margin-bottom: 0;">© Copyright 2020 HandyMatcher.com<br><a href="https://handymatcher.com/privacy" style="color:#808080;">Privacy Policy</a></p>
                </div>
            </div>

        </div>
    </footer>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134630411-11"></script>
<script type='text/javascript'>
    window.onbeforeunload = function () {
        window.scrollTo(0, 0);
    }
  window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '701831a0598983076ba1f0cc513050290f952eb4');
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-134630411-11');
</script>
