<?php
//require_once('/var/www/html/plugins/geoplugin.class.php');
//if(!isset($_SESSION['ipCity'])){
//    $geoplugin = new geoPlugin();
//    $geoplugin->locate();
//    $_SESSION['ipCity'] = $geoplugin->city;
//    $_SESSION['ipRegion'] = $geoplugin->region;
//}

?>
<style>
    * {
        font-family: Montserrat;
    }
    header{
        height: 60px;
    }
    header .container{
        height: inherit;
    }
    header .container a{
        color: black;
    }
    header .container a:hover{
        text-decoration: none;
    }
    .header-right{
        float:right;
        padding: 7px;
        border-radius: 8%;
        margin: 10px;
        transition: 0.3s all ease-in-out;
    }
    .header-right:hover{
        background: rgba(133,133,133,0.3);
    }
    .mobileMenu{
        display: none;
    }
    .z-index-100{
	z-index: 100;
    }
    #mobileMenuContent{
        display: none;
        position: relative;
        background-color: #f9f9f9;
        width: 100%;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    #mobileMenuContent a{
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }
    .titlePhone{
        margin-top: 7px;
        font-weight: 900;
        font-size: 18px;
        color:white
    }
    @media (max-width:768px) {
        .titlePhone{
            font-size: 26px;
            margin-top: 7px;
            font-weight: 900;
            color:white
        }
        .mobileMenu{
            display: block;
        }
        .mobileHide{
            display: none;
        }
    }
</style>
<?php $siteUrl = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/"; /*
<div style="width:100%;background:black;color:white">
    <div class="container">
        <div class="col-12">
            <?php
            if($_SESSION['ipCity']){
                echo 'Handy Services In ';
                if($_SESSION['ipCity'] == $_SESSION['ipRegion']){
                    echo $_SESSION['ipCity'];
                }else{
                    echo $_SESSION['ipCity']. " " .$_SESSION['ipRegion'];
                }
            }
            ?>
<!--            <div class="text-right">
                <h2 class="titlePhone"><img style="-webkit-user-select: none;margin: auto;width: 22px;" src="https://loyalmover.nyc3.cdn.digitaloceanspaces.com/images/callUs.png"> <a id="phone" href="tel:+1 866 205 8279">866 205 8279</a></h2>
            </div>-->
        </div>
    </div>
</div>
 */?>
<header>
    <div class="container mobileHide">
        <div class="row">
            <div class="col-12">
                <a class="logo" href="https://handymatcher.com/<?php if(isset($ref)){echo '?ref='.$ref;} ?>"><img src="<?= $siteUrl ?>/images/logo.jpg"></a>
                <a class="header-right" href="https://handymatcher.com/pro.php">Join as a Pro</a>
                <a class="header-right" href="https://handymatcher.com/directory">Explore</a>
                <a class="header-right disabled" href="https://handymatcher.com/login">Log In</a>
                <a class="header-right disabled" href="https://handymatcher.com/signup">Sign Up</a>
            </div>
        </div>
    </div>
    <div class="mobileMenu container">
        <div class="row">
            <div class="col-12">
                <a class="logo" href="https://handymatcher.com/<?php if(isset($ref)){echo '?ref='.$ref;} ?>"><img src="<?= $siteUrl ?>/images/logo.jpg"></a>
                <a class="header-right" onclick="showMenu()" href="#">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 z-index-100">
                <div id="mobileMenuContent">
                    <a href="https://handymatcher.com/">Home</a>
                    <a href="https://handymatcher.com/pro.php">Join as a Pro</a>
                    <a href="https://handymatcher.com/directory">Explore</a>
                    <a class="disabled" href="https://handymatcher.com/login">Log In</a>
                    <a class="disabled" href="https://handymatcher.com/signin">Sign Up</a>
                </div>
            </div>
        </div>
    </div>
</header>
<script>
function showMenu(){
   // document.getElementById("mobileMenuContent").style.display = "block";
    var menuBox = document.getElementById('mobileMenuContent');
    if(menuBox.style.display == "block") { // if is menuBox displayed, hide it
        menuBox.style.display = "none";
    }
    else { // if is menuBox hidden, display it
        menuBox.style.display = "block";
    }
}
function hideMobileMenu(e){
    if(e.path[0].className == "fa fa-bars"){
        document.getElementById("mobileMenuContent").style.display = "block";
    }else{
        if(e.path[0].className == "fa fa-bars"){
            document.getElementById("mobileMenuContent").style.display = "block";
        }else{
            document.getElementById("mobileMenuContent").style.display = "none";
        }
    }
}
document.body.addEventListener("click", hideMobileMenu);
</script>
