<style>
    @media (max-width:768px) {
        main{
            padding-top: 25px;
        }
        hr{
            margin-top: 0.5rem;
            margin-bottom: 0.5rem;
        }
        .my-custom-control {
            height: 26px;
            margin-bottom: 10px;
        }
        .myImg{
            width: 50px;
        }
        .myBox h3{
            font-size: 1.1rem;
            font-weight: 600;
        }
        .myBox h5{
            font-size: 0.83rem;
        }
        #step1 {
            margin-top: 5px;
        }
        .customBox h2 {
            font-size: 1.2rem;
        }
    }
</style>
<main>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="myBox">
                    <div class="stepContainer">
                        <div class="row customBox">
                            <div class="col-12 text-center">
                                <h2>We have matching Moving Companies for you!</h2>
                                <h5>Get a free quotes from up to 4 prescreened pros now.</h5>
                                <hr>
                                <form action="thankYou.php" method="POST">
                                    <input type="hidden" name="service" value="Get A Free Estimate">
                                    <input type="hidden" name="formType" value="1">
                                    <input type="hidden" name="fromZip" value="<?= $_GET['fromZip'] ?>">
                                    <input type="hidden" name="toZip" value="<?= $_GET['toZip'] ?>">
                                    <input type="hidden" name="moveDate" value="<?= $_GET['moveDate'] ?>">
                                    <input type="hidden" name="rooms" value="<?= $_GET['moveSize'] ?>">
                                    <input type="hidden" name="typeOfMove" value="<?= $_GET['typeOfMove'] ?>">
                                    <input type="hidden" name="ref" value="<?= $ref ?>">
                                    <?php if(isset($_SESSION['affiliateLink'])){?>
                                    <input type="hidden" name="aff" value="<?= $_SESSION['affiliateLink'] ?>">
                                    <?php }else if(isset($_COOKIE['affiliateLink'])){ ?>
                                    <input type="hidden" name="aff" value="<?= $_COOKIE['affiliateLink'] ?>">
                                    <?php } ?>
                                    <div id="firstStep">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="firstName">First Name</label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" onkeyup="checkForm()" name="firstName" id="fName" class="my-custom-control">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="lastName">Last Name</label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" onkeyup="checkForm()" name="lastName" id="lName" class="my-custom-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="lastStep" style="display:none;">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="phone">Phone</label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" onkeyup="checkForm()" id="phone" onkeyup="checkPhone(this.value)" name="phone" class="my-custom-control">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="email">Email</label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" onkeyup="checkForm()" id="email" name="email" class="my-custom-control">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="checkbox" checked style="margin-right:5px;vertical-align: text-top;" id='info' name="info" onclick="checkForm()"><label for="info" style="font-size:10px;vertical-align: middle;">Yes. I would like to receive free project cost information.</label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div id="firstStepBtn">
                                        <input type="button" onclick='runStep2();' class="btn btn-custom btn-lg" value="Continue">
                                    </div>
                                    <div id="lastStepBtn" style="display:none;">
                                        <input type="button" onclick='runStep1();' class="btn btn-white btn-lg" value="Back">
                                        <input type="submit" disabled id="estimate" class="btn btn-custom btn-lg" value="Get a free estimate">
                                        <div class="text-left">
                                            <small style="color:rgb(96, 96, 96);">
                                                By clicking Get a free estimate, you affirm you have read and agree to the HandyMatcher Terms, and you agree and authorize HandyMatcher and its affiliates, and their networks of Service Professionals, to deliver marketing calls or texts using automated technology to the number you provided above regarding your project and other home services offers. Consent is not a condition of purchase.
                                            </small>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).on("scroll",function(){
       $("#progressBarContainer").css("top",0);
    });
    function checkForm(){
        if(document.getElementById("info").checked === true){
            if($("#phone").val()){
                if($("#email").val()){
                    $("#estimate").removeAttr("disabled");
                }
            }
        }else{
            $("#estimate").attr("disabled",true);
        }
    }
    function runStep2(){
        $("#firstStep").hide();
        $("#lastStep").show();
        $("#firstStepBtn").hide();
        $("#lastStepBtn").show();
    }
    function runStep1(){
        $("#firstStep").show();
        $("#lastStep").hide();
        $("#firstStepBtn").show();
        $("#lastStepBtn").hide();
    }
</script>