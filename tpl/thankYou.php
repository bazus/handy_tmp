<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (isset($_POST['fullName'])){
    $fullName = $_POST['fullName'];
}else{
    if(isset($_POST['firstName']) && isset($_POST['lastName'])){
        $fullName = $_POST['firstName']." ".$_POST['lastName'];
    }else{
        $fullName = "Empty";
    }
}
if (isset($_POST['ref'])){
    $ref = $_POST['ref'];
}else{
    $ref = "Empty";
}
if (isset($_POST['email'])){
    $email = $_POST['email'];
}else{
    $email = "Empty";
}
if (isset($_POST['phone'])){
    $phone = $_POST['phone'];
}else{
    $phone = "Empty";
}
if (isset($_POST['fromZip'])){
    $fromZip = $_POST['fromZip'];
}else{
    $fromZip = "Empty";
}
if (isset($_POST['toZip'])){
    $toZip = $_POST['toZip'];
}else{
    $toZip = "Empty";
}
if (isset($_POST['toState'])){
    $toState = $_POST['toState'];
}else{
    $toState = "Empty";
}
if (isset($_POST['toCity'])){
    $toCity = $_POST['toCity'];
}else{
    $toCity = "Empty";
}
if (isset($_POST['rooms'])){
    $rooms = $_POST['rooms'];
}else{
    $rooms = "Empty";
}
if (isset($_POST['moveDate'])){
    $moveDate = $_POST['moveDate'];
}else{
    $moveDate = "Empty";
}
if (isset($_POST['typeOfMove'])){
    $typeOfMove = $_POST['typeOfMove'];
}else{
    $typeOfMove = 0;
}

if(isset($_POST['aff'])){
   
   require_once '/var/www/html/classes/affiliate/affiliates.php';
    $affilaites = new affiliates(); 
    
    $aff = $affilaites->getAffIdFromLink($_POST['aff']);
    
    if($aff){
        $affiliate = $aff;
    }else{
        $affiliate = 0;
    }
}else{
    $affiliate = 0;
}

$ipAddress = get_client_ip_env();
$userAgent = $_SERVER['HTTP_USER_AGENT'] ;
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"https://app.leadrift.com/admin/getLead.php");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
    "fullName=$fullName&ref=$ref&email=$email&phone=$phone&fromZip=$fromZip&toZip=$toZip&toState=$toState&toCity=$toCity&moveSize=$rooms&moveDate=$moveDate&ipAddress=$ipAddress&userAgent=$userAgent&receiveFrom=HandyMatcher&typeOfMove=$typeOfMove&aff=$affiliate");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);
curl_close ($ch);