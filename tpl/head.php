<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?= $title ?></title>
<link rel="shortcut icon" type="image/x-icon" href="https://handymatcher.com/images/favicon.png">
<meta name="description" content="<?= $description ?>" />
<link rel="canonical" href="index.php" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?= $title ?>" />
<meta property="og:description" content="<?= $description ?>" />
<meta property="og:url" content="index.php" />
<meta property="og:site_name" content="Handy Matcher" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="<?= $title ?>" />
<meta name="twitter:description" content="<?= $description ?>" />
<meta name="twitter:image" content="https://handymatcher.com/images/favicon.png" />
<meta name="msapplication-TileImage" content="https://handymatcher.com/images/favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">

<link rel="stylesheet" href="https://handymatcher.com/css/bootstrap.min.css">
<script src="https://handymatcher.com/js/jquery.min.js"></script>
<script src="https://handymatcher.com/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://handymatcher.com/plugins/datepicker/themes/default.css">
<link rel="stylesheet" type="text/css" href="https://handymatcher.com/plugins/datepicker/themes/default.date.css">
<script src="https://handymatcher.com/plugins/datepicker/picker.js"></script>
<script src="https://handymatcher.com/plugins/datepicker/picker.date.js"></script>

<!-- Facebook Pixel Code -->
<script>
try{
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '2225549777707774');
fbq('track', 'PageView');
}catch(e){}
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=2225549777707774&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->