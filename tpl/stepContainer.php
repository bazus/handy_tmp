<style>
    .myImg{
        width: 85px;
        margin-bottom: 10px;
    }
    @media (max-width:768px) {
        main{
            padding-top: 15px;
        }
        hr{
            margin-top: 0.5rem;
            margin-bottom: 0.5rem;
        }
        .my-custom-control {
            height: 26px;
            margin-bottom: 10px;
        }
        .myImg{
            width: 50px;
        }
        .myBox h3{
            font-size: 1.1rem;
            font-weight: 600;
        }
        .myBox h5{
            font-size: 0.83rem;
        }
        #step1 {
            margin-top: 5px;
        }
        .customBox h2 {
            font-size: 1.2rem;
        }
    }
</style>
<div class="stepContainer">
    <div class="row customBox" id="step1">
        <div class="col-12 text-center">
            <h2>Where are you moving from?</h2>
            <hr>
            <img src="https://handymatcher.com/images/location.png" class="rounded mx-auto d-block myImg">
            <h5>Zip Code</h5>
            <div id="zipInfo"></div>
            <!--<input id="fromZip" onkeyup="checkStep1(this.value)" type="text" class="my-custom-control" placeholder="Enter Zip Code">-->
            <input id="fromZip" type="text" class="my-custom-control" placeholder="Enter Zip Code">
            <hr>
            <!-- <button disabled id="firstNext" class="btn btn-custom btn-lg" onclick="moveToStep2()">Next</button> -->
            <button id="firstNext" class="btn btn-custom btn-lg" onclick="moveToStep2()">Next</button>
        </div>
    </div>
    <div class="row customBox" id="step2" style="display:none;">
        <div class="col-12 text-center">
            <h2>Where are you moving to?</h2>
            <hr>
            <img src="https://handymatcher.com/images/location.png" class="rounded mx-auto d-block myImg">
            <h5>Zip Code</h5>
            <div id="zipInfo2"></div>
            <!-- <input id="toZip" onkeyup="checkStep2(this.value)" type="text" class="my-custom-control" placeholder="Enter Zip Code"> -->
            <input id="toZip" type="text" class="my-custom-control" placeholder="Enter Zip Code">
            <hr>
            <button class="btn btn-default btn-lg" onclick="moveToStep1()">Previous</button>
            <!--<button disabled id="secondNext" class="btn btn-custom btn-lg" onclick="moveToStep3()">Next</button>-->
            <button id="secondNext" class="btn btn-custom btn-lg" onclick="moveToStep3()">Next</button>
        </div>
    </div>
    <div class="row customBox" id="step3" style="display:none;">
        <div class="col-12 text-center">
            <h2>When will you be moving?</h2>
            <hr>
            <h5>Moving Date</h5>
            <input type="text" id="movingDate" class="my-custom-control" value="<?= date("m/d/Y",strtotime("Today")) ?>">
            <hr>
            <button class="btn btn-default btn-lg" onclick="moveToStep3()">Previous</button>
            <button id="thirdNext" class="btn btn-custom btn-lg" onclick="moveToStep4()">Next</button>
        </div>
    </div>
    <div class="row customBox" id="step4" style="display:none;">
        <div class="col-12 text-center">
            <h2>Select your type of move</h2>
            <hr>
            <h5>Type Of Move</h5>
            <select name="typeOfMove" id="typeOfMove" onchange="checkStep4(this.value)" class="form-control">
                <option value="1">Full Service</option>
                <option value="2">Self Service</option>
                <option value="3">Auto Transport</option>
                <option value="4">Office Moves</option>
                <option value="5">Piano Moving</option>
                <option value="6">Packing Service Only</option>
                
            </select>
            <hr>
            <button class="btn btn-default btn-lg" onclick="moveToStep3()">Previous</button>
            <button id="fourthNext" class="btn btn-custom btn-lg" onclick="moveToStep5()">Next</button>
        </div>
    </div>
     <div class="row customBox" id="step5" style="display:none;">
        <div class="col-12 text-center">
            <h2>What are you planning to move?</h2>
            <hr>
            <h5>Moving Size</h5>
            <select name="rooms" id="movingSize" onchange="checkStep5(this.value)" class="form-control">
                <option value="10">Studio 1500 lbs</option>
                <option value="1">1 BR Small 3000 lbs</option>
                <option value="2">1 BR Large 4000 lbs</option>
                <option value="3"="">2 BR Small 4500 lbs</option>
                <option value="4">2 BR Large 6500 lbs</option>
                <option value="5" selected>3 BR Small 8000 lbs</option>
                <option value="6">3 BR Large 9000 lbs</option>
                <option value="7">4 BR Small 10000 lbs</option>
                <option value="8">4 BR Large 12000 lbs</option>
                <option value="9">Over 12000 lbs</option>
                <option disabled>---------------</option>
                <option value="11">Commercial</option>
                <option value="12">Office</option>
                <option value="13">Partial</option>
            </select>
            <hr>
            <button class="btn btn-default btn-lg" onclick="moveToStep4()">Previous</button>
            <button id="fifthNext" class="btn btn-custom btn-lg" onclick="runStep7()">Next</button>
        </div>
    </div>
    <div class="row customBox" id="step6" style="display:none;">
        <div class="col-12 text-center">
            <h2>Loading</h2>
            <h5>We are looking for moving companies based on the information you fill above</h5>
            <hr>
            <div class="loader"></div>
        </div>
        <div class="col-12 text-center">
            if this step takes more then 10 seconds please <a href="#" onclick="runStep6()">Click Here</a>
        </div>
    </div>
    <div class="row customBox" id="step7" style="display:none;">
        <div class="col-12 text-center">
            <h2>We have matching Moving Companies for you!</h2>
            <h5>Get a free quotes from up to 4 prescreened pros now.</h5>
            <hr>
            <form action="https://handymatcher.com/thankYou.php" method="POST">
                <input type="hidden" name="service" value="Get A Free Estimate">
                <input type="hidden" name="formType" value="1">
                <input type="hidden" name="fromZip" id="formFromZip">
                <input type="hidden" name="toZip" id="formToZip">
                <input type="hidden" name="moveDate" id="formMoveDate">
                <input type="hidden" name="rooms" id="formRooms">
                <input type="hidden" name="typeOfMove" id="formTypeOfMove">
                <input type="hidden" name="ref" value="<?= $ref ?>">
                <?php if(isset($_SESSION['affiliateLink'])){?>
                <input type="hidden" name="aff" value="<?= $_SESSION['affiliateLink'] ?>">
                <?php }else if(isset($_COOKIE['affiliateLink'])){ ?>
                <input type="hidden" name="aff" value="<?= $_COOKIE['affiliateLink'] ?>">
                <?php } ?>
                <div id="firstStep">
                    <div class="row">
                        <div class="col-4">
                            <label for="firstName">First Name</label>
                        </div>
                        <div class="col-8">
                            <input type="text" onkeyup="checkForm()" name="firstName" id="fName" class="my-custom-control">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-4">
                            <label for="lastName">Last Name</label>
                        </div>
                        <div class="col-8">
                            <input type="text" onkeyup="checkForm()" name="lastName" id="lName" class="my-custom-control">
                        </div>
                    </div>
                </div>
                <div id="lastStep" style="display:none;">
                    <div class="row">
                        <div class="col-4">
                            <label for="phone">Phone</label>
                        </div>
                        <div class="col-8">
                            <input type="text" onkeyup="checkForm()" id="phone" onkeyup="checkPhone(this.value)" name="phone" class="my-custom-control">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-4">
                            <label for="email">Email</label>
                        </div>
                        <div class="col-8">
                            <input type="text" onkeyup="checkForm()" id="email" name="email" class="my-custom-control">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <input type="checkbox" checked style="margin-right:5px;vertical-align: text-top;" id='info' name="info" onclick="checkForm()"><label for="info" style="font-size:10px;vertical-align: middle;">Yes. I would like to receive free project cost information.</label>
                        </div>
                    </div>
                </div>
                <hr>
                <div id="firstStepBtn">
                    <input type="button" onclick='runStep2();' class="btn btn-custom btn-lg" value="Continue">
                </div>
                <div id="lastStepBtn" style="display:none;">
                    <input type="button" onclick='runStep1();' class="btn btn-white btn-lg" value="Back">
                    <input type="submit" disabled id="estimate" class="btn btn-custom btn-lg" value="Get a free estimate">
                    <div class="text-left">
                        <small style="color:rgb(96, 96, 96);">
                            By clicking Get a free estimate, you affirm you have read and agree to the HandyMatcher Terms, and you agree and authorize HandyMatcher and its affiliates, and their networks of Service Professionals, to deliver marketing calls or texts using automated technology to the number you provided above regarding your project and other home services offers. Consent is not a condition of purchase.
                        </small>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>