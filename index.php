<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    $isMobile = true;
}else{
    $isMobile = false;
}
if (isset($_GET['r'])){
    $ref = $_GET['r']." | Handy Matcher";
}else{
    $ref = "Organic"." | Handy Matcher";
}
$fullPath = "/var/www/html/";
if($_SERVER['HTTP_HOST'] == "localhost"){$fullPath = "";}
if(isset($_GET['affiliate'])){
    function get_client_ip_env() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
    if(!isset($_SESSION)){
        session_start();
    }
    if(!isset($_COOKIE['affiliateLink'])){
        $_SESSION['affiliateLink'] = $_GET['affiliate'];
        $cookie_name = "affiliateLink";
        $cookie_value = $_GET['affiliate'];
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

        require_once '/var/www/html/classes/affiliate/affiliates.php';
        $affilaites = new affiliates();
        $ip = get_client_ip_env();
        $affilaites->createClick($_GET['affiliate'], $ip);

    }
}
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <?php
        $title = "Handy Matcher | Moving and Home Improvement";
        $description = "Handy Matcher | Moving and Home Improvement. It is the simplest way to find and book top-rated local home services. Connect with trusted home repair and improvement contractors.";
        require_once '/var/www/html/tpl/head.php';
    ?>
    <link rel="stylesheet" href="https://handymatcher.com/css/style.css">
</head>

<body>
    <?php require_once($fullPath."tpl/header.php")?>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-12 heroContainer">
                    <div class="hero">
                        <h1>Find trusted local pros for any home project.</h1>
                        <br><br>
                        <div class="custom-container">
                            <div class="row">
                                <div class="col-9 mobileBig">
                                    <select class="form-control" id="service" <?php if($isMobile){?> onchange="openService(this.value)"<?php } ?>>
                                        <option value="" selected="" disabled="">What service do you need?</option>
                                        <option value="1">Local Moving</option>
                                        <option value="2">Long Distance Moving</option>
                                        <option value="3">Office Moving</option>
                                        <option value="5">Auto Transport</option>
                                        <option value="6">Plumbing</option>
                                        <option value="7">Flooring</option>
                                        <option value="8">Roofing</option>
                                        <option value="9">Cleaning</option>
                                    </select>
                                </div>
                                <div class="col-3 mobileBig">
                                    <button class="btn btn-block btn-custom" onclick="openService($('#service').val())">Find Pros</button>
                                </div>
                            </div>
                            <div class="row mobileShow">
                                <div class="col-12" style="color:#fff;background: rgba(0,0,0,0.5);margin-top: 15px;">
                                    <a style="color:#fff;" href="https://handymatcher.com/pro.php">Are you a quality pro? Join our network.</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div id="arrow"></div>
    <div id="servicesTop" class="container">
      <h5 style="padding-left:20px; padding-top:15px;">Popular projects</h5>
      <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
      >
        <div class="d-flex">
          <div class="col-2 service-icon">
            <img
              srcset="https://handymatcher.com/images/icons/handyman-tools.svg"
              class="service-icon"
            />
          </div>
          <div class="col-10 service-text">
            <h5>Handymen</h5>
          </div>
        </div>
      </div>
      <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
      >
        <div class="d-flex">
          <div class="col-2 service-icon">
            <img
              srcset="https://handymatcher.com/images/icons/plug.svg"
              class="service-icon"
            />
          </div>
          <div class="col-10 service-text">
            <h5>Electrical</h5>
          </div>
        </div>
      </div>
      <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
      >
        <div class="d-flex">
          <div class="col-2 service-icon">
            <img
              srcset="https://handymatcher.com/images/icons/ac.svg"
              class="service-icon"
            />
          </div>
          <div class="col-10 service-text">
            <h5>
              HVAC
            </h5>
          </div>
        </div>
      </div>
      <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
      >
        <div class="d-flex">
          <div class="col-2 service-icon">
            <img
              srcset="https://handymatcher.com/images/icons/pipeline.svg"
              class="service-icon"
            />
          </div>
          <div class="col-10 service-text">
            <h5>Plumbing</h5>
          </div>
        </div>
      </div>
      <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
      >
        <div class="d-flex">
          <div class="col-2 service-icon">
            <img
              srcset="https://handymatcher.com/images/icons/roof.svg"
              class="service-icon"
            />
          </div>
          <div class="col-10 service-text">
            <h5>Roofing</h5>
          </div>
        </div>
      </div>
      <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
      >
        <div class="d-flex">
          <div class="col-2 service-icon">
            <img
              srcset="https://handymatcher.com/images/icons/carpet.svg"
              class="service-icon"
            />
          </div>
          <div class="col-10 service-text">
            <h5>Flooring</h5>
          </div>
        </div>
      </div>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-12">
            <div class="service-bottom-buttom"><a href="https://handymatcher.com/directory.php">View All Categories</a></div>
          </div>
        </div>
      </div>
    </div>
    <br><br>
    <br><br><br>

    <!-- BOXES -->

    <section id="home-icons">
        <div class="container-fluid myRows">
            <div class="grey">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 mb-4 text-center" id="aka-1">
                            <br><br>
                            <h5 class="h-5">Tell us about your project</h5>
                            <p class="p-1">
                              Select a project category that best matches your home repair or improvement need
                              We'll ask you a few important questions to ensure we're able to match you to the right pro for your job
                              HandyMatcher has over 70k service professionals specializing in over 100 categories
                            </p>
                        </div>
                        <div class="col-md-6">
                            <img class="center-image img-fluid" src="images/1.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img class="img-fluid center-image lazy" src="images/2.jpg">
                    </div>
                    <div class="col-md-6 mb-4  text-center" id="aka-1">
                        <br><br>
                        <h5 class="h-5">Get Matched by Our Team</h5>
                        <p class="p-1">
                          You'll receive information for up to four pre-screened, local home improvement pros
                          Your matched pros provide the specific service you need and are available right now

                        </p>
                    </div>
                </div>
            </div>
            <br><br><br>
            <div class="grey">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 mb-4 text-center" id="aka-1">
                            <br><br>
                            <h5 class="h-5">Get Connected to the Best Pros</h5>
                            <p class="p-1">
                              As soon as your request is processed, we send your information to your matched pros
                              Shortly after receiving your service request, they'll contact you to discuss your project
                              If you prefer, you can contact them at your convenience
                            </p>
                        </div>
                        <div class="col-md-6">
                            <img class="img-fluid center-image lazy" src="images/3.jpg">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container tabletTop">
        <section>
            <div class="conatiner">
                <div class="snd-div">
                    <h5>
                      Tell us what you need. Get prices on the spot. Book risk free. Save a lot.
                    </h5>
                </div>
            </div>
            <br><br>
        </section>

        <br>
        <br>
        <section>
            <div class="container">
                <div class="row text-center">
                    <div class="col-4" id="">
                        <img src="images/i1.png" alt="" class="img-2">
                        <br>
                        <h6>Real Prices. Instantly.</h6>
                        <p>Get custom quotes on the spot</p>
                    </div>
                    <div class="col-4">
                        <img src="images/i2.png" alt="" class="img-2">
                        <br>
                        <h6>Risk Free Booking</h6>
                        <p>Order your job with zero down</p>
                    </div>
                    <div class="col-4">
                        <img src="images/i3.png" alt="" class="img-2">
                        <br>
                        <h6>Big Savings</h6>
                        <p>Save up to 40% on any project</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <br><br><br><br><br>

    <?php require_once($fullPath."tpl/footer.php")?>
    <script src="https://use.fontawesome.com/7e5b532868.js"></script>
    <script>
        function moveToForm(){
            window.location.href = "https://handymatcher.com/moving";
        }
        function openService(id){
            switch(id){
                case "":
                    break;
                case '1':
                    window.location.href = "https://handymatcher.com/localMoving.php/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                    break;
                case '2':
                    window.location.href = "https://handymatcher.com/longDistanceMoving.php/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                    break;
                case '3':
                    window.location.href = "https://handymatcher.com/moving.php/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                    break;
                case '4':
                    window.location.href = "https://handymatcher.com/moving.php/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                    break;
                default:
                    window.location.href = "https://handymatcher.com/moving.php/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                    break;
            }
        }
    </script>
</body>
</html>
