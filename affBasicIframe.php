<?php
header("Access-Control-Allow-Origin: *");
$affLink = $_GET['affLink'];
function get_client_ip_env() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}
require_once '/var/www/html/classes/affiliate/affiliates.php';
$affilaites = new affiliates();
$ip = get_client_ip_env();
$affilaites->createClick($affLink, $ip);
?>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
<style>
    *{
        font-family: Montserrat
    }
    div .row{
        margin-bottom: 7.5px;
    }
    label{
        font-weight: 600;
        margin-top: 5px;
    }
</style>
<div id="container" class="container-fluid" style="padding: 25px;">
    <div class="row">
        <div class="col-5">
            <label>Full Name</label>
        </div>
        <div class="col-7">
            <input class="form-control" type="text" placeholder="First & Last Name" id="fullName">
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            <label>Phone</label>
        </div>
        <div class="col-7">
            <input class="form-control" type="text" placeholder="Phone Number" id="phone">
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            <label>Email</label>
        </div>
        <div class="col-7">
            <input class="form-control" type="text" placeholder="Email Address" id="email">
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            <label>Moving From</label>
        </div>
        <div class="col-7">
            <input class="form-control" type="text" placeholder="Pick Up ZIP Code" id="fromZip">
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            <label>Moving To</label>
        </div>
        <div class="col-7">
            <input class="form-control" type="text" placeholder="Delivery ZIP Code" id="toZip">
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            <label>Moving Date</label>
        </div>
        <div class="col-7">
            <input class="form-control" type="text" placeholder="MM/DD/YYYY" id="moveDate">
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            <label>Moving Size</label>
        </div>
        <div class="col-7">
            <select id="movingSize" class="form-control">
                <option value="10">Studio 1500 lbs</option>
                <option value="1">1 BR Small 3000 lbs</option>
                <option value="2">1 BR Large 4000 lbs</option>
                <option value="3" =""="">2 BR Small 4500 lbs</option>
                <option value="4">2 BR Large 6500 lbs</option>
                <option value="5" selected="">3 BR Small 8000 lbs</option>
                <option value="6">3 BR Large 9000 lbs</option>
                <option value="7">4 BR Small 10000 lbs</option>
                <option value="8">4 BR Large 12000 lbs</option>
                <option value="9">Over 12000 lbs</option>
                <option disabled="">---------------</option>
                <option value="11">Commercial</option>
                <option value="12">Office</option>
                <option value="13">Partial</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <button onclick="sendData($(this))" class="btn btn-primary btn-block">Get a free estimate</button>
        </div>
    </div>
</div>



<script>
    function sendData(obj){
        obj.text("Sending Info...");
        obj.attr("disabled",true);
        $.ajax({
            method:"POST",
            url: "https://handymatcher.com/actions/affiliate/affThankYou.php",
            data:{ 
                fullName: $("#fullName").val(),
                email: $("#email").val(),
                phone: $("#phone").val(),
                fromZip: $("#fromZip").val(),
                toZip: $("#toZip").val(),
                moveDate: $("#moveDate").val(),
                rooms: $("#movingSize").val(),
                affLink: "<?= $affLink ?>",
                ref:1
            }
        }).done(function (data) {
            $("#container").html("<div class='row text-center'><div class='col-12'><h3>Thank You</h3><h4>The best moving companies in your area will be in contact with you soon!</h4></div></div>");
        });
    }
</script>
