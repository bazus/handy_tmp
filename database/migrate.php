<?php

require_once '/var/www/html/vendor/autoload.php';
require_once '/var/www/html/config/database.php';

use Illuminate\Database\Capsule\Manager as DB;

foreach (glob("migrations/*.php") as $filename)
{
    include $filename;
}
