<?php
use Illuminate\Database\Capsule\Manager as DB;

DB::schema()->create('leads', function ($table) {
    $table->id();
    $table->string('service');
    $table->string('formType');
    $table->string('fromZip');
    $table->string('toZip');
    $table->date('moveDate');
    $table->mediumInteger('rooms');
    $table->string('typeOfMove');
    $table->string('ref');
    $table->string('firstName');
    $table->string('lastName');
    $table->string('phone');
    $table->string('email');
    $table->string('info');


    $table->timestamps();
});
