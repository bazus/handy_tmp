<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '../vendor/autoload.php';
require_once '../config/database.php';

use Illuminate\Database\Capsule\Manager as DB;


$url = explode('/', $_SERVER['REQUEST_URI'])[2];


$client_obj = DB::table('moving')->where( 'url', '=', $url )->first();

if( !$client_obj){
    header('Location: /directory');
    die();
}

$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    $isMobile = true;
}else{
    $isMobile = false;
}
if (isset($_GET['r'])){
    $ref = $_GET['r']." | Handy Matcher";
}else{
    $ref = "Organic"." | Handy Matcher";
}
$fullPath = "/var/www/html/";
if($_SERVER['HTTP_HOST'] == "localhost"){$fullPath = "";}
require_once '/var/www/html/movingDir/movingDir.php';
$q = false;
if(isset($_GET['q'])){
    $q = htmlspecialchars(htmlentities($_GET['q']));
    $q = str_replace("-", " ", $q);
}
$error = "";
$client = (array) $client_obj;

$clients[0] = $client;
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <?php
    $title = $client['companyname'] ." | Handy Matcher";
    $description =  "See past project info for ".$client['companyname']." | Handy Matcher";
    require_once '/var/www/html/tpl/head.php';
    ?>
    <style>
        .btn-white{
            background: #f4f4f4;
            border: 1px solid #ced4da;
        }
    </style>

    <link rel="stylesheet" href="https://handymatcher.com/css/directory.css">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="stacks">


    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/plugins/font-awesome/css/all.min.css" rel="stylesheet">

    <!-- Theme Styles -->
    <link href="/assets/css/lime.min.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">


</head>

<body>

<?php require_once($fullPath."tpl/header.php")?>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="profile-cover" style="background: url(https://handymatcher.com/images/HomeBG.jpg) center center no-repeat"></div>
                <div class="profile-header">
                    <div class="profile-img">
                        <img src="https://handymatcher.com/images/defaultLogo.jpg">
                    </div>
                    <div class="profile-name">
                        <h1><?= $client['companyname']?></h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Contact information:</h5>
                        <ul class="list-unstyled profile-about-list">
                          <li><i class="material-icons">local_phone</i><span>Phone Number: <?= $client['phone']?></span></li>
                          <li><i class="material-icons">map</i><span>State: <?= $client['state']?></span></li>
                          <li><i class="material-icons">local_shipping</i><span>USDOT: <?= $client['usdot']?></span></li>
                          <li><i class="material-icons">perm_identity</i><span>Contact Person: <?= $client['firstname']?> <?= $client['lastname']?></span></li>
                          <li><i class="material-icons">devices</i><span>Website: <?= $client['website']?></span></li>
                          <li><i class="material-icons">mail_outline</i><span>Email: <?= $client['email']?></span></li>                          
                        </ul>
                        <br><br>
                        <a class="btn btn-block btn-primary m-t-lg" href="https://handymatcher.com/moving.php">Request a Quote Now</a>
                        <br>
                    </div>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">About <?= $client['companyname']?></h2>
                        <p><?= $client['companyname']?> specializes in local and long distance moving as well as residential and commercial moves.
                          In addition to our moving services, we provide packing services, secured storage and a variety of moving supplies.
                          Are you going to move from <?= $client['state']?>? You can count on <?= $client['companyname']?>
                          as we will help you to save up to 40% on your upcoming move and make it absolutely stress-free! </p>
                        <p><?= $client['description']?></p>
                        <br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php require_once($fullPath."tpl/footer.php")?>
<script src="https://use.fontawesome.com/7e5b532868.js"></script>
<script>
    var page = 1;

    var companies = [];
    $(document).ready(function(){
        $("#companies .col-md-4").each(function(){
            companies.push($(this));
        });
    });
    function search(){
        var v = $("#search").val();
        v = v.replace(/\s+/g,'-');
        if(v.match(/^[0-9A-Za-z\s\-]+$/)){
            v = v.replace(/\s+/g,'-');
            window.location.href = "https://handymatcher.com/directory/search/"+v;
        }else{
            $("#errorDiv").html("<small class='text-danger'>Query must contain only letters, numbers and spaces. Please try again</small>");
        }
    }
    $(document).keyup(function (e) {
        if ($("#search").is(":focus")){
            $("#errorDiv").html("");
        }
        if ($("#search").is(":focus") && (e.keyCode == 13)) {
            search();
        }
    });
    function openService(id){
        switch(id){
            case "":
                break;
            case '1':
                window.location.href = "https://handymatcher.com/localMoving.php?ref=<?= $ref ?>";
                break;
            case '2':
                window.location.href = "https://handymatcher.com/longDistanceMoving.php?ref=<?= $ref ?>";
                break;
            case '3':
                window.location.href = "https://handymatcher.com/moving.php?ref=<?= $ref ?>";
                break;
            case '4':
                window.location.href = "https://handymatcher.com/moving.php?ref=<?= $ref ?>";
                break;
            default:
                alert("in development check back soon!");
                break;
        }
    }
</script>
</body>
</html>
