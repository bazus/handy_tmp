<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
/*if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: dashboard.php");
    exit;
}*/
$siteUrl = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/";

// Include config file
require_once "config/db.php";

// Define variables and initialize with empty values
// Define variables and initialize with empty values
$email = $password = "";
$email_err = $password_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST") {

// Check if username is empty
    if (empty(trim($_POST["email"]))) {
        $email_err = "Please enter email.";
    } else {
        $email = trim($_POST["email"]);
    }

// Check if password is empty
    if (empty(trim($_POST["password"]))) {
        $password_err = "Please enter your password.";
    } else {
        $password = trim($_POST["password"]);
    }

// Validate credentials
    if (empty($email_err) && empty($password_err)) {
        // Prepare a select statement
        $sql = "SELECT id, email, password FROM users WHERE email = ?";

        if ($stmt = mysqli_prepare($link, $sql)) {
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_email);

            // Set parameters
            $param_email = $email;

            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)) {
                // Store result
                mysqli_stmt_store_result($stmt);

                // Check if username exists, if yes then verify password
                if (mysqli_stmt_num_rows($stmt) == 1) {
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $email, $hashed_password);
                    if (mysqli_stmt_fetch($stmt)) {
                        if (password_verify($password, $hashed_password)) {
                            // Password is correct, so start a new session
                            session_start();

                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["email"] = $email;

                            // Redirect user to welcome page
                            header("location: pro/crm/index.php");
                        } else {
                            // Display an error message if password is not valid
                            $password_err = "The password you entered was not valid.";
                        }
                    }
                } else {
                    // Display an error message if username doesn't exist
                    $email_err = "No account found with that email.";
                }
            } else {
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }

// Close connection
   // mysqli_close($db);
}
?>
<html lang="en">
<head>
    <title>Login</title>
    <!-- ============== Jquery & UI ============== -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- ============== Bootstrap ============== -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>



    <!-- ============== Custom Files ============== -->
<link rel="stylesheet" href="<?= $siteUrl; ?>css/style.css">
<script>
    var BASE_URL = '<?= $siteUrl ?>';
</script>

<meta name="google-signin-client_id" content="357226719040-hd75f4ltg8cakhmgcfmaqo7qsi9cv9dn.apps.googleusercontent.com">

</head>
<body>
<?php require_once($fullPath."tpl/header.php")?>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <!--<div class="app-header header-shadow bg-dark">
        <div class="app-header__logo">
            <img src="<?= $siteUrl ?>/images/logo.jpg" alt="HandyMatcher" height="58">
        </div>
        <div class="app-header__content">
            <div class="app-header-right">
                <div class="header-btn-lg pr-0">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-right header-user-info ml-3">
                                <button onclick="location.href='https://handymatcher.com/';" type="button" class="btn-shadow p-1 btn btn-primary btn-sm">
                                    Main
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
<div class="app-main">
    <div class="app-main__outer" style="padding: 0">
        <div class="app-main__inner">
            <div id="loginModal" class="modal-dialog w-100 mx-auto" >
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="h5 modal-title text-center">
                            <h4 class="mt-2">
                                <div>Sign In to Handy Matcher</div>
                            </h4>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12">
                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                                <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                                    <label>Email</label>
                                    <input type="text" name="email" class="form-control" value="<?php echo $email; ?>">
                                    <span class="help-block"><?php echo $email_err; ?></span>
                                </div>
                                <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control">
                                    <span class="help-block"><?php echo $password_err; ?></span>
                                    <div class="mt-5 modal-title text-center">
                                        <button class="btn btn-primary btn-lg form-control" type="submit" value="Login">Sign In</button>
                                    </div>
                                </div>
                                <div class="modal-footer clearfix"></div>
                                <div class="modal-title text-center"><p>Don't have an account yet? <a style="color:black" class="font-weight-bold" href="signup_old.php">Sign Up</a></p></div>
                                <div class="text-center" style="font-size: 0.7rem;">By signing up, signing in or continuing. I agree to Houzz's <a href="#">Terms of Use </a> and <a href="#">Privacy Policy</a></div>
                            </form>
                            </div>
                    </div>

                </div>
            </div>

                </div>

        </div>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    </div>
</div>

<!--<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>-->
</body>
</html>