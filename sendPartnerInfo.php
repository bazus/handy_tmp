<?php
if(isset($_POST['clientData'])){
    $title = "New Partners Request - Handy Matcher";
    $str =  "<div>";
    $str .= "<p>Client Name: ".$clientData['name']."</p>";
    $str .= "<p>Company Name: ".$clientData['companyName']."</p>";
    $str .= "<p>Phone: ".$clientData['phone']."</p>";
    $str .= "<p>Email: ".$clientData['email']."</p>";
    $str .= "<p>Service: ".$clientData['service']."</p>";
    $str .= "<p>Working States: ".$clientData['states']."</p><br>";
    function get_client_ip_env() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    $str .= "<p>IP: ".  get_client_ip_env()."</p>";
    $str .= "<p>User Agent: ".$useragent."</p>";
    $str .= "</div>";
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,"https://app.leadrift.com/admin/sendEmail.php");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
        "title=$title&content=$str");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);
    curl_close ($ch);
    echo json_encode($server_output);
}else{
    echo json_encode($_REQUEST);
    echo json_encode(false);
}
