﻿<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    $isMobile = true;
}else{
    $isMobile = false;
}
if (isset($_GET['r'])){
    $ref = $_GET['r']." | Handy Matcher";
}else{
    $ref = "Organic"." | Handy Matcher";
}
$fullPath = "/var/www/html/";
if($_SERVER['HTTP_HOST'] == "localhost"){$fullPath = "";}

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <?php 
        $title = "Handy Matcher | Privacy & Policy";
        $description = "Handy Matcher | Moving and Home Improvement. It is the simplest way to find and book top-rated local home services. Connect with trusted home repair and improvement contractors.";
        require_once '/var/www/html/tpl/head.php';
    ?>
</head>

<body>
    <?php require_once($fullPath."tpl/header.php")?>
    <div class="container tabletTop">
    <section>
        <div class="conatiner">
            <div class="row">
                <div class="col-12">
                    <h4 style="display:block;">HandyMatcher.com Privacy & Policy</h4><hr>
                <p style="display:block;">HandyMatcher.com currently surveys its visitors for certain demographic information.</p>
                <p>This information typically includes contact information (email address, zip code, phone number).</p>
                <p>The contact information provided by users to Networkmoving is used solely for the purposes of:</p>
                <ul>
                    <li>contacting our users if necessary.</li>
                    <li>allowing reputable and professional companies to contact users in order to offer them a service that the users had voluntarily requested.</li>
                    <li>obtaining a better understanding of our users, thus enabling HandyMatcher.com to create a website that is more accurately designed for its users.</li>
                </ul>
                <p>HandyMatcher.com supplements users’ surveys with information that is received from third parties. </p>
                <p>HandyMatcher.com does not in any way sell or redistribute any of our consumer’s information to any third party organization under any circumstances.HandyMatcher.com are the world’s largest moving and relocation website with movers, real estate professionals, lenders.</p>
                <hr>
                <h2>Collection of Personally Identifiable Information</h2>
                <p>HandyMatcher.com collects Users’ personally-identifiable information (“PII”) that is volunteered by Users. Examples of PII that may be requested and/or collected include but are not limited to: first and last name, address, zip code, email address, telephone number, facsimile number, and company or business identity. From time to time, HandyMatcher.com may also present opportunities for Users to voluntarily provide additional information about themselves.</p>
                <h2>Use and Sharing of PII</h2>
                <p>HandyMatcher.com uses PII to provide the User with information about HandyMatcher.com Services, and to provide HandyMatcher.com third-party vendors with information about User’s moving needs. YOUR USE OF THE WEBSITE SERVES AS YOUR ACKNOWLEDGEMENT AND APPROVAL OF THIS PRACTICE. IF YOU WANT TO ENSURE THAT YOUR PII IS NOT AVAILABLE TO THIRD PARTIES, YOU MUST DISCONTINUE YOUR USE OF THE WEBSITE.</p>
                <p>Users’ PII may also be used to: deliver and improve our products and services; manage our business; manage your account and provide you with customer support; perform research and analysis about your use of, or interest in, our or others’ products, services, or content; communicate with you by email, postal mail, telephone and/or mobile devices about products or services that may be of interest to you either from us or other third parties; develop, display, and track content and advertising tailored to your interests on our Service and other Websites or websites, including providing our advertisements to you when you visit other websites; analyze data about our Website (i.e., analytics); verify your eligibility to utilize our Service; enforce or exercise any rights in our Terms of Service; and perform functions or services as otherwise described to you at the time of collection.</p>
                <p>PII collected by us may be added to our databases and used for future marketing purposes, including but not limited to email and direct marketing. We may also share your PII with third-party vendors that perform certain services on our behalf. These services may include fulfilling orders, providing customer service and marketing assistance, performing business and sales analysis, ad tracking and analytics, member screenings, supporting our Website functionality, and supporting other features offered as part of our services. These vendors may have access to personal information needed to perform their functions but are not permitted to share or use such information for any other purposes.</p>
                <p>In addition, we may also disclose Users’ PII in order to: (1) comply with applicable laws (including, without limitation, the CAN-SPAM Act); (2) respond to governmental inquiries; (3) comply with valid legal process; (4) protect the rights or property of HandyMatcher.com, including without limitation, filing copyright applications with the Library of Congress, Copyright Office, or (5) protect the health and personal safety of any individual.</p>
                <p>By submitting your PII through the Website, you agree that your PII may be used in any manner contemplated in this section.</p>
                <h2>Access to Your Information</h2>
                <p>If you want to review, correct or change your User information, please submit your request in writing to info@handymatcher.com.</p>
                <h2>Changes to This Policy</h2>
                <p>HandyMatcher.com may, from time to time, amend this Policy, in whole or part, at its sole discretion. Any changes to this Policy will be effective immediately upon the posting of the revised policy to the Website.</p>
                <p>The information presented on this page covers networkmoving usage of information gathered for our website, networkmoving contains links to other sites. networkmoving is not responsible for the privacy practices, business operations, or content of any such sites.</p>
                </div>
            </div>
        </div>
    </section>
    <?php require_once($fullPath."tpl/footer.php")?>
    <script src="https://use.fontawesome.com/7e5b532868.js"></script>
    <script>
        function openService(id){
            switch(id){
                case "": 
                    break;
                case '1':
                    window.location.href = "https://handymatcher.com/localMoving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                    break;
                case '2':
                    window.location.href = "https://handymatcher.com/longDistanceMoving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                    break;
                case '3':
                    window.location.href = "https://handymatcher.com/moving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                    break;
                case '4':
                    window.location.href = "https://handymatcher.com/moving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                    break;
                default:
                    alert("in development check back soon!");
                    break;
            }
        }
    </script>
</body>
</html>
