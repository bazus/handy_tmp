﻿<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    $isMobile = true;
}else{
    $isMobile = false;
}
if (isset($_GET['r'])){
    $ref = $_GET['r']." | Handy Matcher";
}else{
    $ref = "Organic"." | Handy Matcher";
}
$fullPath = "/var/www/html/";
if($_SERVER['HTTP_HOST'] == "localhost"){$fullPath = "";}
require_once '/var/www/html/movingDir/movingDir.php';
$q = false;
if(isset($_GET['q'])){
    $q = htmlspecialchars(htmlentities($_GET['q']));
    $q = str_replace("-", " ", $q);
}
$error = "";
if(!$q || strlen($q) < 3){
    $error = "<small class='text-danger'>Query Error</small>";
}
$movingDir = new movingDir();
if(!$error){
    $clients = $movingDir->searchData($q);
    if(!$clients['status']){
        $error = "<small class='text-danger'>No Results</small>";
    }else{
        $clients = $clients['data'];
    }
    if(count($clients) > 50){
        $clients = array_slice($clients,0,49);
    }
}
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <?php 
        $title = "Handy Matcher | Directory of Moving and Home Improvement Professionals";
        $description = "Handy Matcher | Moving and Home Improvement. It is the simplest way to find and book top-rated local home services. Connect with trusted home repair and improvement contractors.";
        require_once '/var/www/html/tpl/head.php';
    ?>
    <style>
        .btn-white{
            background: #f4f4f4;
            border: 1px solid #ced4da;
        }
    </style>
    <link rel="stylesheet" href="https://handymatcher.com/css/directory.css">
</head>

<body>
    
    <?php require_once($fullPath."tpl/header.php")?>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-4 mb-3">
                    <div class="input-group">
                        <input value="<?= $q ?>" type="text" id="search" placeholder="Search For..." class="form-control">
                        <div class="input-group-append">
                            <button onclick="search()" class="btn btn-white"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group" id="errorDiv">
                        <?php if($error){
                        echo $error;
                        } ?>
                    </div>
                </div>
            </div>
            <div class="row" id="companies">
                <?php foreach($clients as $client){ /*if(!$client['url']){continue;}*/?>
                <div class="col-md-12">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3">
                                    <?php if(isset($client['logo']) && $client['logo']){ ?>
                                    <img style="max-height:74px;max-width: -webkit-fill-available;margin: 0 auto;display: block;" src="<?= $client['logo'] ?>" alt="<?= $client['companyname'] ?> Logo">
                                    <?php }else{ ?>
                                    <img style="max-height:74px;max-width: -webkit-fill-available;margin: 0 auto;display: block;" src="https://handymatcher.com/images/defaultLogo.jpg" alt="<?= $client['companyname'] ?> Logo">
                                    <?php } ?>
                                </div>
                                <div class="col-9">
                                    <h5 class="card-title"><?= $client['companyname'] ?></h5>
                                    <p class="card-text"><?php if ($client['phone']){echo "Phone: ".$client['phone'];} ?><?php if ($client['email']){echo " Email: ".$client['email'];} ?>
                                        <?php if(isset($client['url'])){?>
                                            <a href="https://handymatcher.com/company/<?= $client['url'] ?>" class="btn btn-default" style="background:goldenrod;color:black;float: right;width: fit-content;">More Info</a>
                                        <?php }else{?>
                                            <a href="https://handymatcher.com/moving/Company-<?= $client['id'] ?>" class="btn btn-default" style="background:goldenrod;color:black;float: right;width: fit-content;">Get an estimate</a>
                                        <?php } ?></p>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </main>
    <?php require_once($fullPath."tpl/footer.php")?>
    <script src="https://use.fontawesome.com/7e5b532868.js"></script>
    <script>
        var page = 1;
//        function showMore(obj){
//            isRunning = true;
//            $("#showMoreH").text("Loading...");
//            page++;
//            history.pushState("", "", "/directory/Page-"+page);
//            $.ajax({
//                url: "https://handymatcher.com/movingDir/pager.php",
//                method: "POST",
//                data: {
//                    page:page
//                },
//                cache: false,
//                async: true
//            }).done(function (data) {
//                try {
//                    $("#showMore").remove();
//                    data = JSON.parse(data);
//                    for(var i = 0; i<data.length;i++){
//                        setCompanies(data[i]);
//                    }
//                    $("#companies").html($("#companies").html()+ '<div class="col-md-12" style="cursor: pointer;" id="showMore" onclick="showMore(this)"><div class="card mb-3"><div class="card-body text-center"><h5 class="card-title text-muted" id="showMoreH">Show More</h5></div></div></div>');
//                    isRunning = false;
//                }catch (e) {
//                    isRunning = false;
//                }
//
//            });
//        }
//        function setCompanies(data){
//            var container = document.getElementById("companies");
//            
//            var div12 = document.createElement("div");
//            div12.classList.add("col-md-12");
//            
//            var divCard = document.createElement('div');
//            divCard.classList.add("card");
//            divCard.classList.add("mb-3");
//            
//            var divCardBody = document.createElement('div');
//            divCardBody.classList.add("card-body");
//            
//            var row = document.createElement("div");
//            row.classList.add('row');
//            
//            var divImg = document.createElement("div");
//            divImg.classList.add("col-3");
//            var img = document.createElement('img');
//            img.style.maxHeight = "74px";
//            img.style.maxWidth = "-webkit-fill-available";
//            img.style.margin = "0 auto";
//            img.style.display = "block";
//            img.setAttribute('alt',data.companyname);
//            if(data.logo){
//                img.setAttribute('src',data.logo);
//            }else{
//                img.setAttribute('src',"https://handymatcher.com/images/defaultLogo.jpg");
//            }
//            divImg.appendChild(img);
//            row.appendChild(divImg);
//            
//            var col9 = document.createElement("div");
//            col9.classList.add('col-9');
//            var h5 = document.createElement("h5");
//            h5.classList.add("card-title");
//            h5.innerText = data.companyname;
//            
//            var p = document.createElement("p");
//            p.classList.add("card-text");
//            p.innerHTML = "";
//            if(data.phone){
//                p.innerHTML = p.innerHTML + "Phone: " + data.phone + " ";
//            }
//            if(data.email){
//                p.innerHTML = p.innerHTML + "Email: " + data.email + " ";
//            }
//            if(data.id){
//                p.innerHTML = p.innerHTML+ '<a href="https://handymatcher.com/company/'+data.id+'" class="btn btn-default" style="background:goldenrod;color:black;float: right;width: fit-content;font-weight: bold;">More Info</a>';
//            }
//            col9.appendChild(h5);
//            col9.appendChild(p);
//            row.appendChild(col9);
//            divCardBody.appendChild(row);
//            divCard.appendChild(divCardBody);
//            div12.appendChild(divCard);
//            container.appendChild(div12);
//        }
        var companies = [];
        $(document).ready(function(){
            $("#companies .col-md-4").each(function(){
                companies.push($(this));
            });
        });
        function search(){
            var v = $("#search").val();
            v = v.replace(/\s+/g,'-');
            if(v.match(/^[0-9A-Za-z\s\-]+$/)){
                v = v.replace(/\s+/g,'-');
                window.location.href = "https://handymatcher.com/directory/search/"+v;
            }else{
                $("#errorDiv").html("<small class='text-danger'>Query must contain only letters, numbers and spaces. Please try again</small>");
            }
        }
        $(document).keyup(function (e) {
            if ($("#search").is(":focus")){
                $("#errorDiv").html("");
            }
            if ($("#search").is(":focus") && (e.keyCode == 13)) {
                search();
            }
        });
        function openService(id){
            switch(id){
                case "": 
                    break;
                case '1':
                    window.location.href = "https://handymatcher.com/localMoving.php?ref=<?= $ref ?>";
                    break;
                case '2':
                    window.location.href = "https://handymatcher.com/longDistanceMoving.php?ref=<?= $ref ?>";
                    break;
                case '3':
                    window.location.href = "https://handymatcher.com/moving.php?ref=<?= $ref ?>";
                    break;
                case '4':
                    window.location.href = "https://handymatcher.com/moving.php?ref=<?= $ref ?>";
                    break;
                default:
                    alert("in development check back soon!");
                    break;
            }
        }
    </script>
</body>
</html>
