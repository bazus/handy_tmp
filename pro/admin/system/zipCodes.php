<?php
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/style.css">
        <title>Zip Codes</title>
    </head>
    <body>
    <header>
        <div class="container-fluid">
            <div class="row" style="padding: 0">
                <div class="col-12" style="padding: 0">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a class="navbar-brand" href="#">Home Page</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php">Leads</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="pings.php">Ping</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="posts.php">Post</a>
                                </li>
                                <li class="nav-item active"><li>
                                    <a class="nav-link" href="zipCodes.php">Zip Codes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="clients.php">Clients</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="#">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="container" style="margin-top: 15px;">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="newZipCode" placeholder="New Zip Code">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <input type="number" min="0" max="1000" class="form-control" id="newZipCodeLeads" placeholder="Number Of Leads">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="newZipCodePrice" placeholder="Price Each">
                                                <span class="input-group-append">
                                                    <button class="btn btn-primary" onclick="addNewZipCode()">Add New</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                                <tr>
                                    <th>Zip Code</th>
                                    <th>Price</th>
                                    <th>Status</th>
                                    <th>Number Of Leads</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody id="myZipCodesTable">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="assets/js/zipCodes.min.js"></script>
    </body>
</html>