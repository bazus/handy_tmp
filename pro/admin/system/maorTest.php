<?php
if (isset($_GET['zipCode']) && isset($_GET['radius'])) {
    require_once("../classes/server/DB.php");
    require_once("../classes/calculate/zipCodes.php");

    $zipCodes = new zipCodes();

    $originalZip = $_GET['zipCode'];
    $originalRadius = $_GET['radius'];

    $a = $zipCodes->getZipCodes($originalZip, $originalRadius);

    if ($a['status'] == true) {
        foreach ($a['zipCodes'] as $zipCode) {
            echo $zipCode . "<br>";
        }
    }
}else{
    echo "Please use URL parameters ?zipCode=XXXXX&radius=XXX";
}