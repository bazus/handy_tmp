<?php
require_once("../../classes/server/DB.php");
require_once("../../classes/clients/clients.php");

$clients = new clients();
$myClients = $clients->getData();
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="../assets/css/style.css">
        <title>Send Lead</title>
    </head>
    <body>
    <header>
        <div class="container-fluid">
            <div class="row" style="padding: 0">
                <div class="col-12" style="padding: 0">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a class="navbar-brand" href="#">Home Page</a>
                        <button class="navbar-toggler" class="form-control" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="../index.php">Leads</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="../pings.php">Ping</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="../posts.php">Post</a>
                                </li>
                                <li class="nav-item"><li>
                                    <a class="nav-link" href="../zipCodes.php">Zip Codes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="../clients.php">Clients</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="#">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="container" style="margin-top: 15px;">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="form-group">
                        <input class="form-control" type="text" id="firstName" placeholder="First Name">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" id="lastName" placeholder="Last Name">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" id="phone" placeholder="Phone">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" id="email" placeholder="Email">
                    </div>

                    <div class="spacer"></div>

                    <div class="form-group">
                        <input class="form-control" type="text" id="fromZip" placeholder="From ZIP">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" id="toZip" placeholder="To ZIP">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" id="fromState" placeholder="From State">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" id="toState" placeholder="To State">
                    </div>

                    <div class="spacer"></div>

                    <div class="form-group">
                        <select class="form-control" id="moveSize">
                            <option value="" selected="" disabled="">Select Home Size</option>
                            <option value="2">Studio</option>
                            <option value="3">1 Bedroom Home</option>
                            <option value="4">2 Bedroom Home</option>
                            <option value="5">3 Bedroom Home</option>
                            <option value="6">4 Bedroom Home</option>
                            <option value="7">4+ Bedroom Home</option>
                            <option value="1">Partial</option>
                            <option disabled="">—————-</option>
                            <option value="7">Office Move</option>
                            <option value="7">Commercial Move</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" id="moveDate" placeholder="Move Date">
                    </div>

                    <div class="form-group">
                        <input type="button" class="btn btn-primary" onclick="addLead()" value="Add Lead">
                    </div>
                </div>
            </div>
        </div>
    </main>
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>

            function addLead(){
                var firstName = $("#firstName").val();
                var lastName = $("#lastName").val();
                var phone = $("#phone").val();
                var email = $("#email").val();
                var fromZip = $("#fromZip").val();
                var toZip = $("#toZip").val();
                var fromState = $("#fromState").val();
                var toState = $("#toState").val();
                var moveSize = $("#moveSize").val();
                var moveDate = $("#moveDate").val();

                $.ajax({
                    url: "../../actions/leads/addLead.php",
                    method:"POST",
                    data:{
                        firstName: firstName,
                        lastName: lastName,
                        phone: phone,
                        email: email,
                        fromZip: fromZip,
                        toZip: toZip,
                        fromState: fromState,
                        toState: toState,
                        moveSize: moveSize,
                        moveDate: moveDate
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        console.log(data);
                        $("input :not(.btn)").each(function () {
                            $(this).val("");
                        });
                        if (data['status'] == true){
                            alert("Lead Added");
                        }else{
                            alert(data['success']);
                        }
                    }catch (e) {
                        console.log(data);
                        console.log(e);
                        alert(e);
                        alert(data);
                    }
                });
            }
        </script>
    </body>
</html>