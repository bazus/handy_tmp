<?php
require_once("../../classes/server/DB.php");
require_once("../../classes/clients/clients.php");
$clients = new clients();
$allClients = $clients->getData();
if ($allClients['status'] == true) {
    $allClients = $allClients['clients'];
    $client = [];

    foreach ($allClients as $allClient) {
        if ($allClient['id'] == $_GET['id']) {
            $client = $allClient;
        }
    }
}else{
    die("error loading client zip-codes please contact maor ASAP with 'error 101 clientZipCodes'");
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/css/style.css">
    <title>Add Client (<?= $client['name'] ?>) Zip Codes</title>
</head>
<body>
<header>
    <div class="container-fluid">
        <div class="row" style="padding: 0">
            <div class="col-12" style="padding: 0">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="#">Home Page</a>
                    <button class="navbar-toggler" class="form-control" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="../index.php">Leads</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../pings.php">Ping</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../posts.php">Post</a>
                            </li>
                            <li class="nav-item"><li>
                                <a class="nav-link" href="../zipCodes.php">Zip Codes</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="../clients.php">Clients</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">Settings</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<main>
    <div class="container" style="margin-top: 15px;">
        <div class="row">
            <div class="col-12 text-center">
                <div>
                    <h2>Showing Client (<?= $client['name'] ?>) Zip Codes</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <div class="row">
                                    <div class="col-3"></div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input type="text" id="newZipCode" class="form-control" placeholder="Client (<?= $client['name'] ?>) New Zip Code">
                                            <span class="input-group-append">
                                                <button class="btn btn-primary" onclick="addZipCode()">Add Zip Code</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                    </div>
                                </div>
                            </tr>
                            <tr>
                                <th>Zip Code</th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tbody id="zipCodes">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    function getClientZipCodes(){
        $.ajax({
            url: "../../actions/clients/getClientZipCodes.php",
            method: "POST",
            data:{
                id: <?= $client['id']?>
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data.status == true) {
                    document.getElementById("zipCodes").innerHTML = "";
                    for (var i = 0; i < data.zipCodes.length; i++) {
                        setClientZipCodes(data.zipCodes[i]);
                    }
                }else{
                    document.getElementById("zipCodes").innerHTML = "<tr><td colspan='2' class='text-center'>No Zip Codes</td></tr>";
                }
            }catch (e) {
                console.log(data);
                console.log(e);
            }
        });
    }
    function setClientZipCodes(data) {
        var container = document.getElementById("zipCodes");

        var tr = document.createElement("tr");
        var td = document.createElement("td");
        var tdText = document.createTextNode(data.zipCode);
        td.appendChild(tdText);
        tr.appendChild(td);
        
        var td = document.createElement("td");
        if (data.isActive == 1){
            td.classList.add("text-primary");
            var tdText = document.createTextNode("Active");
        } else{
            td.classList.add("text-danger");
            var tdText = document.createTextNode("Inactive");
        }

        td.appendChild(tdText);
        tr.appendChild(td);

        container.appendChild(tr);
    }

    function addZipCode(){
        var newZipCode = $("#newZipCode").val();
        if (newZipCode){}else{console.log('error');return false;}
        $.ajax({
            url: "../../actions/clients/addClientZipCodes.php",
            method:"POST",
            data:{
                id: <?= $client['id'] ?>,
                zipCode:newZipCode
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data.status == true) {
                    getClientZipCodes();
                    $("#newZipCode").val("");
                }else{
                    console.log("error");
                    console.log(data);
                }
            }catch (e) {
                console.log(data);
                console.log(e);
            }
        });
    }
    $(document).ready(function() {
        getClientZipCodes();
    });
</script>
</body>
</html>