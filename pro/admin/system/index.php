<?php
require_once '/var/www/html/config/database.php';
use Illuminate\Database\Capsule\Manager as DB;

$leads = DB::table('leads')->get();


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/style.css">
        <title>Leads</title>
    </head>
    <body>
    <header>
        <div class="container-fluid">
            <div class="row" style="padding: 0">
                <div class="col-12" style="padding: 0">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a class="navbar-brand" href="#">Home Page</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="index.php">Leads</a>
                                </li>
                                <li class="nav-item disabled">
                                    <a class="nav-link disabled" href="pings.php">Ping</a>
                                </li>
                                <li class="nav-item disabled">
                                    <a class="nav-link disabled" href="posts.php">Post</a>
                                </li>
                                <li class="nav-item disabled"><li>
                                    <a class="nav-link disabled" href="zipCodes.php">Zip Codes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="clients.php">Clients</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="#">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-bottom: -1px;">
                            <thead>
                                <tr>
                                    <th style="width: 80%;"></th>
                                    <th style="text-align: right!important;">
                                        <a href="leads/sendLead.php" class="btn btn-outline-primary">Send Lead</a>
                                    </th>
                                    <th style="text-align: right!important;">
                                        <a href="leads/sentLeads.php" class="btn btn-primary">Show Sent Leads</a>
                                    </th>
                                    <th style="text-align: right!important;">
                                        <a href="leads/addLead.php" class="btn btn-primary">Add Lead</a>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Full Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Moving From</th>
                                    <th>Moving To</th>
                                    <th>Move Size</th>
                                    <th>Move Date</th>
                                    <th>Price</th>
                                    <th>Provider</th>
                                    <th>Status</th>
                                    <th>Send Lead</th>
                                </tr>
                            </thead>
                            <tbody id="myLeadsTable">
                                     <?php foreach($leads as $lead): ?>
                                     	<tr>
                                     
                                         <td><?php echo $lead->id; ?></td>
                                         <td><?php echo $lead->firstName .$lead->lastName ; ?></td>
                                         <td><?php echo $lead->phone; ?></td>
                                         <td><?php echo $lead->email; ?></td>
                                         <td><?php echo $lead->fromZip; ?></td>
                                         <td><?php echo $lead->toZip; ?></td>
                                         <td>?</td>
                                         <td><?php echo $lead->moveDate; ?></td>
                                         <td>?</td>
                                         <td>?</td>
                                         <td>?</td>
                                         <td>?</td>
                                 
										</tr>                                     
                                     <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="assets/js/script.min.js"></script>
    </body>
</html>