function getClients(){
    $.ajax({
        url: "../actions/clients/getClients.php",
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status == true) {
                document.getElementById("myClientsTable").innerHTML = "";
                for (var i = 0; i < data.clients.length; i++) {
                    setClients(data.clients[i]);
                }
            }else{
                document.getElementById("myClientsTable").innerHTML = "<tr><td colspan='8' class='text-center'>No Clients</td></tr>";
            }
        }catch (e) {
            console.log(data);
            console.log(e);
        }
    });
}
function setClients(d) {

    var thead = document.getElementById("myClientsTable");

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.id);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.name);
    td.appendChild(tdText);
    tr.appendChild(td);


    var td = document.createElement("td");
    var tdText = document.createTextNode(d.phone);
    td.appendChild(tdText);
    tr.appendChild(td);


    var td = document.createElement("td");
    var tdText = document.createTextNode(d.email);
    td.appendChild(tdText);
    tr.appendChild(td);


    var td = document.createElement("td");
    var tdText = document.createTextNode("$"+d.balance);
    td.appendChild(tdText);
    tr.appendChild(td);


    var td = document.createElement("td");
    var tdText = document.createTextNode("$"+d.leadPrice);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    if (d.postMethods == 1 || d.postMethods == '1') {
        var tdText = document.createTextNode("Granot");
    }
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var a = document.createElement("a");
    a.setAttribute("href","clients/addZipCode.php?id="+d.id);
    var aText = document.createTextNode("Edit Zip Codes");
    a.appendChild(aText);
    td.appendChild(a);
    tr.appendChild(td);

    thead.appendChild(tr);
}
getClients();