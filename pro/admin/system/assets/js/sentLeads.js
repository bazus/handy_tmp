function getLeads(){
    $.ajax({
        url: "../../actions/leads/getSentLeads.php",
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status == true) {
                document.getElementById("myLeadsTable").innerHTML = "";
                for (var i = 0; i < data.leads.length; i++) {
                    setLeads(data.leads[i]);
                }
            }else{
                document.getElementById("myLeadsTable").innerHTML = "<tr><td colspan='11' class='text-center'>No Leads</td></tr>";
            }
        }catch (e) {
            console.log(e);
        }
    });
}
function setLeads(d) {

    var thead = document.getElementById("myLeadsTable");

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.id);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.fullName);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.phone);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.email);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.fromData);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.toData);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.sentTo);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.sentAt);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.status);
    td.appendChild(tdText);
    tr.appendChild(td);

    thead.appendChild(tr);
}
getLeads();