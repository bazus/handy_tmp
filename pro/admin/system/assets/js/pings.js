function getPings(){
    var isTest = document.getElementById("showTest").checked;
    $.ajax({
        url: "../actions/pings/getPings.php",
        method: "POST",
        data:{
            isTest:isTest
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status == true) {
                document.getElementById("myPingsTable").innerHTML = "";
                for (var i = 0; i < data.pings.length; i++) {
                    setPings(data.pings[i]);
                }
            }else{
                document.getElementById("myPingsTable").innerHTML = "<tr><td colspan='7' class='text-center'>No Pings</td></tr>";
            }
        }catch (e) {
            console.log(e);
        }
    });
}
function setPings(d) {

    var thead = document.getElementById("myPingsTable");

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.id);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.fromState+" "+d.fromZipCode+" "+d.fromCity);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.toState+" "+d.toZipCode+" "+d.toCity);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    if (d.moveSize == 1){
        var tdText = document.createTextNode("Partial move -> Under 2000");
    }else if (d.moveSize == 2){
        var tdText = document.createTextNode("Studio move -> 2000 - 2500");
    }else if (d.moveSize == 3){
        var tdText = document.createTextNode("1 Bedroom move -> 2500-4000");
    }else if (d.moveSize == 4){
        var tdText = document.createTextNode("2 Bedroom move -> 4000-6300");
    }else if (d.moveSize == 5){
        var tdText = document.createTextNode("3 Bedroom move -> 6300-8700");
    }else if (d.moveSize == 6){
        var tdText = document.createTextNode("4 Bedroom move -> 8700-11000");
    }else if (d.moveSize == 7){
        var tdText = document.createTextNode("Commercial move OR 4+ and above Bedroom move -> Above 11000");
    }
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    console.log(d);
    var tdText = document.createTextNode(d.moveDate);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode("$"+d.priceOffer);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.providerName);
    td.appendChild(tdText);
    tr.appendChild(td);

    thead.appendChild(tr);
}
getPings();