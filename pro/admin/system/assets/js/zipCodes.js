function getZipCodes(){
    $.ajax({
        url: "../actions/zipCodes/getZipCodes.php",
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status == true) {
                document.getElementById("myZipCodesTable").innerHTML = "";
                for (var i = 0; i < data.zipCodes.length; i++) {
                    setZipCodes(data.zipCodes[i]);
                }
            }else{
                document.getElementById("myZipCodesTable").innerHTML = "<tr><td colspan='5' class='text-center'>No Zip Codes</td></tr>";
            }
        }catch (e) {
            console.log(data);
            console.log(e);
        }
    });
}
function setZipCodes(d) {

    var thead = document.getElementById("myZipCodesTable");

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.zipCode);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode("$"+d.price);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var span = document.createElement("span");
    span.classList.add("badge");
    if (d.isActive == 1){
        var spanText = document.createTextNode("Active");
        span.classList.add("badge-primary");
    }else{
        var spanText = document.createTextNode("Inactive");
        span.classList.add("badge-danger");
    }
    span.appendChild(spanText);
    td.appendChild(span);
    tr.appendChild(td);

    var td = document.createElement("td");
    var leadsPlus = document.createElement("button");
    leadsPlus.setAttribute("onclick","leadsNumber("+d.id+",true)");
    leadsPlus.style.margin = "5px";
    leadsPlus.classList.add("btn");
    leadsPlus.classList.add("btn-sm");
    leadsPlus.classList.add("btn-default");
    var leadsPlusText = document.createTextNode("+");
    leadsPlus.appendChild(leadsPlusText);
    td.appendChild(leadsPlus);
    var leadsText = document.createTextNode(d.receiveNumberOFLeads);
    td.appendChild(leadsText);
    var leadsMinus = document.createElement("button");
    leadsMinus.setAttribute("onclick","leadsNumber("+d.id+",false)");
    leadsMinus.style.margin = "5px";
    leadsMinus.classList.add("btn");
    leadsMinus.classList.add("btn-sm");
    leadsMinus.classList.add("btn-default");
    var leadsMinusText = document.createTextNode("-");
    leadsMinus.appendChild(leadsMinusText);
    td.appendChild(leadsMinus);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode("test");
    td.appendChild(tdText);
    tr.appendChild(td);

    thead.appendChild(tr);
}
function leadsNumber(id,action) {
    $.ajax({
        url: "../actions/zipCodes/setZipCode.php",
        method:"POST",
        data:{
          id:id,
          action:action
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status == true) {
                getZipCodes();
            }else{
                console.log("error");
                console.log(data);
            }
        }catch (e) {
            console.log(data);
            console.log(e);
        }
    });
}
function addNewZipCode(){
    var zipCode = $("#newZipCode").val();
    var leads = $("#newZipCodeLeads").val();
    var price = $("#newZipCodePrice").val();
    $.ajax({
        url: "../actions/zipCodes/addZipCode.php",
        method:"POST",
        data:{
            zipCode:zipCode,
            leads:leads,
            price:price
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status == true) {
                getZipCodes();
            }else{
                console.log("error");
                console.log(data);
            }
        }catch (e) {
            console.log(data);
            console.log(e);
        }
    });
}
getZipCodes();