var clients = [];
function getLeads(){
    $.ajax({
        url: "../actions/leads/getLeads.php",
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status == true) {
                document.getElementById("myLeadsTable").innerHTML = "";
                clients = data.clients;
                for (var i = 0; i < data.leads.length; i++) {
                    setLeads(data.leads[i]);
                }
            }else{
                document.getElementById("myLeadsTable").innerHTML = "<tr><td colspan='11' class='text-center'>No Leads</td></tr>";
            }
        }catch (e) {
            console.log(e);
        }
    });
}
function setLeads(d) {

    var thead = document.getElementById("myLeadsTable");

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.id);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.fullName);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.phone1);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.email);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.fromZipCode+" "+d.fromState+" "+d.fromCity);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.toZipCode+" "+d.toState+" "+d.toCity);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    if (d.moveSize == 1){
        var tdText = document.createTextNode("Partial move -> Under 2000");
    }else if (d.moveSize == 2){
        var tdText = document.createTextNode("Studio move -> 2000 - 2500");
    }else if (d.moveSize == 3){
        var tdText = document.createTextNode("1 Bedroom move -> 2500-4000");
    }else if (d.moveSize == 4){
        var tdText = document.createTextNode("2 Bedroom move -> 4000-6300");
    }else if (d.moveSize == 5){
        var tdText = document.createTextNode("3 Bedroom move -> 6300-8700");
    }else if (d.moveSize == 6){
        var tdText = document.createTextNode("4 Bedroom move -> 8700-11000");
    }else if (d.moveSize == 7){
        var tdText = document.createTextNode("Commercial move OR 4+ and above Bedroom move -> Above 11000");
    }
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.movingDate);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode("$"+d.price);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(d.providerName);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    if (d.status == 0){
        var tdText = document.createTextNode("Received");
    }else if (d.status == 1){
        var tdText = document.createTextNode("Sold");
    }else if (d.status == 2){
        var tdText = document.createTextNode("Bad Lead");
    }
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    td.id = d.id+"-td";
    var select =  buildClients(d.id);

    td.appendChild(select);
    tr.appendChild(td);

    thead.appendChild(tr);
}
function buildClients(id) {
    var select = document.createElement("select");
    select.id = id+"-select";
    select.classList.add("form-control");
    select.setAttribute("onchange","setSendLead("+id+",this)");
    var option = document.createElement("option");
    option.innerText = "Send Lead";
    option.setAttribute("value","");
    select.appendChild(option);

    for(var x = 0; x<clients.length;x++){
        var option = document.createElement("option");
        option.innerText = clients[x].name;
        option.setAttribute("value",clients[x].id);
        select.appendChild(option);
    }
    return select;
}
function setSendLead(id,obj) {
    var el = $("#"+id+"-td");
    el.html("<input onclick='sendLead("+id+","+$(obj).val()+")' type='button' class='btn btn-primary' value='Send Lead'><input type='button' onclick='cancelSendLead("+id+")' class='btn btn-warning' value='Cancel'>");
}
function cancelSendLead(id) {
    var el = document.getElementById(id+"-td");
    el.innerHTML = "";
    var select =  buildClients(id);
    el.appendChild(select);
}
function sendLead(id,clientId) {
    console.log(clientId);
    var clientId = $("#"+id+"-select").val();
    console.log(clientId);
    if (!clientId){
        return false;
    }
    $.ajax({
        url: "../actions/leads/sendLead.php",
        async: true,
        data:{
            leadId:id,
            clientId:clientId
        }
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            alert(data);
        }catch (e) {
            console.log(e);
        }
    });
}
getLeads();