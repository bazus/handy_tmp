<?php
if (isset($_POST['id']) && isset($_POST['zipCode'])) {
    require_once("../../classes/server/DB.php");
    require_once("../../classes/clients/clientsZipCodes.php");

    $clientsZipCodes = new clientsZipCodes($_POST['id']);
    $zipCodes = $clientsZipCodes->addData($_POST['zipCode']);
    echo json_encode($zipCodes);
}else{
    echo json_encode(['status'=>false,'success'=>false]);
}