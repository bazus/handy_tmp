<?php
if (isset($_POST['id'])) {
    require_once("../../classes/server/DB.php");
    require_once("../../classes/clients/clientsZipCodes.php");

    $clientsZipCodes = new clientsZipCodes($_POST['id']);
    $zipCodes = $clientsZipCodes->getData();
    echo json_encode($zipCodes);
}else{
    echo json_encode(['status'=>false,'success'=>false]);
}