<?php
require_once("../../classes/server/DB.php");
require_once("../../classes/clients/clients.php");
$P = $_POST;
$data = [
    "name"=>$P['clientName'],
    "phone"=>$P['clientPhone'],
    "email"=>$P['clientEmail'],
    "balance"=>$P['clientBalance'],
    "leadPrice"=>$P['clientLeadPrice'],
    "postMethods"=>$P['clientPostMethod'],
    "postAddress"=>$P['clientPostEmails']
];
$clients = new clients();
$addClient = $clients->addData($data);
echo json_encode($addClient);