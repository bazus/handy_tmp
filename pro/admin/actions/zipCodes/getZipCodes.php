<?php
require_once("../../classes/server/DB.php");
require_once("../../pingpost/classes/zipCodes/zipCodes.php");
$zipCodes = new zipCodes();
$myZipCodes = $zipCodes->getAllZipCodes();

echo json_encode($myZipCodes);