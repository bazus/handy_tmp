<?php
require_once("../../classes/server/DB.php");
require_once("../../pingpost/classes/zipCodes/zipCodes.php");

$zipCode = $_POST['zipCode'];
$leads = $_POST['leads'];
$price = $_POST['price'];

$zipCodes = new zipCodes();
$addNewZipCode = $zipCodes->addNewZipCode($zipCode,$leads,$price);

echo json_encode($addNewZipCode);