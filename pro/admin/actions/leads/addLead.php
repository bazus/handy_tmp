<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once("../../classes/server/DB.php");
require_once("../../classes/leads/leads.php");

$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$phone = $_POST['phone'];
$email = $_POST['email'];

$fromZip = $_POST['fromZip'];
$toZip = $_POST['toZip'];

$fromState = $_POST['fromState'];
$toState = $_POST['toState'];

$moveSize = $_POST['moveSize'];
$moveDate = date("Y-m-d 00:00:00",strtotime($_POST['moveDate']));

$data = [];
$data['fullName'] = $firstName." ".$lastName;
$data['phone'] = $phone;
$data['email'] = $email;
$data['fromCountry'] = "US";
$data['fromZipCode'] = $fromZip;
$data['fromState'] = $fromState;
$data['toCountry'] = "US";
$data['toZipCode'] = $toZip;
$data['toState'] = $toState;
$data['moveSize'] = $moveSize;
$data['moveDate'] = $moveDate;
$data['providerId'] = 1;

$res = [];

try{
    $leads = new leads();
    $res = $leads->addLead($data);

    echo json_encode($res);
}catch (Exception $e){
    echo json_encode($e);
}