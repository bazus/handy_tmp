<?php
require_once("../../classes/server/DB.php");
require_once("../../classes/leads/leads.php");
require_once("../../classes/clients/clients.php");
$clients = new clients();
$myClients = $clients->getData();
$leads = new leads();
$myLeads = $leads->getData();
foreach ($myLeads['leads'] as &$lead){
    if (!$lead['fromState']){
        $lead['fromState'] = "";
    }
    if (!$lead['toState']){
        $lead['fromState'] = "";
    }
    if (!$lead['fromCity']){
        $lead['fromCity'] = "";
    }
    if (!$lead['toCity']){
        $lead['toCity'] = "";
    }
}
$myLeads['clients'] = $myClients['clients'];
echo json_encode($myLeads);