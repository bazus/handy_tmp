<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once("../../classes/server/DB.php");
require_once("../../classes/clients/clients.php");
require_once("../../classes/leads/sentLeads.php");

$sentLeads = new sentLeads();
$id = $_POST['clientId'];

$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$phone = $_POST['phone'];
$email = $_POST['email'];

$fromZip = $_POST['fromZip'];
$toZip = $_POST['toZip'];

$fromState = $_POST['fromState'];
$toState = $_POST['toState'];

$moveSize = $_POST['moveSize'];
$moveDate = date("m/d/Y",strtotime($_POST['moveDate']));

$clients = new clients();
$client = $clients->getClient($id);

$res = [];
if ($client['status'] == true) {
    $clientContactMethods = $client['client']['postAddress'];
    $clientContactMethods = explode(",",$clientContactMethods);
    foreach ($clientContactMethods as &$method){
        $method = trim($method);
    }

    foreach ($clientContactMethods as $sendLeadTo){

        $apiID = "C2E03772A9EA";

        $leadId = 000;

        $fromCountry = "US";
        $toCountry = "US";

        if ($moveSize == 2){
            $weight = "1500";
        }else if ($moveSize == 3){
            $weight = "3000";
        }else if ($moveSize == 4){
            $weight = "4500";
        }else if ($moveSize == 5){
            $weight = "8000";
        }else if ($moveSize == 6){
            $weight = "10000";
        }else{
            $weight = "12000";
        }

        $source = "leadRift";
        if ($toState == $fromState){
            $leadType = "101";
        }else{
            $leadType = "102";
        }
        $postURL = "https://lead.hellomoving.com/LEADSGWHTTP.lidgw?&API_ID=$apiID";
        $fields_string = "&moverref=$sendLeadTo&servtypeid=$leadType&leadno=$leadId&firstname=$firstName&lastName=$lastName&ostate=$fromState&ozip=$fromZip&ocountry=$fromCountry&dstate=$toState&dzip=$toZip&dcountry=$toCountry&weight=$weight&movesize=$moveSize&movedte=$moveDate&email=$email&phone1=$phone&source=$source&label=$source";

        //open connection
        $ch_new = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch_new,CURLOPT_URL,$postURL);
        curl_setopt($ch_new,CURLOPT_POST,1);
        curl_setopt($ch_new,CURLOPT_POSTFIELDS,$fields_string);
        curl_setopt($ch_new, CURLOPT_RETURNTRANSFER, 1);
        // ================================ //
        curl_setopt($ch_new, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch_new, CURLOPT_TIMEOUT, 60); //timeout in seconds
        curl_setopt($ch_new, CURLOPT_SSL_VERIFYPEER, FALSE);
        // ================================ //

        //execute post
        $resp = curl_exec($ch_new);

        if(curl_errno($ch_new)){
            $errorExist = true;
            $errorText = htmlspecialchars(curl_error($ch_new));
        }

        $code = curl_getinfo($ch_new, CURLINFO_HTTP_CODE);

        $resp = explode(",",$resp);
        if($code != 200) {
            $res[] = "error";
        }else{
            $res[] = $resp[2];
        }
        $sentLeads->addData($fields_string,$sendLeadTo,$resp);
    }
    echo json_encode($res);
}else{
    echo json_encode(false);
}