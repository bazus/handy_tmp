<?php

class zipCodes extends DB
{
    function __construct()
    {
        parent::__construct();
    }

    public function getActiveZipCodes(){

        $res = [];
        $res['status'] = false;
        $res['zipCodes'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT zipCode FROM joseianq_myApi.zipCodes WHERE isDeleted=0 AND isActive=1 AND receiveNumberOFLeads>0 ORDER BY id DESC");
            }else {
                $stmt = $this->conn->prepare("SELECT zipCode FROM myApi.zipCodes WHERE isDeleted=0 AND isActive=1 AND receiveNumberOFLeads>0 ORDER BY id DESC");
            }

            $stmt->execute();
            $stmtData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmtData) {
                foreach($stmtData AS $data) {
                    $res['zipCodes'][] = $data['zipCode'];
                }
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['zipCodes'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['zipCodes'] = [];
        }

        return $res;
    }

    public function getAllZipCodes(){

        $res = [];
        $res['status'] = false;
        $res['zipCodes'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT * FROM joseianq_myApi.zipCodes WHERE isDeleted=0 ORDER BY id DESC");
            }else {
                $stmt = $this->conn->prepare("SELECT * FROM myApi.zipCodes WHERE isDeleted=0 ORDER BY id DESC");
            }

            $stmt->execute();
            $stmtData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmtData) {
                foreach($stmtData AS $data) {
                    $res['zipCodes'][] = $data;
                }
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['zipCodes'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['zipCodes'] = [];
        }

        return $res;
    }

    public function getZipCodePrice($zipCode){

        $res = [];
        $res['status'] = false;
        $res['price'] = 0.00;
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT price FROM joseianq_myApi.zipCodes WHERE isDeleted=0 AND isActive=1 AND receiveNumberOFLeads>0 AND zipCode=:zipCode ORDER BY id DESC LIMIT 1");
            }else {
                $stmt = $this->conn->prepare("SELECT price FROM myApi.zipCodes WHERE isDeleted=0 AND isActive=1 AND receiveNumberOFLeads>0 AND zipCode=:zipCode ORDER BY id DESC LIMIT 1");
            }

            $stmt->bindParam(':zipCode', $zipCode, PDO::PARAM_INT);

            $stmt->execute();
            $stmtData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmtData) {
                $res['price'] = $stmtData[0]['price'];
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['price'] = 0.00;
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['price'] = 0.00;
        }

        return $res;
    }

    public function updateZipCodeLeads($id,$up = true){

        $res = [];
        $res['status'] = false;
        $res['success'] = false;
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                if ($up == true) {
                    $stmt = $this->conn->prepare("UPDATE joseianq_myApi.zipCodes SET receiveNumberOFLeads=(receiveNumberOFLeads+1) WHERE id=:id");
                }else{
                    $stmt = $this->conn->prepare("UPDATE joseianq_myApi.zipCodes SET receiveNumberOFLeads=(receiveNumberOFLeads-1) WHERE id=:id");
                }
            }else {
                if ($up == true){
                    $stmt = $this->conn->prepare("UPDATE myApi.zipCodes SET receiveNumberOFLeads=(receiveNumberOFLeads+1) WHERE id=:id");
                }else{
                    $stmt = $this->conn->prepare("UPDATE myApi.zipCodes SET receiveNumberOFLeads=(receiveNumberOFLeads-1) WHERE id=:id");
                }
            }

            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            $exec = $stmt->execute();
            if ($exec) {
                $res['success'] = true;
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['success'] = false;
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['success'] = false;
        }

        return $res;
    }
    public function updateZipCodeLeadsByZipCode($zipCode,$up = true){

        $res = [];
        $res['status'] = false;
        $res['success'] = false;
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                if ($up == true) {
                    $stmt = $this->conn->prepare("UPDATE joseianq_myApi.zipCodes SET receiveNumberOFLeads=(receiveNumberOFLeads+1) WHERE zipCode=:zipCode");
                }else{
                    $stmt = $this->conn->prepare("UPDATE joseianq_myApi.zipCodes SET receiveNumberOFLeads=(receiveNumberOFLeads-1) WHERE zipCode=:zipCode");
                }
            }else {
                if ($up == true){
                    $stmt = $this->conn->prepare("UPDATE myApi.zipCodes SET receiveNumberOFLeads=(receiveNumberOFLeads+1) WHERE zipCode=:zipCode");
                }else{
                    $stmt = $this->conn->prepare("UPDATE myApi.zipCodes SET receiveNumberOFLeads=(receiveNumberOFLeads-1) WHERE zipCode=:zipCode");
                }
            }

            $stmt->bindParam(':zipCode', $zipCode, PDO::PARAM_INT);

            $exec = $stmt->execute();
            if ($exec) {
                $res['success'] = true;
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['success'] = false;
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['success'] = false;
        }

        return $res;
    }
    public function addNewZipCode($zipCode,$leads,$price){
        $res = [];
        $res['status'] = false;
        $res['success'] = false;
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("INSERT INTO joseianq_myApi.zipCodes(zipCode,price,receiveNumberOFLeads) VALUES(:zipCode,:price,:receiveNumberOFLeads)");
            }else {
                $stmt = $this->conn->prepare("INSERT INTO myApi.zipCodes(zipCode,price,receiveNumberOFLeads) VALUES(:zipCode,:price,:receiveNumberOFLeads)");
            }

            $stmt->bindParam(':zipCode', $zipCode, PDO::PARAM_INT);
            $stmt->bindParam(':price', $price, PDO::PARAM_INT);
            $stmt->bindParam(':receiveNumberOFLeads', $leads, PDO::PARAM_INT);

            $exec = $stmt->execute();
            if ($exec) {
                $res['success'] = true;
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['success'] = false;
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['success'] = false;
        }

        return $res;
    }

}