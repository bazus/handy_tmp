<?php


class ping extends DB
{

    function __construct()
    {
        parent::__construct();
    }

    public function addPing($data,$isTest = 0){

        $res = [];
        $res['status'] = false;
        $res['success'] = false;

        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("INSERT INTO joseianq_myApi.ping(areaCode,fromCountry,fromZipCode,fromState,fromCity,toCountry,toZipCode,toState,toCity,moveSize,moveDate,providerId,priceOffer,isTest) VALUES(:areaCode,:fromCountry,:fromZipCode,:fromState,:fromCity,:toCountry,:toZipCode,:toState,:toCity,:moveSize,:moveDate,:providerId,:priceOffer,:isTest)");
            }else{
                $stmt = $this->conn->prepare("INSERT INTO myApi.ping(areaCode,fromCountry,fromZipCode,fromState,fromCity,toCountry,toZipCode,toState,toCity,moveSize,moveDate,providerId,priceOffer,isTest) VALUES(:areaCode,:fromCountry,:fromZipCode,:fromState,:fromCity,:toCountry,:toZipCode,:toState,:toCity,:moveSize,:moveDate,:providerId,:priceOffer,:isTest)");
            }

            $stmt->bindParam(':areaCode', $data['areaCode'], PDO::PARAM_STR);
            $stmt->bindParam(':fromCountry', $data['fromCountry'], PDO::PARAM_STR);
            $stmt->bindParam(':fromZipCode', $data['fromZipCode'], PDO::PARAM_STR);
            $stmt->bindParam(':fromState', $data['fromState'], PDO::PARAM_STR);
            $stmt->bindParam(':fromCity', $data['fromCity'], PDO::PARAM_STR);
            $stmt->bindParam(':toCountry', $data['toCountry'], PDO::PARAM_STR);
            $stmt->bindParam(':toZipCode', $data['toZipCode'], PDO::PARAM_STR);
            $stmt->bindParam(':toState', $data['toState'], PDO::PARAM_STR);
            $stmt->bindParam(':toCity', $data['toCity'], PDO::PARAM_STR);
            $stmt->bindParam(':moveSize', $data['moveSize'], PDO::PARAM_STR);
            $stmt->bindParam(':moveDate', $data['moveDate'], PDO::PARAM_STR);
            $stmt->bindParam(':providerId', $data['providerId'], PDO::PARAM_STR);
            $stmt->bindParam(':priceOffer', $data['priceOffer'], PDO::PARAM_STR);
            $stmt->bindParam(':isTest', $isTest, PDO::PARAM_STR);

            $stmt->execute();

            $res['success'] = true;
            $res['status'] = true;

        }catch (PDOException $e){
            $res['status'] = false;
            $res['success'] = false;
        }

        return $res;
    }
}