<?php


class leads extends DB
{

    function __construct()
    {
        parent::__construct();
    }

    public function addLead($data){

        $res = [];
        $res['status'] = false;
        $res['success'] = false;

        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("INSERT INTO joseianq_myApi.leads(fullName,phone1,email,fromCountry,fromZipCode,fromState,fromCity,toCountry,toZipCode,toState,toCity,moveSize,movingDate,userAgent,ipAddress,providerId,price) VALUES(:fullName,:phone,:email,:fromCountry,:fromZipCode,:fromState,:fromCity,:toCountry,:toZipCode,:toState,:toCity,:moveSize,:moveDate,:userAgent,:ip,:providerId,:price)");
            }else{
                $stmt = $this->conn->prepare("INSERT INTO myApi.leads(fullName,phone1,email,fromCountry,fromZipCode,fromState,fromCity,toCountry,toZipCode,toState,toCity,moveSize,movingDate,userAgent,ipAddress,providerId,price) VALUES(:fullName,:phone,:email,:fromCountry,:fromZipCode,:fromState,:fromCity,:toCountry,:toZipCode,:toState,:toCity,:moveSize,:moveDate,:userAgent,:ip,:providerId,:price)");
            }

            $stmt->bindParam(':fullName', $data['fullName'], PDO::PARAM_STR);
            $stmt->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
            $stmt->bindParam(':email', $data['email'], PDO::PARAM_STR);
            $stmt->bindParam(':fromCountry', $data['fromCountry'], PDO::PARAM_STR);
            $stmt->bindParam(':fromZipCode', $data['fromZipCode'], PDO::PARAM_STR);
            $stmt->bindParam(':fromState', $data['fromState'], PDO::PARAM_STR);
            $stmt->bindParam(':fromCity', $data['fromCity'], PDO::PARAM_STR);
            $stmt->bindParam(':toCountry', $data['toCountry'], PDO::PARAM_STR);
            $stmt->bindParam(':toZipCode', $data['toZipCode'], PDO::PARAM_STR);
            $stmt->bindParam(':toState', $data['toState'], PDO::PARAM_STR);
            $stmt->bindParam(':toCity', $data['toCity'], PDO::PARAM_STR);
            $stmt->bindParam(':moveSize', $data['moveSize'], PDO::PARAM_STR);
            $stmt->bindParam(':moveDate', $data['moveDate'], PDO::PARAM_STR);
            $stmt->bindParam(':userAgent', $data['userAgent'], PDO::PARAM_STR);
            $stmt->bindParam(':ip', $data['ip'], PDO::PARAM_STR);
            $stmt->bindParam(':providerId', $data['providerId'], PDO::PARAM_STR);
            $stmt->bindParam(':price', $data['price'], PDO::PARAM_STR);

            $stmt->execute();

            $res['success'] = true;
            $res['status'] = true;

        }catch (PDOException $e){
            $res['status'] = false;
            $res['success'] = $e;
        }

        return $res;
    }
}