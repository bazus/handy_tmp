<?php

class providers extends DB
{
    function __construct()
    {
        parent::__construct();
    }

    public function getProviderDataByKey($key){

        $res = [];
        $res['status'] = false;
        $res['provider'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT * FROM joseianq_myApi.providers WHERE providerKey=:providerKey ORDER BY id DESC LIMIT 1");
            }else {
                $stmt = $this->conn->prepare("SELECT * FROM myApi.providers WHERE providerKey=:providerKey ORDER BY id DESC LIMIT 1");
            }

            $stmt->bindParam(':providerKey', $key, PDO::PARAM_STR);
            $stmt->execute();
            $stmtData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmtData) {
                $res['provider'] = $stmtData[0];
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['provider'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['provider'] = [];
        }

        return $res;
    }

}