<?php
if (isset($_REQUEST['key'])){

    try{
        require_once("../classes/server/DB.php");
        require_once("../classes/server/post.php");
        require_once("../classes/zipCodes/zipCodes.php");
        require_once("../classes/providers/providers.php");

        $sendData = [];
        $sendData['price'] = 0.00;

        // check for missing fields
        if (isset($_REQUEST['key']) && $_REQUEST['key']) {
            $key = $_REQUEST['key'];
        }else{
            throw new Exception('key',102);
        }

        if (isset($_REQUEST['fullName']) && $_REQUEST['fullName']) {
            $sendData['fullName'] = $_REQUEST['fullName'];
            if (!$sendData['fullName'] || !trim($sendData['fullName'])){
                throw new Exception('fullName',103);
            }
        }else{
            throw new Exception('fullName',102);
        }

        if (isset($_REQUEST['phone']) && $_REQUEST['phone']) {
            $sendData['phone'] = $_REQUEST['phone'];
            if (strlen($sendData['phone']) != 10 || !preg_match('/^[0-9]{10}+$/', $sendData['phone'])){
                var_dump(strlen($sendData['phone']));
                var_dump(preg_match('/^[0-9]{10}+$/', $sendData['phone']));
                throw new Exception('',105);
            }
        }else{
            throw new Exception('phone',102);
        }

        if (isset($_REQUEST['email']) && $_REQUEST['email']) {
            $sendData['email'] = $_REQUEST['email'];
            if (!$sendData['email'] || !trim($sendData['email'])){
                throw new Exception('email',103);
            }
            if (!filter_var($sendData['email'], FILTER_VALIDATE_EMAIL)){
                throw new Exception('email',103);
            }
        }else{
            throw new Exception('email',102);
        }

        if (isset($_REQUEST['fromCountry']) && $_REQUEST['fromCountry']) {
            $sendData['fromCountry'] = $_REQUEST['fromCountry'];
            if ($sendData['fromCountry'] != "US" && $sendData['fromCountry'] != "USA"){
                throw new Exception('fromCountry',103);
            }
        }else{
            throw new Exception('fromCountry',102);
        }

        if (isset($_REQUEST['fromZipCode']) && $_REQUEST['fromZipCode']) {
            $sendData['fromZipCode'] = $_REQUEST['fromZipCode'];
            if (strlen($sendData['fromZipCode']) != 5){
                throw new Exception('fromZipCode',103);
            }
        }else{
            throw new Exception('fromZipCode',102);
        }

        if (isset($_REQUEST['fromState']) && $_REQUEST['fromState']) {
            $sendData['fromState'] = $_REQUEST['fromState'];
            if (strlen($sendData['fromState']) != 2){
                throw new Exception('fromState',103);
            }
        }else{
            throw new Exception('fromState',102);
        }

        if (isset($_REQUEST['fromCity']) && $_REQUEST['fromCity']) {
            $sendData['fromCity'] = $_REQUEST['fromCity'];
        }else{
            throw new Exception('fromCity',102);
        }

        if (isset($_REQUEST['toCountry']) && $_REQUEST['toCountry']) {
            $sendData['toCountry'] = $_REQUEST['toCountry'];
            if ($sendData['toCountry'] != "US" && $sendData['toCountry'] != "USA"){
                throw new Exception('toCountry',103);
            }
        }else{
            throw new Exception('toCountry',102);
        }

        if (isset($_REQUEST['toZipCode']) && $_REQUEST['toZipCode']) {
            $sendData['toZipCode'] = $_REQUEST['toZipCode'];
            if (strlen($sendData['toZipCode']) != 5){
                throw new Exception('toZipCode',103);
            }
        }else{
            throw new Exception('toZipCode',102);
        }

        if (isset($_REQUEST['toState']) && $_REQUEST['toState']) {
            $sendData['toState'] = $_REQUEST['toState'];
            if (strlen($sendData['toState']) != 2){
                throw new Exception('toState',103);
            }
        }else{
            throw new Exception('toState',102);
        }

        if (isset($_REQUEST['toCity']) && $_REQUEST['toCity']) {
            $sendData['toCity'] = $_REQUEST['toCity'];
        }else{
            throw new Exception('toCity',102);
        }

        if (isset($_REQUEST['moveSize']) && $_REQUEST['moveSize']) {
            $sendData['moveSize'] = $_REQUEST['moveSize'];
        }else{
            throw new Exception('moveSize',102);
        }

        if (isset($_REQUEST['moveDate']) && $_REQUEST['moveDate']) {
            $sendData['moveDate'] = $_REQUEST['moveDate'];
            $sendData['moveDate'] = date("Y-m-d H:i:s",strtotime($sendData['moveDate']));
        }else{
            throw new Exception('moveDate',102);
        }

        if (isset($_REQUEST['ip']) && $_REQUEST['ip']) {
            $sendData['ip'] = $_REQUEST['ip'];
        }else{
            $sendData['ip'] = 0;
        }

        if (isset($_REQUEST['ref']) && $_REQUEST['ref']) {
            $sendData['ref'] = $_REQUEST['ref'];
        }else{
            $sendData['ref'] = 0;
        }

        if (isset($_REQUEST['userAgent']) && $_REQUEST['userAgent']) {
            $sendData['userAgent'] = $_REQUEST['userAgent'];
        }else{
            $sendData['userAgent'] = 0;
        }

        // check for provider
        $providers = new providers();
        $provider = $providers->getProviderDataByKey($key);

        if ($provider['status'] == true){
            $sendData['providerId'] = $provider['provider']['id'];
        }else{
            throw new Exception("",80);
        }
        // check for zip code
        $sendData['price'] = 10;

        $post = new post();
        $post->addPost($sendData,1);

        echo '{"status":true,"code":100,"price":'.number_format($sendData['price'],2).',"response":"Test post received successfully"}';
    }catch (Exception $e){
        if ($e->getCode() == 101){
            // server error - cant get zipcodes list
            echo '{"status":false,"code":101,"price":0.00,"response":"Server error, please try again later"}';
        }else if ($e->getCode() == 90){
            // zip code unnecessary
            echo '{"status":true,"code":100,"price":0.00,"response":"Test post received successfully"}';
        }else if ($e->getCode() == 80){
            // unknown provider
            echo '{"status":false,"code":80,"price":0.00,"response":"Unknown key, please contact support"}';
        }else if ($e->getCode() == 102){
            // Missing Fields
            echo '{"status":false,"code":102,"price":0.00,"response":"Missing fields: '.$e->getMessage().' : '.$_REQUEST[$e->getMessage()].'"}';
        }else if ($e->getCode() == 103){
            // Wrong fields format
            echo '{"status":false,"code":103,"price":0.00,"response":"Wrong field format: '.$e->getMessage().' : '.$_REQUEST[$e->getMessage()].'"}';
        }else if ($e->getCode() == 105){
            // Phone number error
            echo '{"status":false,"code":105,"price":0.00,"response":"Input phone should be 10 characters exactly"';
        }
    }
    die;
}else{

}
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Post</title>
</head>
<body>
<main>
    <div class="container-fluid">
        <div class="row" style="padding: 0">
            <div class="col-4" style="padding: 0;margin: 25px auto;text-align: center">
                <form class="form form-horizontal" action="https://app.leadrift.com/myApi/pingpost/test/post.php" method="POST" target="_blank">
                    <div class="form-group">
                        <label class="control-label col-md-12" for="key">Provider Key:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="key" name="key" placeholder="Enter your api key here">
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="fullName">fullName</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="fullName" name="fullName" value="Test Test">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="phone">phone</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="phone" name="phone" value="8000000000">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="email">email</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="email" name="email" value="test@test.com">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="ip">ip</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="ip" name="ip" value="10.0.0.1">
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="fromCountry">fromCountry:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="fromCountry" name="fromCountry" value="US">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="fromZipCode">fromZipCode:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="fromZipCode" name="fromZipCode" value="10001">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="fromState">fromState:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="fromState" name="fromState" value="NY">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="fromCity">fromCity:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="fromCity" name="fromCity" value="New York City">
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="toCountry">toCountry:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="toCountry" name="toCountry" value="US">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="toZipCode">toZipCode:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="toZipCode" name="toZipCode" value="02143">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="toState">toState:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="toState" name="toState" value="MA">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12" for="toCity">to_place:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="toCity" name="toCity" value="Somerville">
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="col-md-12 control-label" for="moveSize">moveSize:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="moveSize" name="moveSize" value="1">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12 control-label" for="moveDate">moveDate:</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="moveDate" name="moveDate" value="2016-08-22">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-4">
                            <button class="btn btn-primary btn-block" type="submit">
                                <strong>Post!</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
</body>
</html>