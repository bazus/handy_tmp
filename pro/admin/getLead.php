<?php
header("Access-Control-Allow-Origin: *");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (isset($_POST['fullName'])){
    $fullName = $_POST['fullName'];
}else{
    $fullName = "Empty";
}
if (isset($_POST['ref'])){
    $ref = $_POST['ref'];
}else{
    $ref = "Empty";
}
if (isset($_POST['email'])){
    $email = $_POST['email'];
}else{
    $email = "Empty";
}
if (isset($_POST['phone'])){
    $phone = $_POST['phone'];
}else{
    $phone = "Empty";
}
if (isset($_POST['fromZip'])){
    $fromZip = $_POST['fromZip'];
}else{
    $fromZip = "Empty";
}
if (isset($_POST['toZip'])){
    $toZip = $_POST['toZip'];
}else{
    $toZip = "Empty";
}
if (isset($_POST['moveSize'])){
    $moveSize = $_POST['moveSize'];
}else{
    $moveSize = "Empty";
}
if (isset($_POST['moveDate'])){
    $moveDate = $_POST['moveDate'];
}else{
    $moveDate = "Empty";
}
if (isset($_POST['fromState'])) {
    $fromState = $_POST['fromState'];
}else{
    $fromState = "Empty";
}
if (isset($_POST['toState'])) {
    $toState = $_POST['toState'];
}else{
    $toState = "Empty";
}
$emailTo = "maorjosephnotify@gmail.com";
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$message = '<html><body>';
$message .= '<h2 style="font-size: 18px;">New Lead From loyalmover.com form</h2>';
$message .= '<p style="font-size:16px;">'."Full Name: ".$fullName." <br>Email: ".$email." <br>Phone: ".$phone." <br><br>From ZIP: ".$fromZip." <br>To ZIP: ".$toZip." <br><br>Move Size: ".$moveSize." <br>Move Date: ".$moveDate." <br><br>Referral: ".$ref.'</p>';
$message .= '<a href=\'https://app.leadrift.com/admin/system/index.php\'>Go see lead in CRM</a>';
$message .= '</body></html>';

mail($emailTo,"New Lead - Loyal Mover Form",$message,$headers);
$emailTo = "leads@leadrift.com";
mail($emailTo,"New Lead - Loyal Mover Form",$message,$headers);
$data = [];
$data['fullName'] = $fullName;
$data['phone'] = $phone;
$data['email'] = $email;
$data['fromCountry'] = "US";
$data['fromZipCode'] = $fromZip;
$data['fromState'] = $fromState;
$data['toCountry'] = "US";
$data['toZipCode'] = $toZip;
$data['toState'] = $toState;
$data['moveSize'] = $moveSize;
$data['moveDate'] = $moveDate;
$data['providerId'] = 1;

require_once("classes/server/DB.php");
require_once("classes/leads/leads.php");

$res = [];
try{
    $leads = new leads();
    $res = $leads->addLead($data);

    echo json_encode($res);
}catch (Exception $e){
    echo json_encode($e);
}