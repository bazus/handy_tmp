<?php


class post extends DB
{

    function __construct()
    {
        parent::__construct();
    }

    public function getPosts($showTest = false){

        $res = [];
        $res['status'] = false;
        $res['posts'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                if ($showTest == "false") {
                    $stmt = $this->conn->prepare("SELECT p.*,pro.providerName FROM joseianq_myApi.post AS p LEFT JOIN joseianq_myApi.providers AS pro ON pro.id=p.providerId WHERE p.isTest=0 ORDER BY p.id DESC LIMIT 100");
                } else {
                    $stmt = $this->conn->prepare("SELECT p.*,pro.providerName FROM joseianq_myApi.post AS p LEFT JOIN joseianq_myApi.providers AS pro ON pro.id=p.providerId WHERE p.isTest=1 ORDER BY p.id DESC LIMIT 100");
                }
            }else{
                if ($showTest == "false") {
                    $stmt = $this->conn->prepare("SELECT p.*,pro.providerName FROM myApi.post AS p LEFT JOIN myApi.providers AS pro ON pro.id=p.providerId WHERE p.isTest=0 ORDER BY p.id DESC LIMIT 100");
                } else {
                    $stmt = $this->conn->prepare("SELECT p.*,pro.providerName FROM myApi.post AS p LEFT JOIN myApi.providers AS pro ON pro.id=p.providerId WHERE p.isTest=1 ORDER BY p.id DESC LIMIT 100");
                }
            }


            $stmt->execute();
            $stmtData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmtData) {
                foreach($stmtData AS $data) {
                    $res['posts'][] = $data;
                }
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['posts'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['posts'] = [];
        }

        return $res;
    }
}