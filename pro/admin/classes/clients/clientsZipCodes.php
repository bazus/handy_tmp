<?php

class clientsZipCodes extends DB
{

    public $clientId = 0;
    function __construct($clientId)
    {
        parent::__construct();
        $this->clientId = $clientId;
    }

    public function getData(){

        $res = [];
        $res['status'] = false;
        $res['zipCodes'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT * FROM joseianq_myApi.clientsZipCodes WHERE isDeleted=0 AND clientId=:clientId ORDER BY id DESC");
            }else{
                $stmt = $this->conn->prepare("SELECT * FROM myApi.clientsZipCodes WHERE isDeleted=0 AND clientId=:clientId ORDER BY id DESC");
            }

            $stmt->bindParam(':clientId', $this->clientId, PDO::PARAM_INT);

            $stmt->execute();
            $stmtData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmtData) {
                foreach ($stmtData as &$stmtDatum){
                    if (!isset($stmtDatum['zipCode'])){
                        $stmtDatum['zipCode'] = "No Zip Codes";
                    }
                }
                $res['zipCodes'] = $stmtData;
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['zipCodes'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['zipCodes'] = $e;
        }

        return $res;
    }
    public function addData($zipCode){

        $res = [];
        $res['status'] = false;
        $res['success'] = false;

        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("INSERT INTO joseianq_myApi.clientsZipCodes(clientId,zipCode) VALUES(:clientId,:zipCode)");
            }else{
                $stmt = $this->conn->prepare("INSERT INTO myApi.clientsZipCodes(clientId,zipCode) VALUES(:clientId,:zipCode)");
            }

            $stmt->bindParam(':clientId', $this->clientId, PDO::PARAM_INT);
            $stmt->bindParam(':zipCode', $zipCode, PDO::PARAM_INT);

            $stmt->execute();

            $res['success'] = true;
            $res['status'] = true;

        }catch (PDOException $e){
            $res['status'] = false;
            $res['success'] = $e;
        }

        return $res;
    }

}