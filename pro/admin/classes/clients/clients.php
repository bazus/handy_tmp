<?php
class clients extends DB
{

    function __construct()
    {
        parent::__construct();
    }

    public function getData(){

        $res = [];
        $res['status'] = false;
        $res['clients'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT c.*,czp.zipCode FROM joseianq_myApi.clients AS c LEFT JOIN joseianq_myApi.clientsZipCodes AS czp ON czp.clientId=c.id WHERE c.isDeleted=0 ORDER BY c.id DESC");
            }else{
                $stmt = $this->conn->prepare("SELECT c.*,czp.zipCode FROM myApi.clients AS c LEFT JOIN myApi.clientsZipCodes AS czp ON czp.clientId=c.id WHERE c.isDeleted=0 ORDER BY c.id DESC");
            }

            $stmt->execute();
            $stmtData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmtData) {
                $res['clients'] = $stmtData;
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['clients'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['clients'] = $e;
        }

        return $res;
    }
    public function getClient($id){

        $res = [];
        $res['status'] = false;
        $res['client'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT c.*,czp.zipCode FROM joseianq_myApi.clients AS c LEFT JOIN joseianq_myApi.clientsZipCodes AS czp ON czp.clientId=c.id WHERE c.isDeleted=0 AND c.id=:id ORDER BY c.id DESC");
            }else{
                $stmt = $this->conn->prepare("SELECT c.*,czp.zipCode FROM myApi.clients AS c LEFT JOIN myApi.clientsZipCodes AS czp ON czp.clientId=c.id WHERE c.isDeleted=0 AND c.id=:id ORDER BY c.id DESC");
            }

            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            $stmt->execute();
            $stmtData = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmtData) {
                $res['client'] = $stmtData;
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['client'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['client'] = $e;
        }

        return $res;
    }
    public function addData($data){

        $res = [];
        $res['status'] = false;
        $res['success'] = false;

        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("INSERT INTO joseianq_myApi.clients(name,phone,email,balance,leadPrice,postMethods,postAddress) VALUES(:name,:phone,:email,:balance,:leadPrice,:postMethods,:postAddress)");
            }else{
                $stmt = $this->conn->prepare("INSERT INTO myApi.clients(name,phone,email,balance,leadPrice,postMethods,postAddress) VALUES(:name,:phone,:email,:balance,:leadPrice,:postMethods,:postAddress)");
            }

            $stmt->bindParam(':name', $data['name'], PDO::PARAM_STR);
            $stmt->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
            $stmt->bindParam(':email', $data['email'], PDO::PARAM_STR);
            $stmt->bindParam(':balance', $data['balance'], PDO::PARAM_STR);
            $stmt->bindParam(':leadPrice', $data['leadPrice'], PDO::PARAM_STR);
            $stmt->bindParam(':postMethods', $data['postMethods'], PDO::PARAM_STR);
            $stmt->bindParam(':postAddress', $data['postAddress'], PDO::PARAM_STR);

            $stmt->execute();

            $res['success'] = true;
            $res['status'] = true;

        }catch (PDOException $e){
            $res['status'] = false;
            $res['success'] = $e;
        }

        return $res;
    }

}