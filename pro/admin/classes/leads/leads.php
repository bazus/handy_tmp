<?php

class leads extends DB
{
    function __construct()
    {
        parent::__construct();
    }

    public function getData(){

        $res = [];
        $res['status'] = false;
        $res['leads'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT l.*,p.providerName FROM joseianq_myApi.leads AS l LEFT JOIN joseianq_myApi.providers AS p ON p.id=l.providerId ORDER BY l.id DESC");
            }else{
                $stmt = $this->conn->prepare("SELECT l.*,p.providerName FROM myApi.leads AS l LEFT JOIN myApi.providers AS p ON p.id=l.providerId ORDER BY l.id DESC");
            }

            $stmt->execute();
            $stmtData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmtData) {
                $res['leads'] = $stmtData;
                $res['status'] = true;
            }else{
                $res['status'] = true;
                $res['status'] = true;
                $res['leads'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['leads'] = [];
        }

        return $res;
    }
    public function getDataById($leadId){

        $res = [];
        $res['status'] = false;
        $res['leads'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT l.*,p.providerName FROM joseianq_myApi.leads AS l LEFT JOIN joseianq_myApi.providers AS p ON p.id=l.providerId WHERE l.id=:leadId ORDER BY l.id DESC");
            }else{
                $stmt = $this->conn->prepare("SELECT l.*,p.providerName FROM myApi.leads AS l LEFT JOIN myApi.providers AS p ON p.id=l.providerId WHERE l.id=:leadId ORDER BY l.id DESC");
            }
            $stmt->bindParam(':leadId', $leadId, PDO::PARAM_INT);
            $stmt->execute();
            $stmtData = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmtData) {
                $res['leads'] = $stmtData;
                $res['status'] = true;
            }else{
                $res['status'] = true;
                $res['status'] = true;
                $res['leads'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['leads'] = [];
        }

        return $res;
    }

    public function getSentData(){

        $res = [];
        $res['status'] = false;
        $res['leads'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT * FROM joseianq_myApi.sentLeads ORDER BY id DESC");
            }else{
                $stmt = $this->conn->prepare("SELECT * FROM myApi.sentLeads ORDER BY id DESC");
            }

            $stmt->execute();
            $stmtData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmtData) {
                foreach ($stmtData as $lead){
                    if ($lead['data']) {
                        $lead['data'] = unserialize($lead['data']);
                    }
                    $lead['data'] = explode("&",$lead['data']);
                    if ($lead['status']) {
                        $lead['status'] = unserialize($lead['status']);
                    }
                    if (isset($lead['data'][4]) && isset($lead['data'][5])){
                        $firstName = explode("=",$lead['data'][4]);
                        $lastName = explode("=",$lead['data'][5]);
                        $lead['fullName'] = $firstName[1]." ".$lastName[1];
                    }else{
                        $lead['fullName'] = "No Data";
                    }
                    if (isset($lead['data'][16])){
                        $phone = explode("=",$lead['data'][16]);
                        $lead['phone'] = $phone[1];
                    }else{
                        $lead['phone'] = "No Data";
                    }
                    if (isset($lead['data'][15])){
                        $email = explode("=",$lead['data'][15]);
                        $lead['email'] = $email[1];
                    }else{
                        $lead['email'] = "No Data";
                    }
                    if (isset($lead['data'][7])){
                        $fromZip = explode("=",$lead['data'][7]);
                        $lead['fromData'] = "ZIP: ".$fromZip[1];
                        if (isset($lead['data'][6])){
                            $fromState = explode("=",$lead['data'][6]);
                            $lead['fromData'] .= " State: ".$fromState[1];
                        }
                    }else{
                        $lead['fromData'] = "No Data";
                    }
                    if (isset($lead['data'][10])){
                        $toZip = explode("=",$lead['data'][10]);
                        $lead['toData'] = "ZIP: ".$toZip[1];
                        if (isset($lead['data'][9])){
                            $toState = explode("=",$lead['data'][9]);
                            $lead['toData'] .= " State: ".$toState[1];
                        }
                    }else{
                        $lead['toData'] = "No Data";
                    }
                    $res['leads'][] = $lead;
                }
                $res['status'] = true;
            }else{
                $res['status'] = true;
                $res['leads'] = "Error";
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['leads'] = [];
        }

        return $res;
    }

    public function addLead($data){

        $res = [];
        $res['status'] = false;
        $res['success'] = false;

        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("INSERT INTO joseianq_myApi.leads(fullName,phone1,email,fromCountry,fromZipCode,fromState,toCountry,toZipCode,toState,moveSize,movingDate,providerId) VALUES(:fullName,:phone,:email,:fromCountry,:fromZipCode,:fromState,:toCountry,:toZipCode,:toState,:moveSize,:moveDate,:providerId)");
            }else{
                $stmt = $this->conn->prepare("INSERT INTO myApi.leads(fullName,phone1,email,fromCountry,fromZipCode,fromState,toCountry,toZipCode,toState,moveSize,movingDate,providerId) VALUES(:fullName,:phone,:email,:fromCountry,:fromZipCode,:fromState,:toCountry,:toZipCode,:toState,:moveSize,:moveDate,:providerId)");
            }

            $stmt->bindParam(':fullName', $data['fullName'], PDO::PARAM_STR);
            $stmt->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
            $stmt->bindParam(':email', $data['email'], PDO::PARAM_STR);
            $stmt->bindParam(':fromCountry', $data['fromCountry'], PDO::PARAM_STR);
            $stmt->bindParam(':fromZipCode', $data['fromZipCode'], PDO::PARAM_STR);
            $stmt->bindParam(':fromState', $data['fromState'], PDO::PARAM_STR);
            $stmt->bindParam(':toCountry', $data['toCountry'], PDO::PARAM_STR);
            $stmt->bindParam(':toZipCode', $data['toZipCode'], PDO::PARAM_STR);
            $stmt->bindParam(':toState', $data['toState'], PDO::PARAM_STR);
            $stmt->bindParam(':moveSize', $data['moveSize'], PDO::PARAM_STR);
            $stmt->bindParam(':moveDate', $data['moveDate'], PDO::PARAM_STR);
            $stmt->bindParam(':providerId', $data['providerId'], PDO::PARAM_STR);

            $stmt->execute();

            $res['success'] = true;
            $res['status'] = true;

        }catch (PDOException $e){
            $res['status'] = false;
            $res['success'] = $e;
        }

        return $res;
    }

}