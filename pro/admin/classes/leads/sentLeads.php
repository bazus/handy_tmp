<?php


class sentLeads extends DB
{
    function __construct()
    {
        parent::__construct();
    }
    function addData($data,$to,$status){

        $res = [];
        $res['status'] = false;
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("INSERT INTO joseianq_myApi.sentLeads(data,sentTo,status) VALUES(:data,:to,:status)");
            }else{
                $stmt = $this->conn->prepare("INSERT INTO myApi.sentLeads(data,sentTo,status) VALUES(:data,:to,:status)");
            }
            $data = serialize($data);
            $status = serialize($status);
            $stmt->bindParam(':data',$data , PDO::PARAM_STR);
            $stmt->bindParam(':to', $to, PDO::PARAM_STR);
            $stmt->bindParam(':status',$status , PDO::PARAM_STR);

            $stmt->execute();

            $stmtData = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmtData) {
                $res['status'] = true;
            }else{
                $res['status'] = false;
            }

        }catch (PDOException $e){
            $res['status'] = false;
        }

        return $res;
    }
    function updateLead($leadId){
        $res = [];
        $res['status'] = false;
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("UPDATE joseianq_myApi.leads SET isSeen=1 WHERE id=:leadId");
            }else{
                $stmt = $this->conn->prepare("UPDATE myApi.sentLeads SET isSeen=1 WHERE id=:leadId");
            }
            $stmt->bindParam(':leadId',$leadId , PDO::PARAM_INT);

            $stmt->execute();

            $stmtData = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmtData) {
                $res['status'] = true;
            }else{
                $res['status'] = false;
            }

        }catch (PDOException $e){
            $res['status'] = false;
        }

        return $res;
    }
}