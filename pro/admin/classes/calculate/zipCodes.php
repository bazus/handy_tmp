<?php


class zipCodes extends DB
{

    function __construct()
    {
        parent::__construct();
    }

    function getZipCodes($originalZip,$originalRadius){

        $res = [];
        $res['status'] = false;
        $res['zipCodes'] = [];
        try {
            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare("SELECT ZipCode,Latitude,Longitude FROM joseianq_myApi.ZIPCodes WHERE ZipCode=:orgZip");
            }else{
                $stmt = $this->conn->prepare("SELECT ZipCode,Latitude,Longitude FROM myApi.ZIPCodes WHERE ZipCode=:orgZip");
            }

            $stmt->bindParam(':orgZip', $originalZip, PDO::PARAM_INT);

            $stmt->execute();

            $location = $stmt->fetch(PDO::FETCH_ASSOC);

            $lat = $location["Latitude"];
            $lon = $location["Longitude"];

            if ($_SERVER['HTTP_HOST'] != "localhost") {
                $stmt = $this->conn->prepare('SELECT distinct(ZipCode) FROM joseianq_myApi.ZIPCodes WHERE (3958*3.1415926*sqrt((Latitude-' . $lat . ')*(Latitude-' . $lat . ') + cos(Latitude/57.29578)*cos(' . $lat . '/57.29578)*(Longitude-' . $lon . ')*(Longitude-' . $lon . '))/180) <= ' . $originalRadius . ';');
            } else {
                $stmt = $this->conn->prepare('SELECT distinct(ZipCode) FROM myApi.ZIPCodes WHERE (3958*3.1415926*sqrt((Latitude-' . $lat . ')*(Latitude-' . $lat . ') + cos(Latitude/57.29578)*cos(' . $lat . '/57.29578)*(Longitude-' . $lon . ')*(Longitude-' . $lon . '))/180) <= ' . $originalRadius);
            }

            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            {
                $res['zipCodes'][] = $row['ZipCode'];
            }
            $res['status'] = true;

        }catch (PDOException $e){
            $res['status'] = false;
            $res['zipCodes'] = $e;
        }

        return $res;
    }
}