<?php
require_once("DB.php");

class register extends DB
{
    function __construct()
    {
        parent::__construct();
    }

    public function register($fullName,$email,$password,$isGoogle = false){

        $res = [];
        $res['status'] = false;
        $res['userExists'] = false;
        $res['registerSuccess'] = false;

        if (isset($_SESSION['userId'])){
            $res['status'] = false;
            $res['userExists'] = false;
            $res['registerSuccess'] = true;
            return $res;
        }

        if ($this->emailExists($email)){
            $res['status'] = false;
            $res['userExists'] = true;
            $res['registerSuccess'] = false;
            return $res;
        }

        try {
            if ($isGoogle){
                $stmt = $this->conn->prepare("INSERT INTO joseianq_crm.users (fullName,email,registerWithGoogle) VALUES(:fullName,:email,:google)");
            }else {
                $stmt = $this->conn->prepare("INSERT INTO joseianq_crm.users (fullName,email,password) VALUES(:fullName,:email,:password)");
            }

            $stmt->bindParam(':fullName', $fullName, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            if ($isGoogle) {
                $stmt->bindParam(':google', $password, PDO::PARAM_STR);
            }else{
                $password = password_hash($password,PASSWORD_BCRYPT);
                $stmt->bindParam(':password', $password, PDO::PARAM_STR);
            }

            $stmt->execute();

            $_SESSION['userId'] = $this->conn->lastInsertId();
            $_SESSION['fullName'] = $fullName;

            $res['status'] = true;
            $res['userExists'] = false;
            $res['registerSuccess'] = true;

        }catch (PDOException $e){
            $res['status'] = false;
            $res['userExists'] = false;
            $res['registerSuccess'] = false;
        }

        return $res;
    }

    private function emailExists($email){
        try {
            $stmt = $this->conn->prepare("SELECT id FROM joseianq_crm.users WHERE email=:email");

            $stmt->bindParam(':email', $email, PDO::PARAM_STR);

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row && $row['id']) {
                return true;
            } else {
                return false;
            }
        }catch (PDOException $e){
            return false;
        }
    }

}