<?php
require_once("DB.php");

class login extends DB
{
    function __construct()
    {
        parent::__construct();
    }

    public function login($email,$password,$isGoogle = false){

        $res = [];
        $res['status'] = false;
        $res['userLoggedIn'] = false;

        if (isset($_SESSION['userId'])){
            $res['status'] = false;
            $res['userLoggedIn'] = true;
            return $res;
        }
        try {
            if ($isGoogle) {
                $stmt = $this->conn->prepare("SELECT id,fullName,password FROM joseianq_crm.users WHERE email=:email AND registerWithGoogle=:google LIMIT 1");
            }else{
                $stmt = $this->conn->prepare("SELECT id,fullName,password FROM joseianq_crm.users WHERE email=:email LIMIT 1");
            }
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            if ($isGoogle) {
                $stmt->bindParam(':google', $password, PDO::PARAM_STR);
            }


            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row) {
                if ($isGoogle) {
                    if ($row['id'] && $row['fullName']) {
                        $_SESSION['userId'] = $row['id'];
                        $_SESSION['fullName'] = $row['fullName'];
                        $res['status'] = true;
                        $res['userLoggedIn'] = true;
                    }
                }else{
                    if (password_verify($password, $row['password'])) {
                        if ($row['id'] && $row['fullName']) {
                            $_SESSION['userId'] = $row['id'];
                            $_SESSION['fullName'] = $row['fullName'];
                            $res['status'] = true;
                            $res['userLoggedIn'] = true;
                        } else {
                            $res['status'] = false;
                            $res['userLoggedIn'] = false;
                        }
                    } else {
                        $res['status'] = false;
                        $res['userLoggedIn'] = false;
                    }
                }
            }else{
                $res['status'] = false;
                $res['userLoggedIn'] = false;
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['userLoggedIn'] = false;
        }

        return $res;
    }
}