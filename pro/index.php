<?php
session_start();
if (isset($_GET['clearSession'])){
    session_destroy();
}
$siteUrl = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/";
?>
<html>
    <head>
        <title>Lead Rift</title>
        <link rel="shortcut icon" href="<?= $siteUrl ?>assets/images/dsasdds.png">
        <!-- ============== Jquery & UI ============== -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <!-- ============== Bootstrap ============== -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script><script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

        <!-- ============== Font Awesome ============== -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">

        <!-- ============== SweetAlert ============== -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <!-- ============== Alert Toast ============== -->
        <link rel="stylesheet" href="plugins/Alert-Toast-MK-Web-Notifications/src/css/mk-notifications.min.css">
        <script src="plugins/Alert-Toast-MK-Web-Notifications/src/js/mk-notifications.min.js"></script>

        <!-- ============== Custom Files ============== -->
        <link href="<?= $siteUrl; ?>main.css" rel="stylesheet"></head>
        <link rel="stylesheet" href="<?= $siteUrl; ?>css/style.css">
        <script>
            var BASE_URL = '<?= $siteUrl ?>';
        </script>
        <!-- Start of HubSpot Embed Code -->
        <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5551127.js"></script>
        <!-- End of HubSpot Embed Code -->

        <meta name="google-signin-client_id" content="357226719040-hd75f4ltg8cakhmgcfmaqo7qsi9cv9dn.apps.googleusercontent.com">

    </head>
    <body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow bg-dark">
            <div class="app-header__logo">
                <img src="<?= $siteUrl ?>assets/images/logo.png" alt="LeadRift" height="58">
            </div>
            <div class="app-header__content">
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-right header-user-info ml-3">
                                    <button onclick="openSignup()" type="button" class="btn-shadow p-1 btn btn-primary btn-sm">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--        <div class="ui-theme-settings">-->
<!--            <button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning">-->
<!--                <i class="fa fa-cog fa-w-16 fa-spin fa-2x"></i>-->
<!--            </button>-->
<!--            <div class="theme-settings__inner">-->
<!--                <div class="scrollbar-container">-->
<!--                    <div class="theme-settings__options-wrapper">-->
<!--                        <h3 class="themeoptions-heading">Layout Options-->
<!--                        </h3>-->
<!--                        <div class="p-3">-->
<!--                            <ul class="list-group">-->
<!--                                <li class="list-group-item">-->
<!--                                    <div class="widget-content p-0">-->
<!--                                        <div class="widget-content-wrapper">-->
<!--                                            <div class="widget-content-left mr-3">-->
<!--                                                <div class="switch has-switch switch-container-class" data-class="fixed-header">-->
<!--                                                    <div class="switch-animate switch-on">-->
<!--                                                        <input type="checkbox" checked data-toggle="toggle" data-onstyle="success">-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class="widget-content-left">-->
<!--                                                <div class="widget-heading">Fixed Header-->
<!--                                                </div>-->
<!--                                                <div class="widget-subheading">Makes the header top fixed, always visible!-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </li>-->
<!--                                <li class="list-group-item">-->
<!--                                    <div class="widget-content p-0">-->
<!--                                        <div class="widget-content-wrapper">-->
<!--                                            <div class="widget-content-left mr-3">-->
<!--                                                <div class="switch has-switch switch-container-class" data-class="fixed-sidebar">-->
<!--                                                    <div class="switch-animate switch-on">-->
<!--                                                        <input type="checkbox" checked data-toggle="toggle" data-onstyle="success">-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class="widget-content-left">-->
<!--                                                <div class="widget-heading">Fixed Sidebar-->
<!--                                                </div>-->
<!--                                                <div class="widget-subheading">Makes the sidebar left fixed, always visible!-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </li>-->
<!--                                <li class="list-group-item">-->
<!--                                    <div class="widget-content p-0">-->
<!--                                        <div class="widget-content-wrapper">-->
<!--                                            <div class="widget-content-left mr-3">-->
<!--                                                <div class="switch has-switch switch-container-class" data-class="fixed-footer">-->
<!--                                                    <div class="switch-animate switch-off">-->
<!--                                                        <input type="checkbox" data-toggle="toggle" data-onstyle="success">-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class="widget-content-left">-->
<!--                                                <div class="widget-heading">Fixed Footer-->
<!--                                                </div>-->
<!--                                                <div class="widget-subheading">Makes the app footer bottom fixed, always visible!-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                        <h3 class="themeoptions-heading">-->
<!--                            <div>-->
<!--                                Header Options-->
<!--                            </div>-->
<!--                            <button type="button" class="btn-pill btn-shadow btn-wide ml-auto btn btn-focus btn-sm switch-header-cs-class" data-class="">-->
<!--                                Restore Default-->
<!--                            </button>-->
<!--                        </h3>-->
<!--                        <div class="p-3">-->
<!--                            <ul class="list-group">-->
<!--                                <li class="list-group-item">-->
<!--                                    <h5 class="pb-2">Choose Color Scheme-->
<!--                                    </h5>-->
<!--                                    <div class="theme-settings-swatches">-->
<!--                                        <div class="swatch-holder bg-primary switch-header-cs-class" data-class="bg-primary header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-secondary switch-header-cs-class" data-class="bg-secondary header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-success switch-header-cs-class" data-class="bg-success header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-info switch-header-cs-class" data-class="bg-info header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-warning switch-header-cs-class" data-class="bg-warning header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-danger switch-header-cs-class" data-class="bg-danger header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-light switch-header-cs-class" data-class="bg-light header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-dark switch-header-cs-class" data-class="bg-dark header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-focus switch-header-cs-class" data-class="bg-focus header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-alternate switch-header-cs-class" data-class="bg-alternate header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="divider">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-vicious-stance switch-header-cs-class" data-class="bg-vicious-stance header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-midnight-bloom switch-header-cs-class" data-class="bg-midnight-bloom header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-night-sky switch-header-cs-class" data-class="bg-night-sky header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-slick-carbon switch-header-cs-class" data-class="bg-slick-carbon header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-asteroid switch-header-cs-class" data-class="bg-asteroid header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-royal switch-header-cs-class" data-class="bg-royal header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-warm-flame switch-header-cs-class" data-class="bg-warm-flame header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-night-fade switch-header-cs-class" data-class="bg-night-fade header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-sunny-morning switch-header-cs-class" data-class="bg-sunny-morning header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-tempting-azure switch-header-cs-class" data-class="bg-tempting-azure header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-amy-crisp switch-header-cs-class" data-class="bg-amy-crisp header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-heavy-rain switch-header-cs-class" data-class="bg-heavy-rain header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-mean-fruit switch-header-cs-class" data-class="bg-mean-fruit header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-malibu-beach switch-header-cs-class" data-class="bg-malibu-beach header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-deep-blue switch-header-cs-class" data-class="bg-deep-blue header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-ripe-malin switch-header-cs-class" data-class="bg-ripe-malin header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-arielle-smile switch-header-cs-class" data-class="bg-arielle-smile header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-plum-plate switch-header-cs-class" data-class="bg-plum-plate header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-happy-fisher switch-header-cs-class" data-class="bg-happy-fisher header-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-happy-itmeo switch-header-cs-class" data-class="bg-happy-itmeo header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-mixed-hopes switch-header-cs-class" data-class="bg-mixed-hopes header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-strong-bliss switch-header-cs-class" data-class="bg-strong-bliss header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-grow-early switch-header-cs-class" data-class="bg-grow-early header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-love-kiss switch-header-cs-class" data-class="bg-love-kiss header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-premium-dark switch-header-cs-class" data-class="bg-premium-dark header-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-happy-green switch-header-cs-class" data-class="bg-happy-green header-text-light">-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                        <h3 class="themeoptions-heading">-->
<!--                            <div>Sidebar Options</div>-->
<!--                            <button type="button" class="btn-pill btn-shadow btn-wide ml-auto btn btn-focus btn-sm switch-sidebar-cs-class" data-class="">-->
<!--                                Restore Default-->
<!--                            </button>-->
<!--                        </h3>-->
<!--                        <div class="p-3">-->
<!--                            <ul class="list-group">-->
<!--                                <li class="list-group-item">-->
<!--                                    <h5 class="pb-2">Choose Color Scheme-->
<!--                                    </h5>-->
<!--                                    <div class="theme-settings-swatches">-->
<!--                                        <div class="swatch-holder bg-primary switch-sidebar-cs-class" data-class="bg-primary sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-secondary switch-sidebar-cs-class" data-class="bg-secondary sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-success switch-sidebar-cs-class" data-class="bg-success sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-info switch-sidebar-cs-class" data-class="bg-info sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-warning switch-sidebar-cs-class" data-class="bg-warning sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-danger switch-sidebar-cs-class" data-class="bg-danger sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-light switch-sidebar-cs-class" data-class="bg-light sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-dark switch-sidebar-cs-class" data-class="bg-dark sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-focus switch-sidebar-cs-class" data-class="bg-focus sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-alternate switch-sidebar-cs-class" data-class="bg-alternate sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="divider">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-vicious-stance switch-sidebar-cs-class" data-class="bg-vicious-stance sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-midnight-bloom switch-sidebar-cs-class" data-class="bg-midnight-bloom sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-night-sky switch-sidebar-cs-class" data-class="bg-night-sky sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-slick-carbon switch-sidebar-cs-class" data-class="bg-slick-carbon sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-asteroid switch-sidebar-cs-class" data-class="bg-asteroid sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-royal switch-sidebar-cs-class" data-class="bg-royal sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-warm-flame switch-sidebar-cs-class" data-class="bg-warm-flame sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-night-fade switch-sidebar-cs-class" data-class="bg-night-fade sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-sunny-morning switch-sidebar-cs-class" data-class="bg-sunny-morning sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-tempting-azure switch-sidebar-cs-class" data-class="bg-tempting-azure sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-amy-crisp switch-sidebar-cs-class" data-class="bg-amy-crisp sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-heavy-rain switch-sidebar-cs-class" data-class="bg-heavy-rain sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-mean-fruit switch-sidebar-cs-class" data-class="bg-mean-fruit sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-malibu-beach switch-sidebar-cs-class" data-class="bg-malibu-beach sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-deep-blue switch-sidebar-cs-class" data-class="bg-deep-blue sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-ripe-malin switch-sidebar-cs-class" data-class="bg-ripe-malin sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-arielle-smile switch-sidebar-cs-class" data-class="bg-arielle-smile sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-plum-plate switch-sidebar-cs-class" data-class="bg-plum-plate sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-happy-fisher switch-sidebar-cs-class" data-class="bg-happy-fisher sidebar-text-dark">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-happy-itmeo switch-sidebar-cs-class" data-class="bg-happy-itmeo sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-mixed-hopes switch-sidebar-cs-class" data-class="bg-mixed-hopes sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-strong-bliss switch-sidebar-cs-class" data-class="bg-strong-bliss sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-grow-early switch-sidebar-cs-class" data-class="bg-grow-early sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-love-kiss switch-sidebar-cs-class" data-class="bg-love-kiss sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-premium-dark switch-sidebar-cs-class" data-class="bg-premium-dark sidebar-text-light">-->
<!--                                        </div>-->
<!--                                        <div class="swatch-holder bg-happy-green switch-sidebar-cs-class" data-class="bg-happy-green sidebar-text-light">-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                        <h3 class="themeoptions-heading">-->
<!--                            <div>Main Content Options</div>-->
<!--                            <button type="button" class="btn-pill btn-shadow btn-wide ml-auto active btn btn-focus btn-sm">Restore Default-->
<!--                            </button>-->
<!--                        </h3>-->
<!--                        <div class="p-3">-->
<!--                            <ul class="list-group">-->
<!--                                <li class="list-group-item">-->
<!--                                    <h5 class="pb-2">Page Section Tabs-->
<!--                                    </h5>-->
<!--                                    <div class="theme-settings-swatches">-->
<!--                                        <div role="group" class="mt-2 btn-group">-->
<!--                                            <button type="button" class="btn-wide btn-shadow btn-primary btn btn-secondary switch-theme-class" data-class="body-tabs-line">-->
<!--                                                Line-->
<!--                                            </button>-->
<!--                                            <button type="button" class="btn-wide btn-shadow btn-primary active btn btn-secondary switch-theme-class" data-class="body-tabs-shadow">-->
<!--                                                Shadow-->
<!--                                            </button>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <div class="app-main">
            <div class="app-main__outer" style="padding: 0">
                <div class="app-main__inner">
                    <div id="loginModal" class="modal-dialog w-100 mx-auto" style="display:none;">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="h5 modal-title text-center">
                                    <h4 class="mt-2">
                                        <div>Welcome back,</div>
                                        <span>Please sign in to your account below.</span>
                                    </h4>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="position-relative form-group"><input name="email" id="loginEmail" placeholder="Email" type="email" class="form-control"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="position-relative form-group"><input name="password" id="loginPassword" placeholder="Password" type="password" class="form-control"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="my-signin2"></div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <h6 class="mb-0">No account? <a href="javascript:openSignup();" class="text-primary">Sign up now</a></h6>
                            </div>
                            <div class="modal-footer clearfix">
                                <div class="float-right">
                                    <button class="btn btn-primary btn-lg" onclick="logUser($('#loginEmail').val(),$('#loginPassword').val())">Login to Dashboard</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="registerModal" class="modal-dialog w-100">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h5 class="modal-title">
                                </h5><h4 class="mt-2">
                                    <div>Welcome,</div>
                                    <span>It only takes a <span class="text-success">few seconds</span> to create your account</span></h4>

                                <div class="divider row"></div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div id="googleSignUpDiv">
                                            <div class="topHeaderP">

                                               <!-- <div id="gSignInWrapper">
                                                    <div id="googleSignInButton" class="customGPlusSignIn">
                                                        <span class="icon"></span>
                                                        <div style="display: table-cell;width: 100%;text-align: center;">
                                                            <span class="signUpWithGoogleText">Sign up with Google</span>
                                                        </div>
                                                    </div>
                                                </div> -->

                                            </div>
                                            <div class="strike">
                                                <span>Or Sign Up By Email</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="position-relative form-group"><input name="email" id="registerEmail" placeholder="Email" type="email" class="form-control"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="position-relative form-group"><input name="fullName" id="registerFullName" placeholder="Full Name" type="text" class="form-control"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="position-relative form-group"><input name="password" id="registerPassword" placeholder="Password" type="password" class="form-control"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="position-relative form-group"><input name="passwordrep" id="registerRePassword" placeholder="Repeat Password" type="password" class="form-control"></div>
                                    </div>
                                </div>
                                <div class="mt-3 position-relative form-check"><input name="check" id="exampleCheck" type="checkbox" class="form-check-input"><label for="exampleCheck" class="form-check-label">Accept our <a href="javascript:void(0);">Terms
                                            and Conditions</a>.</label></div>
                                <div class="divider row"></div>
                                <h6 class="mb-0">Already have an account? <a href="<?= $siteUrl ?>pro/login.php" class="text-primary">Sign in</a>
                                <!--<h6 class="mb-0">Already have an account? <a href="javascript:openLogin();" class="text-primary">Sign in</a>-->
                            <div class="modal-footer d-block text-center">
                                <button class="btn-wide btn-pill btn-shadow btn-hover-shine btn btn-primary btn-lg" onclick="registerUser($('#registerFullName').val(),$('#registerEmail').val(),$('#registerPassword').val(),$('#registerRePassword').val())">Create Account</button>
                            </div>
                        </div>
                    </div>
<!--                    <div class="app-page-title">-->
<!---->
<!--                    </div>-->

                </div>
<!--                <div class="app-wrapper-footer">-->
<!--                    <div class="app-footer">-->
<!--                        <div class="app-footer__inner">-->
<!--                            <div class="app-footer-left">-->
<!--                                <ul class="nav">-->
<!--                                    <li class="nav-item">-->
<!--                                        <a href="javascript:void(0);" class="nav-link">-->
<!--                                            Footer Link 1-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li class="nav-item">-->
<!--                                        <a href="javascript:void(0);" class="nav-link">-->
<!--                                            Footer Link 2-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                            </div>-->
<!--                            <div class="app-footer-right">-->
<!--                                <ul class="nav">-->
<!--                                    <li class="nav-item">-->
<!--                                        <a href="javascript:void(0);" class="nav-link">-->
<!--                                            Footer Link 3-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li class="nav-item">-->
<!--                                        <a href="javascript:void(0);" class="nav-link">-->
<!--                                            <div class="badge badge-success mr-1 ml-0">-->
<!--                                                <small>NEW</small>-->
<!--                                            </div>-->
<!--                                            Footer Link 4-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>    -->
            </div>
            <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
    <script type="text/javascript" src="<?= $siteUrl ?>assets/scripts/main.js"></script></body>
<!--        <div class="fluid-container">-->
<!--            <div class="row" style="margin: 0">-->
<!--                <div class="col-12" style="padding: 0">-->
<!--                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">-->
<!--                        <div class="container">-->
<!--                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#siteNavBar" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">-->
<!--                                <span class="navbar-toggler-icon"></span>-->
<!--                            </button>-->
<!--                            <div class="collapse navbar-collapse" id="siteNavBar">-->
<!--                                <ul class="navbar-nav mr-auto">-->
<!--                                    <li class="nav-brand active">-->
<!--                                        <a href="index.php" class="nav-link"><img src="https://www.freelogodesign.org/file/app/client/thumb/256083ab-95a0-4a31-8768-f3093edc9fb8_200x200.png?1559152784249"></a>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                                <ul class="navbar-nav float-lg-right">-->
<!--                                    <li class="nav-item">-->
<!--                                        <a href="#" class="nav-link" onclick="openSignup()">Sign Up</a>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </nav>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <script src="js/actions.min.js"></script>
        <script src="js/script.min.js"></script>
        <script src="js/google.min.js"></script>
        <!--<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script> -->
    <?php if (isset($_GET['login'])){ ?>
            <script>
                $("#loginModal").show();
                $("#registerModal").hide();
            </script>
        <?php } ?>
    </body>
</html>
