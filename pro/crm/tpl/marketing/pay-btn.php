<?php
$pay_type = 'live';

if($pay_type == 'sandbox') {
    $receiverEmail = 'iura.kharchienko-facilitator@gmail.com';
    $payNowButtonUrl = 'https://www.sandbox.paypal.com/cgi-bin/websc';
    // $returnUrl = 'https://yuraharchenko921.000webhostapp.com/leadrift/backurl.php';
    $returnUrl = 'https://app.leadrift.com/crm/marketing.php?status=paymentSuccess';
} else {
    $receiverEmail = 'iosifpusser@gmail.com';
    $payNowButtonUrl = 'https://www.paypal.com/cgi-bin/websc';
    $returnUrl = 'https://app.leadrift.com/crm/marketing.php?status=paymentSuccess';
}
?>

<form action="<?php echo $payNowButtonUrl; ?>" method="post">
    <!-- _xclick MEAN => The button that the person clicked was a Buy Now button. -->
    <input type="hidden" name="cmd" value="_xclick">

    <!-- link on busines account (email) -->
    <input type="hidden" name="business" value="<?php echo $receiverEmail; ?>">

    <input id="paypalItemName" type="hidden" name="item_name" value="1 Lead">
    <input id="paypalQuantity" type="hidden" name="quantity" value="1">
    <input id="paypalAmmount" type="hidden" name="amount">

    <!-- Do not prompt buyers for a shipping address. (1 mean - Do not prompt for an address) -->
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="return" value="<?php echo $returnUrl; ?>">
    
    <input id="paypalCustom" type="hidden" name="custom" value='<?php echo $_SESSION['userId'];?>'>
    <input id="paypalState" type="hidden" name="on0" value="none" />

    <!-- use Dollar -->
    <input type="hidden" name="currency_code" value="USD">
    <!-- language => united state -->
    <input type="hidden" name="lc" value="US">
    <!-- An identifier of the source that built the code for the button 
        that the buyer clicked, sometimes known as the build notation -->
    <input type="hidden" name="bn" value="Leadrift_BuyNow_WPS_US">

    <button type="submit" id="paypal-btn" style="display: none"></button>
</form>