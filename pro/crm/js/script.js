function showBetaMsg(){
    mkNoti("Under construction", "Section is not available in the Beta mode, We are working on it", {
        status: 'Danger',
        icon: {class: 'fa fa-minus center-msg'}
    });
}

// Init
$(document).ready(function () {
    getMyLeads();
});
$(document).ready(function () {
    mkNotifications({
        positionY: 'right',
        positionX: 'top',
        scrollable: true,
        rtl: false, // true = ltr
        max: 5 // number of notifications to display
    });
});