<?php
$pay_type = 'live';
$localPath = "/home/joseianq/handymatcher.com/pro/crm/";

if($_SERVER['SERVER_NAME'] == 'handymatcher'):
$localPath = $_SERVER["DOCUMENT_ROOT"] . "/crm";
endif;

require('payment.php');
require('Paypal_IPN.php');

$paypal = new Paypal_IPN($pay_type);
$result = $paypal->run();

$verified = $result['status'];
$data = $result['data'];
$states = json_decode($result['state_list']);

if ($verified) {
    $payment = new Payment();
    $payment->add($data, $states);
}
