<?php
/**
 * Class PaypalIpn
 */

class Paypal_IPN
{

    public function __construct($mode='live')
    {
        if($mode == 'live') {
            $this->_url = 'https://www.paypal.com/cgi-bin/websc';
        } else {
            $this->_url = 'https://www.sandbox.paypal.com/cgi-bin/websc';
        }
    }

    public function run()
    {
        $postFields = 'cmd=_notify-validate';

        foreach ($_POST as $key => $value)
        {
            $postFields .= "&$key=" . urlencode($value);
        }

        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => $this->_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postFields
        ]);

        $result = curl_exec($ch);
        curl_close($ch);

        $timestamp = strtotime($_POST['payment_date']);
        $date = date('Y.m.d H:i:s', $timestamp);

        return [
            'status' => $result, 
            'data' => [
                'id'                => NULL,
                'user_id'           => $_POST['custom'],
                'closed'            => '0',
                'payment_status'    => $_POST['payment_status'],
                'payer_status'      => $_POST['payer_status'],
                'first_name'        => $_POST['first_name'],
                'last_name'         => $_POST['last_name'],
                'mc_gross'          => $_POST['mc_gross'],
                'mc_fee'            => $_POST['mc_fee'],
                'payer_id'          => $_POST['payer_id'],
                'payer_email'       => $_POST['payer_email'],
                'payment_date'      => $date, 
                'txn_id'            => $_POST['txn_id'],
                'residence_country' => $_POST['residence_country'],
                'quantity'          => $_POST['quantity'],
                'item_name'         => $_POST['item_name']
            ],
            'state_list' => $_POST['option_name1']
        ];

    }

}