<?php
require_once("../../classes/DB.php");

class leads extends DB
{
    private $userId;
    function __construct($userId)
    {
        parent::__construct();
        $this->userId = $userId;
    }

    public function getData(){

        $res = [];
        $res['status'] = false;
        $res['userLeads'] = [];

        try {
            $stmt = $this->conn->prepare("SELECT id,leadFullName,leadPhone,leadEmail,leadFromZip,leadToZip,leadWeight,leadMoveDate,status FROM joseianq_crm.leads WHERE leadUserIDAssigned=:userId");

            $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);

            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                if ($row) {
                    $row['leadMoveDate'] = date("m/d/Y",strtotime($row['leadMoveDate']));
                    $res['userLeads'][] = $row;
                }
            }
            $res['status'] = true;

        }catch (PDOException $e){
            echo $e;
            $res['status'] = false;
            $res['userLeads'] = [];
        }

        return $res;
    }
    public function addLead($fullName,$email,$phone,$fZip,$tZip,$weight,$moveDate,$leadStatus){

        $res = [];
        $res['status'] = false;

        try {
            $stmt = $this->conn->prepare("INSERT INTO joseianq_crm.leads (leadFullName,leadEmail,leadPhone,leadFromZip,leadToZip,leadWeight,leadMoveDate,leadUserIDAssigned,status) VALUES(:fullName,:email,:phone,:fZip,:tZip,:weight,:moveDate,:userId,:leadStatus)");

            $stmt->bindParam(':fullName', $fullName, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':phone', $phone, PDO::PARAM_STR);

            $stmt->bindParam(':fZip', $fZip, PDO::PARAM_INT);
            $stmt->bindParam(':tZip', $tZip, PDO::PARAM_INT);
            $stmt->bindParam(':weight', $weight, PDO::PARAM_STR);
            $stmt->bindParam(':moveDate', $moveDate, PDO::PARAM_STR);

            $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);
            $stmt->bindParam(':leadStatus', $leadStatus, PDO::PARAM_INT);

            $stmt->execute();

            $res['status'] = true;

        }catch (PDOException $e){
            $res['values'][] = $fullName;
            $res['values'][] = $email;
            $res['values'][] = $phone;
            $res['values'][] = $fZip;
            $res['values'][] = $tZip;
            $res['values'][] = $weight;
            $res['values'][] = $moveDate;

            $res['error'] = $e;
            $res['status'] = false;
        }

        return $res;

    }
    public function changeStatus($lead_id, $status) {

        $res = [];
        $res['status'] = false;

        try {
            $sql = "UPDATE `leads` SET `status` = :status WHERE `leads`.`id` = :lead_id";
            $stmt = $this->conn->prepare($sql);

            $stmt->bindParam(':status', $status, PDO::PARAM_STR);
            $stmt->bindParam(':lead_id', $lead_id, PDO::PARAM_STR);

            $stmt->execute();

            $res['status'] = true;

        }catch (PDOException $e){
            $res['error'] = $e;
            $res['status'] = false;
        }

        return $res;
    }
}