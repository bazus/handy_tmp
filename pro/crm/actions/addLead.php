<?php
if (isset($_POST['fullName']) && isset($_POST['email']) && isset($_POST['phone']))
session_start();
require_once "../classes/leads.php";

$leads = new leads($_SESSION['userId']);

echo json_encode($leads->addLead($_POST['fullName'],$_POST['email'],$_POST['phone'],$_POST['fromZip'],$_POST['toZip'],$_POST['moveSize'],$_POST['moveDate'],$_POST['leadStatus']));