<?php
session_start();
require_once "../classes/leads.php";

$leads = new leads($_SESSION['userId']);

echo json_encode($leads->getData());