<?php
$siteUrl = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/";
$servername = "127.0.0.1";
$username = "joseianq_joseianq";
$password = "m@@Tah6-[JgM";
$dbname = "joseianq_crm";
$conn;
if(!isset($_SESSION)) {
    session_start();
}
$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username,  $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$stmt = $conn->prepare("SELECT id,fullName,email,createdAt FROM joseianq_crm.users WHERE isDeleted=0 ORDER BY id DESC");
$stmt->execute();
$data = [];
while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    $data[] = $row;
}
?>
<html>
    <head>
        <title>Lead Rift</title>
        <link rel="shortcut icon" href="<?= $siteUrl ?>assets/images/dsasdds.png">
        <!-- ============== Jquery & UI ============== -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <!-- ============== Bootstrap ============== -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script><script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

        <!-- ============== Font Awesome ============== -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">

        <!-- ============== SweetAlert ============== -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <!-- ============== Alert Toast ============== -->
        <link rel="stylesheet" href="plugins/Alert-Toast-MK-Web-Notifications/src/css/mk-notifications.min.css">
        <script src="plugins/Alert-Toast-MK-Web-Notifications/src/js/mk-notifications.min.js"></script>

        <!-- ============== Custom Files ============== -->
        <link href="<?= $siteUrl; ?>main.css" rel="stylesheet"></head>
        <link rel="stylesheet" href="<?= $siteUrl; ?>css/style.css">
        <script>
            var BASE_URL = '<?= $siteUrl ?>';
        </script>
        <meta name="google-signin-client_id" content="357226719040-hd75f4ltg8cakhmgcfmaqo7qsi9cv9dn.apps.googleusercontent.com">
    </head>
    <body>
        <div class="container">
            <div class="row" style="margin-top: 50px;">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Register Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($data as $user){ ?>
                                <tr>
                                    <td><?= $user['id'] ?></td>
                                    <td><?= $user['fullName'] ?></td>
                                    <td><?= $user['email'] ?></td>
                                    <td><?= date("Y-m-d H:i",strtotime($user['createdAt'])) ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/actions.min.js"></script>
        <script src="js/script.min.js"></script>
        <script src="js/google.min.js"></script>
        <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
        <?php if (isset($_GET['login'])){ ?>
            <script>
                $("#loginModal").show();
                $("#registerModal").hide();
            </script>
        <?php } ?>
    </body>
</html>
