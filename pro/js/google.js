function onSuccess(googleUser) {

    var profile = googleUser.getBasicProfile();

    logUser(profile.getEmail(),profile.getId(),true);

}
function onFailure(error) {
    console.log(error);
}
function renderButton() {
    googleInit();

    gapi.signin2.render('my-signin2', {
        'scope': 'profile email',
        'width': 240,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });

    startApp();
}

var googleUser = {};

var startApp = function() {

    gapi.load('auth2', function(){
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        auth2 = gapi.auth2.init({
            client_id: '357226719040-hd75f4ltg8cakhmgcfmaqo7qsi9cv9dn.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            // Request scopes in addition to 'profile' and 'email'
            //scope: 'additional_scope'
        });

        attachSignin(document.getElementById('googleSignInButton'));
    });
};

function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function(googleUser) {
            var profile = googleUser.getBasicProfile();
            registerUser(profile.getName(),profile.getEmail(),profile.getId(),true);
        }, function(error) {
            mkNoti("Error","Register failed, please try again later",{
                status: 'Danger',
                icon: {class:'fa fa-minus center-msg'}
            });
        });
}
function googleInit() {
    try {

    }catch (e) {console.log(e);}
}