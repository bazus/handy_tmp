function logUser(email,password,isGoogle){
    $.ajax({
        url: BASE_URL+"actions/login.php",
        method: "POST",
        data: {
            email:email,
            password:password,
            isGoogle:isGoogle
        },
        async: true
    }).done(function (data) {

        try {
            data = JSON.parse(data);
            if (data.status == true){
                mkNoti("Success","Login Successfully",{
                    status: 'Success',
                    icon: {class:'fa fa-plus center-msg'}
                });
                setTimeout(function () {
                    location.href = BASE_URL+"crm/";
                },1000);
            }else{
                if (data.userLoggedIn == true){
                    mkNoti("Already Logged In","You are already logged in",{
                        status: 'Success',
                        icon: {class:'fa fa-plus center-msg'}
                    });
                    setTimeout(function () {
                        location.href = BASE_URL+"crm/";
                    },1000);
                }else{
                    mkNoti("No Match","Login failed, No match",{
                        status: 'Danger',
                        icon: {class:'fa fa-minus center-msg'}
                    });
                    openLogin();
                }
            }
        }catch (e) {
            mkNoti("Error","Login failed, please try again later",{
                status: 'Danger',
                icon: {class:'fa fa-minus center-msg'}
            });
        }

    });
}
function registerUser(fullName,email,password,isGoogle) {
    $.ajax({
        url: BASE_URL+"actions/register.php",
        method: "POST",
        data: {
            fullName:fullName,
            email:email,
            password:password,
            isGoogle:isGoogle
        },
        async: true
    }).done(function (data) {
        try{
            data = JSON.parse(data);
            if (data.status == true){
                mkNoti("Success","Register Successfully",{
                    status: 'Success',
                    icon: {class:'fa fa-plus center-msg'}
                });
                setTimeout(function () {
                    location.href = BASE_URL+"crm/";
                },1000);
            }else{
                mkNoti("Error","Register failed, please try again later",{
                    status: 'Danger',
                    icon: {class:'fa fa-minus center-msg'}
                });
            }
        }catch (e) {
            mkNoti("Error","Register failed, please try again later",{
                status: 'Danger',
                icon: {class:'fa fa-minus center-msg'}
            });
        }
    });
}