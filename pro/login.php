<?php
session_start();
if (isset($_GET['clearSession'])){
    session_destroy();
}
$siteUrl = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/";
?>
<html>
<head>
    <title>Lead Rift</title>
    <link rel="shortcut icon" href="<?= $siteUrl ?>assets/images/dsasdds.png">
    <!-- ============== Jquery & UI ============== -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- ============== Bootstrap ============== -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script><script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

    <!-- ============== Font Awesome ============== -->
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">

    <!-- ============== SweetAlert ============== -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- ============== Alert Toast ============== -->
    <link rel="stylesheet" href="plugins/Alert-Toast-MK-Web-Notifications/src/css/mk-notifications.min.css">
    <script src="plugins/Alert-Toast-MK-Web-Notifications/src/js/mk-notifications.min.js"></script>

    <!-- ============== Custom Files ============== -->
    <link href="<?= $siteUrl; ?>main.css" rel="stylesheet"></head>
<link rel="stylesheet" href="<?= $siteUrl; ?>css/style.css">
<script>
    var BASE_URL = '<?= $siteUrl ?>';
</script>
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5551127.js"></script>
<!-- End of HubSpot Embed Code -->

<meta name="google-signin-client_id" content="357226719040-hd75f4ltg8cakhmgcfmaqo7qsi9cv9dn.apps.googleusercontent.com">

</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <div class="app-header header-shadow bg-dark">
        <div class="app-header__logo">
            <img src="<?= $siteUrl ?>assets/images/logo.png" alt="LeadRift" height="58">
        </div>
<div class="app-main">
    <div class="app-main__outer" style="padding: 0">
        <div class="app-main__inner">
            <div id="loginModal" class="modal-dialog w-100 mx-auto" >
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="h5 modal-title text-center">
                            <h4 class="mt-2">
                                <div>Welcome back,</div>
                                <span>Please sign in to your account below.</span>
                            </h4>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="position-relative form-group"><input name="email" id="loginEmail" placeholder="Email" type="email" class="form-control"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="position-relative form-group"><input name="password" id="loginPassword" placeholder="Password" type="password" class="form-control"></div>
                            </div>
                            <div class="col-md-12">
                                <div id="my-signin2"></div>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <h6 class="mb-0">No account? <a href="<?= $siteUrl ?>pro/index.php" class="text-primary">Sign up now</a></h6>
                    </div>
                    <div class="modal-footer clearfix">
                        <div class="float-right">
                            <button class="btn btn-primary btn-lg" onclick="logUser($('#loginEmail').val(),$('#loginPassword').val())">Login to Dashboard</button>
                        </div>
                    </div>
                </div>
            </div>

                </div>

        </div>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    </div>
</div>
    <script type="text/javascript" src="<?= $siteUrl ?>assets/scripts/main.js"></script></body>
<script src="js/actions.min.js"></script>
<script src="js/script.min.js"></script>
<script src="js/google.min.js"></script>
<!--<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>-->
<?php if (isset($_GET['login'])){ ?>
    <script>
        $("#loginModal").show();
        $("#registerModal").hide();
    </script>
<?php } ?>
</body>
</html>