<?php

require_once "../classes/login.php";

$login = new login();

$email = $_POST['email'];
$password = $_POST['password'];
$isGoogle = $_POST['isGoogle'];

echo json_encode($login->login($email,$password,$isGoogle));