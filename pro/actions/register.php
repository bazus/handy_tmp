<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
require_once "../classes/register.php";

$register = new register();

$fullName = $_POST['fullName'];
$email = $_POST['email'];
$password = $_POST['password'];
$isGoogle = $_POST['isGoogle'];

echo json_encode($register->register($fullName,$email,$password,$isGoogle));