<div class="app-header header-shadow bg-dark">
    <div class="app-header__logo">
        <img src="<?= $siteUrl ?>assets/images/logo.png" alt="LeadRift" height="58">
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="app-header__content">
        <div class="app-header-left">
        <!--
            <ul class="header-menu nav">
                <ul class="header-menu nav">
                    <li class="nav-item">
                        <a href="<?= $siteUrl ?>crm" class="nav-link">
                            Dashboard
                        </a>
                    </li>
                    <li class="dropdown nav-item">
                        <a href="<?= $siteUrl ?>crm/leads.php" class="nav-link">
                            Contacts
                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                        </a>
                    </li>
                    <li class="dropdown nav-item">
                        <a onclick="javascript:showBetaMsg();" class="nav-link">
                            Conversations
                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                        </a>
                    </li>
                    <li class="dropdown nav-item">
                        <a href="<?= $siteUrl ?>crm/marketing.php" class="nav-link">
                            Marketing
                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                        </a>
                    </li>
                    <li class="dropdown nav-item">
                        <a onclick="javascript:showBetaMsg();" class="nav-link">
                            Sales
                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                        </a>
                    </li>
                </ul>
            </ul>
        -->
        </div>
        <div class="app-header-right">
            <div class="header-btn-lg pr-0">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-right header-user-info ml-3">
                            <button onclick="logout()" type="button" class="btn-shadow p-1 btn btn-primary btn-sm">
                                Logout
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>