<?php
if (isset($_POST['lead_id']) && isset($_POST['status']))
session_start();
require_once "../classes/leads.php";

$leads = new leads($_SESSION['userId']);

echo json_encode($leads->changeStatus($_POST['lead_id'],$_POST['status']));