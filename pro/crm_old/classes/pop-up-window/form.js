var clickedOnScrollbar =(mouseX)=> $(window).outerWidth() - 16 <= mouseX ? true : false
var show_block_ex = null

class show_block {
	constructor(title, block, btnText, height="400px", width="600px") {
		this.title = title;
		this.block = block;
		this.btnText = btnText;
		this.height = height;
		this.height = this.height.replace("px", "");
		this.width = width;
		this.width = this.width.replace("px", "");

		this.body = document.querySelector('body');
		this.generalBlock = document.querySelector('.fluid-container');
		this.hiderPlace = null;
	}

	showBlock() {
		// handle create this block
		this.createBlock();

  		this.generalBlock.style.pointerEvents = "none";
		this.generalBlock.style.opacity  = "0.5";
		this.body.style.overflow = 'hidden'

		// window.scrollTo(0, 0);
		setTimeout(() => $(this.block).addClass("showed"), 100)

		// handle close this block
		this.hiddenBlock().then(result=>{
			if(result) {this._hiddenBlock()}
		})
	}

	createBlock() {
		let topFormMargin = Number(document.body.clientHeight - this.height) / 2

		this.hiderPlace = document.createElement('div')
		this.hiderPlace.className = 'hider-place'
	    this.block.className = "animation-block";
	    this.block.style.minHeight = this.height + "px";
		this.block.style.width  = this.width + "px";
		this.block.style.margin = topFormMargin + "px auto";

		this.hiderPlace.appendChild(this.block);
		this.body.appendChild(this.hiderPlace);
		
    	this.createPanel();
		this.createConfirmBtn();
			
	}
	createPanel() {
		let panel = document.createElement("div");
		  panel.className = "block-panel";
		this.block.appendChild(panel);	

		let name = document.createElement("span");
		  	name.innerHTML = this.title;
		panel.appendChild(name);
	}
	createConfirmBtn() {
		if(this.btnText != "") {
			const btnPanel = document.createElement("div");
				btnPanel.className = "panel-btn";
				this.block.appendChild(btnPanel);

			let btn = document.createElement("button");
				btn.className = "confirm-btn";
				btn.textContent = this.btnText;
				btnPanel.appendChild(btn);	    	
		}  
	}

	hiddenBlock(type='standart') {
		let promise = new Promise((resolve) => {

			// click out of blick
			this.hiderPlace.addEventListener('mousedown', (e)=>{
				if(e.target === this.hiderPlace) {

					if( !clickedOnScrollbar(e.clientX) ) {
						resolve(true)
					}

				}
			}, false)
				
		})
		return promise
	}
	_hiddenBlock() {
		this.generalBlock.style.opacity  = "1";
		this.generalBlock.style.pointerEvents = "auto";
		this.body.style.overflow = 'scroll'

		this.block.remove();
		this.hiderPlace.remove();
	}

	confirm() {
		this._hiddenBlock()
	}

	init() {
		this.showBlock();
	}
}