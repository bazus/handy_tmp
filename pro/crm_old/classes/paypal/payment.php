<?php
$localPath = "/home/joseianq/handymatcher.com";

if($_SERVER['SERVER_NAME'] == 'handymatcher'):
$localPath = $_SERVER["DOCUMENT_ROOT"];
endif;

require_once($localPath ."/classes/DB.php");


class Payment extends DB
{
    function __construct()
    {
        parent::__construct();
    }

    public function add($data, $states) {

        try {

            $keys = implode(', ', array_keys($data));
            $tags = ':' . implode(', :', array_keys($data));

            $sql = "INSERT INTO `lead_txn` ({$keys}) VALUES ({$tags})";
            $statement = $this->conn->prepare($sql);
            $statement->execute($data);

            $txn_id = $this->conn->lastInsertId();

            foreach($states as $key => $value) {
                $sql = "INSERT INTO `txn_state` (`id`, `txn_id`, `state`) VALUES (NULL, :txn_id, :state);";

                $statement = $this->conn->prepare($sql);

                $statement->bindParam(':txn_id', $txn_id, PDO::PARAM_STR);
                $statement->bindParam(':state', $value, PDO::PARAM_STR);

                $statement->execute();
            }

        } catch (PDOException $e) {

            exit;

        }

    }

}
