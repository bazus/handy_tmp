<?php
$siteUrl = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/";
session_start();
if (!isset($_SESSION['userId']) || !isset($_SESSION['fullName'])){
    header("location: $siteUrl?login=1");
}