function logout(){

    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut();

    $.ajax({
        url: BASE_URL+"crm/actions/logout.php",
        async: true
    }).done(function (data) {
        location.href = BASE_URL;
    });
}
function onLoad() {
    gapi.load('auth2', function() {
        gapi.auth2.init();
    });
}
function getMyLeads() {
    $.ajax({
        url: BASE_URL+"crm/actions/getLeads.php",
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status === true && data.userLeads.length > 0){
                document.getElementById("myLeadsTable").innerHTML = "";
                for (var i = 0; i<data.userLeads.length;i++){
                    setMyLeads(data.userLeads[i]);
                }
            }else{
                document.getElementById("myLeadsTable").innerHTML = "<tr><td class='text-center' colspan='8'>No contacts to show</td></tr>";
            }
        }catch (e) {

        }
    });
}
function setMyLeads(data) {
    var container = document.getElementById("myLeadsTable");

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    var tdText = document.createTextNode(data.id);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(data.leadFullName);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(data.leadEmail);
    td.appendChild(tdText);
    tr.appendChild(td);

    // create status SELECT --- START
    var td = document.createElement("td");
    var select = document.createElement('select')
        select.className = 'statusLeadSelect'

    $(select).append( get_status_option(data.status) )

    td.appendChild(select);
    tr.appendChild(td);

    handle_status_select(select, data.id)
    // create status SELECT --- END

    // create more info btn --- START
    var td  = document.createElement("td")
    var btn = document.createElement('div')
        btn.textContent = 'Details'
        btn.className   = 'more_info_btn'
        btn.id          = 'more_info_btn_' + data.id
        btn.dataset.name     = data.leadFullName
        btn.dataset.email    = data.leadEmail
        btn.dataset.phone    = data.leadPhone
        btn.dataset.fromZip  = data.leadFromZip
        btn.dataset.toZip    = data.leadToZip
        btn.dataset.weight   = data.leadWeight
        btn.dataset.moveDate = data.leadMoveDate
        btn.dataset.status   = data.status

    td.appendChild(btn)
    tr.appendChild(td)

    handle_more_btn(btn)
    // create more info btn --- END

    container.appendChild(tr);

}
function addLead() {
    var fullName = document.getElementById("addLeadName").value;
    var email = document.getElementById("addLeadEmail").value;
    var phone = document.getElementById("addLeadPhone").value;
    var fromZip = document.getElementById("addLeadFromZip").value;
    var toZip = document.getElementById("addLeadToZip").value;
    var moveSize = document.getElementById("addLeadMoveSize").value;
    var moveDate = document.getElementById("addLeadMoveDate").value;
    var leadStatus = 'pending';

    $.ajax({
        url: BASE_URL+"crm/actions/addLead.php",
        method:"POST",
        data:{
            email:email,
            phone:phone,
            fullName:fullName,
            fromZip: fromZip,
            toZip: toZip,
            moveSize:moveSize,
            moveDate:moveDate,
            leadStatus:leadStatus
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status === true){
                getMyLeads();
            }else{

            }
        }catch (e) {

        }
    });
}

function get_status_option(status) {
    var statusArr = ['pending', 'contacted', 'booked']
    var options = ''

    for(let value of statusArr) {
        if(value == status) {
            options += `<option selected="true">${value}</option>`
        } else {
            options += `<option>${value}</option>`
        }
    }

    return options
}

function handle_status_select(select, lead_id) {
    $(select).on('change', (e)=>{
        $.ajax({
            url: BASE_URL + "crm/actions/changeStatus.php", 
            method: "POST", 
            data: {
                lead_id: lead_id,
                status: e.target.value,
            }, 
            async: true
        })
        
        $('#more_info_btn_' + lead_id)[0].dataset.status = e.target.value
    })
}

class create_details_form {
	constructor(block) {
        this.block = block
    }

    fill_form(dataset) {
        this.block.innerHTML = `
            <div class="details_wrapper">
                <span>Name  : ${dataset.name}</span>
                <span>Email : ${dataset.email}</span>
                <span>Phone : ${dataset.phone}</span>
                <span>From ZIP : ${dataset.fromZip}</span>
                <span>To ZIP : ${dataset.toZip}</span>
                <span>Weight: ${dataset.weight}</span>
                <span>Move date: ${dataset.moveDate}</span>
                <span>Status: ${dataset.status}</span>
            </div>
        `
    }

    set_styles() {
        $('.animation-block').css({
            'padding-bottom': '0px'
        })
    }

    init(dataset) {
        this.fill_form(dataset)
        this.set_styles()
    }
}

function handle_more_btn(btn) {
    $(btn).on('click', (e)=>{
        let block = document.createElement('div')
    
        // create form
        show_block_ex = new show_block('', block, '', '200px', '300px')
        show_block_ex.init()
    
        // draw data for form
        let create_details_form_ex = new create_details_form(block)
            create_details_form_ex.init(e.target.dataset)	
    
        $('.confirm-btn').css({
            'display': 'none',
            'pointer-events': 'none'
        })
    })
}