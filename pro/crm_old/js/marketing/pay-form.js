var handle_pay_btn_checker = 0

$(document).on('click', '.confirm-btn', ()=>{
	$('.confirm-btn').text('Wait...')
	$('#paypal-btn').click()
})

class create_form_pay {

	constructor(block) {
		this.block = block
		this.state = {
			'price': null,
			'UState': null,
			'quantity': null,
			'item_name': null
		}
		this.stateList = [
			"Alabama",
			"Alaska",
			"Arizona",
			"Arkansas",
			"California",
			"Colorado",
			"Connecticut",
			"Delaware",
			"Florida",
			"Georgia",
			"Hawaii",
			"Idaho",
			"Illinois",
			"Indiana",
			"Iowa",
			"Kansas",
			"Kentucky",
			"Louisiana",
			"Maine",
			"Maryland",
			"Massachusetts",
			"Michigan",
			"Minnesota",
			"Mississippi",
			"Missouri",
			"Montana",
			"Nebraska",
			"Nevada",
			"New Hampshire",
			"New Jersey",
			"New Mexico",
			"New York",
			"North Carolina",
			"North Dakota",
			"Ohio",
			"Oklahoma",
			"Oregon",
			"Pennsylvania",
			"Rhode Island",
			"South Carolina",
			"South Dakota",
			"Tennessee",
			"Texas",
			"Utah",
			"Vermont",
			"Virginia",
			"Washington",
			"West Virginia",
			"Wisconsin",
			"Wyoming"
		]
		this.construct_data = [
			{
				'color': '#ffffff',
				'backg': '#474747',
				'adv_list': [
					'Fully Verified',
					'Easy to replace',
				],
				'price': '39',
				'title': 'EXLUSIVE',
				'quantity_list': [
					10,
					30,
					50,
					100
				]
			}
		]
	}

	create_price_variant(wrapper) {
		let price_form = document.createElement('div')
			price_form.className = 'price_form'

		this.create_price_card(price_form, this.construct_data)

		wrapper.append(price_form)
	}

	create_price_card(price_form, data) {
		for(let key in data) {
			let card = document.createElement('div')

			set_card_styles(card, data[key].color, data[key].backg)

			insert_card_price(card, data[key].price)
			insert_card_title(card, data[key].title)
			insert_separator(card)
			insert_cart_adv(card, data[key].adv_list)
			this.create_pay_btn(card, data[key].price)

			$(price_form).append(card)
		}

		function set_card_styles(target, color, backg) {
			target.className = 'price_item'
			target.style.color = color
			target.style.background = backg
		}
		
		function insert_card_price(target, price) {
			$(target).append(`
				<div class="card_price">
					<h1 style="font-weight:bold;">
						$<span class="card_price_value">${price}</span>
					</h1>
				</div>
			`)
		}
		
		function insert_card_title(target, title) {
			$(target).append(`
				<div class="card_title">
				   	<h6>${title.toUpperCase()}</h6>
				</div>
			`)
		}

		function insert_separator(target) {
			let img_src = location.origin + '/assets/images/marketing/separator.png'
			
			$(target).append(`
				<div class="separator_wrap">
					<img src="${img_src}" />
				</div>
			`)
		}

		function insert_cart_adv(target, adv_list) {
			// let img_src = location.origin + '/assets/images/marketing/check_mark.svg'
			// <img class="check_mark_card" src="${img_src}" />

			let wrap = document.createElement('div')
				wrap.className = 'card_adv'

			let ul = document.createElement('ul')

			for(let value of adv_list) {
				$(ul).append(`<li>${value}</li>`)
			}

			$(wrap).append(ul)
			$(target).append(wrap)
		}
	}

	create_pay_btn(target, price) {
		$(target).append(`
			<div class="imitate_pay_btn_wrap">
				<button data-price="${price}" class="imitate_pay_btn">Get Leads</button>
			</div>
		`)
	}

	handle_pay_btn() {
		$(document).on('click', '.imitate_pay_btn', (e)=>{
			
			$('.animation-block').css({
				'min-height': '250px',
				'width': '600px',
			})

			$('.confirm-btn').css({
				'display': 'block',
			})

			// set checker
			handle_pay_btn_checker = 1

			// get value of price
			let price = e.target.dataset.price
			$('#paypalAmmount').val(price)
			
			// get search by value needed dataObject
			let	neededObj = this.construct_data.find((x)=>{
					if(x.price == price) {
						return x
					}
				})
	
			// remove old info
			$('.price_form').remove()
			
			let form_wrap = document.createElement('div')
				form_wrap.className = 'form_wrap'
	
			this.create_quantity_select(form_wrap, neededObj)
			this.create_state_select(form_wrap)
			
			$('.pay-form-wrapper').append(form_wrap)
		})
	}

	create_quantity_select(wrapper, neededObj) {
		let quantity_form = document.createElement('div')
			quantity_form.className = 'quantity_form'

			let select = document.createElement('select')
				select.className = 'quantity_select'
				
				for(let value of neededObj.quantity_list) {
					let title = value != 1 ? 'Leads' : 'Lead'

					$(select).append(`
						<option value="${value}">${value} ${title}</option>
					`)
				}

			quantity_form.innerHTML = `<h5>Choose quantity</h5>`
			quantity_form.append(select)

		wrapper.append(quantity_form)

		$('#paypalItemName').val(neededObj.quantity_list[0] + ' Leads')
		$('#paypalQuantity').val(neededObj.quantity_list[0])

		$(select).chosen({width: '200px'})
		$(select).on('change', (e)=>{
			let itemName = e.target.children[e.target.selectedIndex].textContent
			let quantity = e.target.value

			$('#paypalItemName').val(itemName)
			$('#paypalQuantity').val(quantity)
		})
	}

	create_state_select(wrapper) {
		let state_form = document.createElement('div')
			state_form.className = 'state_form'

			let select = document.createElement('select')
				select.className = 'state_select'
				select.multiple = 'multiple'
				select.dataset.placeholder = "Select Your States"

				for(let value of this.stateList) {
					select.innerHTML += `
						<option>${value}</option>
					`
				}

			state_form.innerHTML = `<h5>Choose you'r states</h5>`
			state_form.append(select)

		wrapper.append(state_form)
				
		$(select).chosen({width: '200px'})
		$(select).on('change', ()=>{
			$('.confirm-btn').css('pointer-events', 'auto')

			// state
			let custom = $(select).val()
			$('#paypalState').val(JSON.stringify(custom))
		})
	}


	set_style() {
		$('.animation-block').css({
			'background': '#363636',
			'color': '#ffffff',
		})
		$('.hider-place').css({
			'background': '#00000070'
		})
	}
	
	init() {
		this.set_style()

		let wrapper = document.createElement('div')
			wrapper.className = 'pay-form-wrapper'

		this.create_price_variant(wrapper)
		
		if( handle_pay_btn_checker == 0 ) this.handle_pay_btn()

		this.block.append(wrapper)
	}

}

$('#pay-form-btn').on('click', ()=>{
	let block = document.createElement('div')

	// create form
	show_block_ex = new show_block('', block, 'Buy', '350px', '280px')
	show_block_ex.init()

	// draw data for form
	let create_form_pay_ex = new create_form_pay(block)
		create_form_pay_ex.init()	

	$('.confirm-btn').css({
		'display': 'none',
		'pointer-events': 'none'
	})
})