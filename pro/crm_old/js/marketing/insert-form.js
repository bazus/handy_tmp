class create_form_insert {
	constructor(block) {
        this.block = block
    }

    fill_form() {
        this.block.innerHTML = `
            <div class="leadForm main-card">
                <div>
                    <h5 class="text-center text-black" style="font-weight: bold;font-size: 1.1rem;">Insert contact manually</h5>
                </div>
                <div class="leadFormInputs">
                    <div class="form-group">
                        <input type="text" class="form-control" id="addLeadName" placeholder="Contact Full Name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="addLeadPhone" placeholder="Contact Phone">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="addLeadEmail" placeholder="Contact Email">
                    </div>
                    <div class="form-group">
                        <input type="number" min="0" class="form-control" id="addLeadFromZip" placeholder="Contact From ZIP">
                    </div>
                    <div class="form-group">
                        <input type="number" min="0" class="form-control" id="addLeadToZip" placeholder="Contact To ZIP">
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="addLeadMoveSize">
                            <option>Studio 1500 lbs</option>
                            <option>1 BR Small 3000 lbs</option>
                            <option>1 BR Large 4000 lbs</option>
                            <option selected>2 BR Small 4500 lbs</option>
                            <option>2 BR Large 6500 lbs</option>
                            <option>3 BR Small 8000 lbs</option>
                            <option>3 BR Large 9000 lbs</option>
                            <option>4 BR Small 10000 lbs</option>
                            <option>4 BR Large 12000 lbs</option>
                            <option>Over 12000 lbs</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="date" class="form-control" id="addLeadMoveDate" placeholder="Contact Move Date">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success btn-block" id="insert_btn_actually" onclick="addLead()">Insert Contact</button>
                    </div>
                </div>
            </div>
        `
    }

    set_styles() {
        $('.leadForm').css('padding', '0px 0px')
        $('.animation-block').css({
            'padding-bottom': '0px'
        })
    }

    init() {
        this.fill_form()
        this.set_styles()
    }
}

document.addEventListener('click', (e)=>{
    if(e.target.id == 'insert_btn_actually') {
        show_block_ex.confirm()
    }
}, false)

$('#insert-form-btn').on('click', ()=>{
	let block = document.createElement('div')

	// create form
	show_block_ex = new show_block('', block, '', '500px', '300px')
	show_block_ex.init()

	// draw data for form
	let create_form_insert_ex = new create_form_insert(block)
		create_form_insert_ex.init()	

	$('.confirm-btn').css({
		'display': 'none',
		'pointer-events': 'none'
	})
})