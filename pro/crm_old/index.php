<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$localPath = "/var/www/app/crm/";

if($_SERVER['SERVER_NAME'] == 'leadrift'):
    $localPath = $_SERVER["DOCUMENT_ROOT"] . '/crm';
endif;

require_once($localPath."/init/GLOBAL.php");
require_once($localPath."/init/check-payment.php");
?>

<html>
<head>
    <title>Lead Rift</title>
    <link rel="shortcut icon" href="<?= $siteUrl ?>assets/images/dsasdds.png">

    <!-- ============== Jquery & UI ============== -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- ============== Bootstrap ============== -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script><script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

    <!-- ============== Font Awesome ============== -->
    <link rel="stylesheet" href="<?= $siteUrl ?>plugins/font-awesome/css/font-awesome.min.css">

    <!-- ============== SweetAlert ============== -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- ============== Alert Toast ============== -->
    <link rel="stylesheet" href="<?= $siteUrl ?>plugins/Alert-Toast-MK-Web-Notifications/src/css/mk-notifications.min.css">
    <script src="<?= $siteUrl ?>plugins/Alert-Toast-MK-Web-Notifications/src/js/mk-notifications.min.js"></script>

    <!-- ============== Chosen.JS ============== -->
    <link rel="stylesheet" href="<?= $siteUrl ?>assets/chosen/chosen.min.css">

    <!-- ============== Custom Files ============== -->
    <link href="<?= $siteUrl; ?>main.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= $siteUrl ?>crm/css/style.css">
    <link rel="stylesheet" href="<?= $siteUrl ?>crm/css/marketing/pay-form.css">
    <link rel="stylesheet" href="<?= $siteUrl ?>crm/css/marketing/details.css">
    <script>
        var BASE_URL = '<?= $siteUrl ?>';
    </script>

    <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5551127.js"></script>
    <!-- End of HubSpot Embed Code -->

    <meta name="google-signin-client_id" content="357226719040-hd75f4ltg8cakhmgcfmaqo7qsi9cv9dn.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
</head>
<body>
<div class="fluid-container">
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php require_once $localPath."/tpl/header.php"?>
        <div class="app-main">
            <div class="app-main__outer" style="padding: 0">
                <div class="app-main__inner container">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <div class="row" style="margin-top: 25px;">
                                <div class="main-card mb-3 card">
                                    <div class="row" style="margin: 0;">

                                        <?php require_once $localPath."/tpl/marketing/pay-form.php" ?>

                                    </div>
                                    <div class="table-responsive">
                                        <table class="align-middle mb-0 table table-bordered table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Full Name</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody id="myLeadsTable">
                                            <tr>
                                                <td colspan="8" class="text-center">Loading...</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= $siteUrl ?>assets/chosen/chosen.jquery.min.js"></script>
<script src="<?= $siteUrl ?>crm/classes/pop-up-window/form.js"></script>
<script src="<?= $siteUrl ?>crm/js/actions.min.js"></script>
<script src="<?= $siteUrl ?>crm/js/script.min.js"></script>
<script src="<?= $siteUrl ?>crm/js/marketing/pay-form.js"></script>
<script src="<?= $siteUrl ?>crm/js/marketing/insert-form.js"></script>
</body>
</html>