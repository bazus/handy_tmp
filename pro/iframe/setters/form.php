<?php
$ipaddress = '';
if (getenv('HTTP_CLIENT_IP')) {
    $ipaddress = getenv('HTTP_CLIENT_IP');
}else if(getenv('HTTP_X_FORWARDED_FOR')) {
    $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
}else if(getenv('HTTP_X_FORWARDED')) {
    $ipaddress = getenv('HTTP_X_FORWARDED');
}else if(getenv('HTTP_FORWARDED_FOR')) {
    $ipaddress = getenv('HTTP_FORWARDED_FOR');
}else if(getenv('HTTP_FORWARDED')) {
    $ipaddress = getenv('HTTP_FORWARDED');
}else if(getenv('REMOTE_ADDR')) {
    $ipaddress = getenv('REMOTE_ADDR');
}else {
    $ipaddress = '127.0.0.1';
}
?>
<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
<link rel="stylesheet" href="assets/jquery.datetimepicker.min.css">
<script src="assets/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="assets/toastr.min.css">
<script src="assets/toastr.min.js" type="text/javascript"></script>
<style>
    #myEstimateForm{
        width: 320px;
        margin: 0 auto;
        overflow-x: hidden;
    }
    #myEstimateForm .myInputGroup{
        width: 100%;
        text-align: center;
    }
    #myEstimateForm .myInputBtns{
        width: 100%;
        text-align: center;
    }
    #myEstimateForm .myInputBtns .myBtn{
        margin-top: 25px;
        border-width: 1px;
        border-style: solid;
        border-color: #7b7b7b;
        border-radius: 3px;
        background-color: #f27208;
        box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.2), 0 1px 5px 0 rgba(0,0,0,0.12);
        color: #fff;
        cursor: pointer;
        text-shadow: 1px 0 1px #515151;
        padding: 10px;
        font-weight: 600;
        font-size: 20px !important;
        line-height: inherit;
        height: inherit;
        width: 98%;
    }
    #myEstimateForm .myInputGroup .myInput{
        font-size: 14px !important;
        font-family: 'Open Sans', sans-serif !important;
        width: 98%;
        height: 40px;
        border: 1px solid #d6d6d6;
        margin: 0 auto 5px;
        border-radius: 3px;
        padding: .5rem 1rem;
        transition: all .3s;
        -webkit-transition: all .3s;
        -o-transition: all .3s;
        -moz-appearance: none;
        box-sizing: border-box;
        appearance: none;
        transition: border, box-shadow ease-in 0.3s;
    }
    #myEstimateForm .myInputGroup .myHalfInput{
        font-size: 14px !important;
        font-family: 'Open Sans', sans-serif !important;
        width: 48%;
        height: 40px;
        border: 1px solid #d6d6d6;
        margin: 0 auto 5px;
        border-radius: 3px;
        padding: .5rem 1rem;
        transition: all .3s;
        -webkit-transition: all .3s;
        -o-transition: all .3s;
        -moz-appearance: none;
        box-sizing: border-box;
        appearance: none;
        transition: border, box-shadow ease-in 0.3s;
    }
    input, select {
        font-family: inherit;
        font-size: 1rem;
        line-height: 1.5;
        margin: 0;
    }
    input:hover {
        box-shadow: 0 0 5px 1px #337ab7;
        border: 1px solid #337ab7;
    }
    select:hover {
        box-shadow: 0 0 5px 1px #337ab7;
        border: 1px solid #337ab7;
    }
    .mml_privacy-secure {
        text-align: right;
        line-height: 20px;
        margin-top: 6px;
        font-family: 'Open Sans', sans-serif;
        font-weight: 400;
        font-size: 14px;
    }
    @media only screen and (max-width: 1200px) {
        #moveDate:empty:before {
            content: attr(placeholder);
        }
    }
</style>

<form method="post" action="https://app.leadrift.com/iframe/setters/postEstimate.php" id="myEstimateForm">
    <div class="hiddenInputs">
        <input type="hidden" name="ip" value="<?= $ipaddress ?>">
        <input type="hidden" name="userAgent" value="<?=$_SERVER['HTTP_USER_AGENT']?>">
        <input type="hidden" name="ref" id="ref" value="">
    </div>
    <div id="tab1">
        <div class="myInputGroup">
            <input type="text" name="fromData" id="fromData" class="myInput" placeholder="Where are you moving from?" required>
        </div>
        <div class="myInputBtns">
            <button type="button" class="myBtn" onclick="finishFirstStep()">Get My Free Estimate</button>
        </div>
    </div>
    <div id="tab2" style="display: none;">
<!--        <div class="myInputGroup">-->
<!--            <select name="fromState" id="fromState" class="myHalfInput">-->
<!--                <option value="" selected="" disabled="">Pickup State</option>-->
<!--                <option value="AK">AK – Alaska</option>-->
<!--                <option value="AL">AL – Alabama</option>-->
<!--                <option value="AR">AR – Arkansas</option>-->
<!--                <option value="AZ">AZ – Arizona</option>-->
<!--                <option value="CA">CA – California</option>-->
<!--                <option value="CO">CO – Colorado</option>-->
<!--                <option value="CT">CT – Connecticut</option>-->
<!--                <option value="DC">DC – District of Columbia</option>-->
<!--                <option value="DE">DE – Delaware</option>-->
<!--                <option value="FL">FL – Florida</option>-->
<!--                <option value="GA">GA – Georgia</option>-->
<!--                <option value="HI">HI – Hawaii</option>-->
<!--                <option value="ID">ID – Idaho</option>-->
<!--                <option value="IL">IL – Illinois</option>-->
<!--                <option value="IN">IN – Indiana</option>-->
<!--                <option value="IA">IA – Iowa</option>-->
<!--                <option value="KS">KS – Kansas</option>-->
<!--                <option value="KY">KY – Kentucky</option>-->
<!--                <option value="LA">LA – Louisiana</option>-->
<!--                <option value="MA">MA – Massachusetts</option>-->
<!--                <option value="MD">MD – Maryland</option>-->
<!--                <option value="ME">ME – Maine</option>-->
<!--                <option value="MI">MI – Michigan</option>-->
<!--                <option value="MN">MN – Minnesota</option>-->
<!--                <option value="MO">MO – Missouri</option>-->
<!--                <option value="MS">MS – Mississippi</option>-->
<!--                <option value="MT">MT – Montana</option>-->
<!--                <option value="NC">NC – North Carolina</option>-->
<!--                <option value="ND">ND – North Dakota</option>-->
<!--                <option value="NE">NE – Nebraska</option>-->
<!--                <option value="NH">NH – New Hampshire</option>-->
<!--                <option value="NJ">NJ – New Jersey</option>-->
<!--                <option value="NM">NM – New Mexico</option>-->
<!--                <option value="NV">NV – Nevada</option>-->
<!--                <option value="NY">NY – New York</option>-->
<!--                <option value="OH">OH – Ohio</option>-->
<!--                <option value="OK">OK – Oklahoma</option>-->
<!--                <option value="OR">OR – Oregon</option>-->
<!--                <option value="PA">PA – Pennsylvania</option>-->
<!--                <option value="PR">PR – Puerto Rico</option>-->
<!--                <option value="RI">RI – Rhode Island</option>-->
<!--                <option value="SC">SC – South Carolina</option>-->
<!--                <option value="SD">SD – South Dakota</option>-->
<!--                <option value="TN">TN – Tennessee</option>-->
<!--                <option value="TX">TX – Texas</option>-->
<!--                <option value="UT">UT – Utah</option>-->
<!--                <option value="VT">VT – Vermont</option>-->
<!--                <option value="VA">VA – Virginia</option>-->
<!--                <option value="WA">WA – Washington</option>-->
<!--                <option value="WV">WV – West Virginia</option>-->
<!--                <option value="WI">WI – Wisconsin</option>-->
<!--                <option value="WY">WY – Wyoming</option>-->
<!--            </select>-->
<!--            <select name="toState" id="toState" class="myHalfInput">-->
<!--                <option value="" selected="" disabled="">Delivery State</option>-->
<!--                <option value="AK">AK – Alaska</option>-->
<!--                <option value="AL">AL – Alabama</option>-->
<!--                <option value="AR">AR – Arkansas</option>-->
<!--                <option value="AZ">AZ – Arizona</option>-->
<!--                <option value="CA">CA – California</option>-->
<!--                <option value="CO">CO – Colorado</option>-->
<!--                <option value="CT">CT – Connecticut</option>-->
<!--                <option value="DC">DC – District of Columbia</option>-->
<!--                <option value="DE">DE – Delaware</option>-->
<!--                <option value="FL">FL – Florida</option>-->
<!--                <option value="GA">GA – Georgia</option>-->
<!--                <option value="HI">HI – Hawaii</option>-->
<!--                <option value="ID">ID – Idaho</option>-->
<!--                <option value="IL">IL – Illinois</option>-->
<!--                <option value="IN">IN – Indiana</option>-->
<!--                <option value="IA">IA – Iowa</option>-->
<!--                <option value="KS">KS – Kansas</option>-->
<!--                <option value="KY">KY – Kentucky</option>-->
<!--                <option value="LA">LA – Louisiana</option>-->
<!--                <option value="MA">MA – Massachusetts</option>-->
<!--                <option value="MD">MD – Maryland</option>-->
<!--                <option value="ME">ME – Maine</option>-->
<!--                <option value="MI">MI – Michigan</option>-->
<!--                <option value="MN">MN – Minnesota</option>-->
<!--                <option value="MO">MO – Missouri</option>-->
<!--                <option value="MS">MS – Mississippi</option>-->
<!--                <option value="MT">MT – Montana</option>-->
<!--                <option value="NC">NC – North Carolina</option>-->
<!--                <option value="ND">ND – North Dakota</option>-->
<!--                <option value="NE">NE – Nebraska</option>-->
<!--                <option value="NH">NH – New Hampshire</option>-->
<!--                <option value="NJ">NJ – New Jersey</option>-->
<!--                <option value="NM">NM – New Mexico</option>-->
<!--                <option value="NV">NV – Nevada</option>-->
<!--                <option value="NY">NY – New York</option>-->
<!--                <option value="OH">OH – Ohio</option>-->
<!--                <option value="OK">OK – Oklahoma</option>-->
<!--                <option value="OR">OR – Oregon</option>-->
<!--                <option value="PA">PA – Pennsylvania</option>-->
<!--                <option value="PR">PR – Puerto Rico</option>-->
<!--                <option value="RI">RI – Rhode Island</option>-->
<!--                <option value="SC">SC – South Carolina</option>-->
<!--                <option value="SD">SD – South Dakota</option>-->
<!--                <option value="TN">TN – Tennessee</option>-->
<!--                <option value="TX">TX – Texas</option>-->
<!--                <option value="UT">UT – Utah</option>-->
<!--                <option value="VT">VT – Vermont</option>-->
<!--                <option value="VA">VA – Virginia</option>-->
<!--                <option value="WA">WA – Washington</option>-->
<!--                <option value="WV">WV – West Virginia</option>-->
<!--                <option value="WI">WI – Wisconsin</option>-->
<!--                <option value="WY">WY – Wyoming</option>-->
<!--            </select>-->
<!--        </div>-->
        <div class="myInputGroup">
            <input type="text" name="toData" id="toData" class="myInput" placeholder="Where are you moving to?" required>
        </div>
        <div class="myInputGroup">
            <input type="text" name="moveDate" id="moveDate" placeholder="Moving Date" style="" class="myHalfInput">
            <select name="moveSize" id="moveSize" class="myHalfInput" required>
                <option value="" selected="" disabled="">Select Home Size</option>
                <option value="Studio">Studio</option>
                <option value="1 Bedroom Home">1 Bedroom Home</option>
                <option value="2 Bedroom Home">2 Bedroom Home</option>
                <option value="3 Bedroom Home">3 Bedroom Home</option>
                <option value="4 Bedroom Home">4 Bedroom Home</option>
                <option value="4+ Bedroom Home">4+ Bedroom Home</option>
                <option value="Partial">Partial</option>
                <option disabled="">—————-</option>
                <option value="Office Move">Office Move</option>
                <option value="Commercial Move">Commercial Move</option>
            </select>
        </div>
        <div class="myInputBtns">
            <button type="button" class="myBtn" onclick="finishSecondStep()">Almost Done!</button>
        </div>
    </div>
    <div id="tab3" style="display: none;">
        <div class="myInputGroup">
            <input type="text" name="fullName" id="fullName" class="myHalfInput" placeholder="First & Last Name" required>
            <input type="text" name="phone" id="phone" class="myHalfInput" placeholder="Your Phone" required>
        </div>
        <div class="myInputGroup">
            <input type="text" name="email" id="email" class="myInput" placeholder="Your Email" required>
        </div>
        <div class="myInputBtns">
            <button type="button" class="myBtn" onclick="finish()">Request Quote</button>
        </div>
    </div>
</form>
<script>
    function finishFirstStep(){
        $("#tab1").hide();
        $("#tab2").show();
    }
    function finishSecondStep(){
        $("#tab2").hide();
        $("#tab3").show();
    }
    function finish(){

        var phone = document.getElementById("phone").value;

        if (phone){
            $("#myEstimateForm").submit();
        }else{
            toastr.error("Phone are required");
        }
    }
    function getParams(){
        var url = document.referrer;
        url = url.substring(url.indexOf("?") + 1);
        url = url.split("&");
        var myURL = [];
        for(var i = 0;i<url.length;i++){
            url[i] = url[i].split("=");
            myURL[url[i][0]] = url[i][1];
        }
        return myURL;
    }
    $(document).ready(function () {
        // $("#moveDate").datetimepicker({
        //     timepicker: false,
        //     format: "Y/m/d"
        // });
        var $_GET = getParams();
        if ($_GET['r']) {
            $("#ref").val($_GET['r']);
        }else{
            $("#ref").val("No Ref");
        }
    });
</script>