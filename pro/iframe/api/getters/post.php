<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once("../functions/DB.php");
require_once("../functions/leads.php");

$leads = new leads();
$data = $leads->getDataById($_POST['id']);

if ($data['status'] == true) {

    $data = $data['lead'];
    $sendData = [];

    $sendData['key'] = "34d83b637f47fe68bb513ed5ffcebac6c3dacaf9";

    $sendData['from_country'] = 'US';
    $sendData['from_postal_code'] = $data['fromZIP'];
    $sendData['from_admin1_code'] = $data['fromState'];
    $sendData['from_place'] = $data['fromPlace'];

    $sendData['to_country'] = 'US';
    $sendData['to_postal_code'] = $data['toZIP'];
    $sendData['to_admin1_code'] = $data['toState'];
    $sendData['to_place'] = $data['toPlace'];
    $sendData['size'] = $data['moveSize'];
    $sendData['moving_date'] = $data['moveDate'];

    $sendData['customer_name'] = $data['fullName'];
    $sendData['customer_phone'] = $data['areaCode'].$data['phone'];
    $sendData['customer_email'] = $data['email'];
    $sendData['ip'] = $data['ip'];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://www.mymovingloads.com/services/leads/post");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
        http_build_query($sendData));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close($ch);

    $server_output = json_decode($server_output);

    if ($server_output->status == true){
        require_once("../functions/leadPing.php");
        $leadPing = new leadPing();
        $leadPing->setPosted($_POST['id'],json_encode($server_output));
    }

}