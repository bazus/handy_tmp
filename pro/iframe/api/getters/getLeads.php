<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once("../functions/DB.php");
require_once("../functions/leads.php");

$leads = new leads();
$myLeads = $leads->getData();
echo json_encode($myLeads);