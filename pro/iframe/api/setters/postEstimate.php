<?php
header("Access-Control-Allow-Origin: *");
try {
    require_once("../functions/DB.php");
    require_once("../functions/leads.php");

    $leads = new leads();
    if (

        isset($_POST['areaCode'])
        && isset($_POST['fromZIP'])
        && isset($_POST['fromState'])
        && isset($_POST['fromPlace'])
        && isset($_POST['toZIP'])
        && isset($_POST['toState'])
        && isset($_POST['toPlace'])
        && isset($_POST['moveSize'])
        && isset($_POST['moveDate'])
        && isset($_POST['fullName'])
        && isset($_POST['email'])
        && isset($_POST['phone'])
        && isset($_POST['ip'])
        && isset($_POST['userAgent'])
        && isset($_POST['ref'])

    ) {
        $addlead = $leads->addLead($_POST['areaCode'], $_POST['fromZIP'], $_POST['fromState'], $_POST['fromPlace'], $_POST['toZIP'], $_POST['toState'], $_POST['toPlace'], $_POST['moveSize'], $_POST['moveDate'], $_POST['fullName'], $_POST['email'], $_POST['phone'], $_POST['ip'], $_POST['userAgent'], $_POST['ref']);

        header("Location: https://www.loyalmover.com");
    }
}catch (Exception $e){
    header("Location: https://www.loyalmover.com");
}