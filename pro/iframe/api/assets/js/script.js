function getLeads(){
    $.ajax({
        url: "./getters/getLeads.php",
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status == true) {
                document.getElementById("myLeadsTable").innerHTML = "";
                for (var i = 0; i < data.leads.length; i++) {
                    setLead(data.leads[i]);
                }
            }else{
                document.getElementById("myLeadsTable").innerHTML = "<tr><td colspan='7' class='text-center'>No Leads</td></tr>";
            }
        }catch (e) {
            console.log(e);
        }
    });
}
function setLead(data) {

    var container = document.getElementById("myLeadsTable");

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    var tdText = document.createTextNode("#"+data.id);

    td.appendChild(tdText);
    tr.appendChild(td);
    var td = document.createElement("td");
    var tdText = document.createTextNode(data.fullName);

    td.appendChild(tdText);
    tr.appendChild(td);
    var td = document.createElement("td");
    var tdText = document.createTextNode(data.fromZIP);

    td.appendChild(tdText);
    tr.appendChild(td);
    var td = document.createElement("td");
    var tdText = document.createTextNode(data.toZIP);

    td.appendChild(tdText);
    tr.appendChild(td);
    var td = document.createElement("td");
    if (data.status && data.status == 1 && data.price){
        var tdText = document.createTextNode("$"+(data.price));
    }else {
        var tdText = document.createTextNode("---");
    }

    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdButton = document.createElement("button");
    tdButton.setAttribute("disabled",true);
    // if (data.status && data.status == 1){
    //     tdButton.setAttribute("disabled",true);
    // }else{
    //     tdButton.setAttribute("onclick","pingLead("+data.id+")");
    // }
    tdButton.classList.add("btn");
    tdButton.classList.add("btn-primary");
    var tdButtonText = document.createTextNode("Ping");

    tdButton.appendChild(tdButtonText);
    td.appendChild(tdButton);

    var tdButton = document.createElement("button");
    tdButton.setAttribute("disabled",true);
    // if (data.status && data.status == 2){
    //     tdButton.setAttribute("disabled",true);
    // }else{
    //     if (data.status && data.status == 1){
    //         tdButton.setAttribute("onclick","postLead("+data.id+")");
    //     }else{
    //         tdButton.setAttribute("disabled",true);
    //     }
    // }
    tdButton.classList.add("btn");
    tdButton.classList.add("btn-success");
    var tdButtonText = document.createTextNode("Post");

    tdButton.appendChild(tdButtonText);
    td.appendChild(tdButton);
    tr.appendChild(td);

    var td = document.createElement("td");
    var span = document.createElement("span");
    if (data.status && data.status == 1){
        span.classList.add("text-success");
        var spanText = document.createTextNode("Ping Received");
    }else{
        if (data.status && data.status == 2){
            span.classList.add("text-success");
            var spanText = document.createTextNode("Post Sent");
        }else{
            span.classList.add("text-danger");
            var spanText = document.createTextNode("No Ping");
        }
    }

    span.appendChild(spanText);
    td.appendChild(span);
    tr.appendChild(td);

    container.appendChild(tr);
}
function pingLead(id){
    jQuery.ajax({
        url: "getters/ping.php",
        method: "POST",
        cache:false,
        data: {
            id:id
        },
        async: true,
    }).done(function (data) {
        getLeads();
    });
}
function postLead(id){
    jQuery.ajax({
        url: "getters/post.php",
        method: "POST",
        cache:false,
        data: {
            id:id
        },
        async: true,
    }).done(function (data) {
        getLeads();
    });
}
getLeads();