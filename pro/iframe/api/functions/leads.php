<?php

class leads extends DB
{
    function __construct()
    {
        parent::__construct();
    }

    public function getData(){

        $res = [];
        $res['status'] = false;
        $res['leads'] = [];
        try {
            $stmt = $this->conn->prepare("SELECT l.*,lp.status,lp.price FROM joseianq_myleads.leads AS l LEFT JOIN joseianq_myleads.leadPing AS lp ON lp.leadId=l.id");

            $stmt->execute();
            $stmtData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($stmtData) {
                $res['leads'] = $stmtData;
                $res['status'] = true;
            }else{
                $res['status'] = false;
                $res['leads'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['leads'] = [];
        }

        return $res;
    }
    public function getDataById($id){

        $res = [];
        $res['status'] = false;
        $res['lead'] = [];
        try {
            $stmt = $this->conn->prepare("SELECT * FROM joseianq_myleads.leads WHERE id=:id LIMIT 1");

            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            $stmt->execute();
            $stmtData = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmtData) {
                $res['status'] = true;
                $res['lead'] = $stmtData;
            }else{
                $res['status'] = false;
                $res['lead'] = [];
            }

        }catch (PDOException $e){
            $res['status'] = false;
            $res['lead'] = [];
        }

        return $res;
    }
    public function addLead($fromZip,$toZip,$fromState,$toState,$toPlace,$moveSize,$moveDate,$fullName,$email,$phone,$ip,$userAgent,$ref){

        try {
            $moveDate = date("Y-m-d 00:00:00",strtotime($moveDate));
            $stmt = $this->conn->prepare("INSERT INTO joseianq_myleads.leads(fromZIP,toZIP,fromState,toState,toPlace,moveSize,moveDate,fullName,email,phone,ip,userAgent,ref) VALUES(:fromZIP,:toZIP,:fromState,:toState,:toPlace,:moveSize,:moveDate,:fullName,:email,:phone,:ip,:userAgent,:ref)");

            $stmt->bindParam(':fromZIP', $fromZip, PDO::PARAM_INT);
            $stmt->bindParam(':toZIP', $toZip, PDO::PARAM_INT);
            $stmt->bindParam(':fromState', $fromState, PDO::PARAM_STR);
            $stmt->bindParam(':toState', $toState, PDO::PARAM_STR);
            $stmt->bindParam(':toPlace', $toPlace, PDO::PARAM_STR);
            $stmt->bindParam(':moveSize', $moveSize, PDO::PARAM_INT);
            $stmt->bindParam(':moveDate', $moveDate, PDO::PARAM_STR);
            $stmt->bindParam(':fullName', $fullName, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':phone', $phone, PDO::PARAM_STR);
            $stmt->bindParam(':ip', $ip, PDO::PARAM_STR);
            $stmt->bindParam(':userAgent', $userAgent, PDO::PARAM_STR);
            $stmt->bindParam(':ref', $ref, PDO::PARAM_STR);

            $stmt->execute();

            return true;

        }catch (PDOException $e){
            return false;
        }

        return true;

    }
}