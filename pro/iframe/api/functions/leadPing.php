<?php

class leadPing extends DB
{
    function __construct()
    {
        parent::__construct();
    }

    public function addData($leadId,$status,$price){
        try {
            $stmt = $this->conn->prepare("INSERT INTO joseianq_myleads.leadPing(leadId,status,price) VALUES(:leadId,:status,:price)");

            $stmt->bindParam(':leadId', $leadId, PDO::PARAM_INT);
            $stmt->bindParam(':status', $status, PDO::PARAM_INT);
            $stmt->bindParam(':price', $price, PDO::PARAM_INT);

            $stmt->execute();

            return true;

        }catch (PDOException $e){
            return false;
        }

        return true;
    }

    public function setPosted($leadId,$json){
        try {
            $stmt = $this->conn->prepare("UPDATE joseianq_myleads.leadPing SET status=2,response=:response WHERE leadId=:leadId");

            $stmt->bindParam(':leadId', $leadId, PDO::PARAM_INT);
            $stmt->bindParam(':price', $price, PDO::PARAM_INT);
            $stmt->bindParam(':response', $json, PDO::PARAM_INT);

            $stmt->execute();

            return true;

        }catch (PDOException $e){
            return false;
        }

        return true;
    }

}